Red [
	Title: "Files management"
]

files: context [

	docs:		none
	selected:	none
	active-doc: none
	cfg-path:	none

	add: func [
		handle	[integer!]
		name	[string! file!]
	][
		append docs reduce [handle name]
		active-doc: handle
	]

	doc-proto: context [
		handle: 	0 				; [integer!] 	scintilla's hWnd
		name: 		none
		filename:	none 			; [file!] 	filename (including path (?)) for saving, display... can be none when file wasn't saved yet
		saved?: 	false 			; [logic!]  save state
		header:		none 			; 			header from header editor
	]

	;select-doc: func [
	;	handle 	[integer!]
	;] [
	;	current-doc: select docs handle
	;	if none? editor-options [editor-options: prefs/set-options handle join ide-home %default.options]
	;	handle
	;]

	find-doc: func [
		"Select doc by its property. Returns doc object or none."
		word 		[word!]
		value 		[any-type!]
	] [
		foreach doc values-of docs [
			if equal? value doc/:word [return doc]
		]
		none
	]

	append-doc: func [
		handle 		[integer!]
		doc 		[none! object!]
	] [
		unless doc [
			doc: make doc-proto compose [
				handle: (handle)
				name: "Untitled"
				filename: none
			]
		]
		extend docs reduce [handle doc]
	]

	clear-doc: func [
		handle 			[integer!]
	] [
		scintilla/clear-all handle
		scintilla/set-save-point handle
		scintilla/empty-undo-buffer handle
		current-doc/filename: none
		current-doc/name: "Untitled"
	]

	open-doc: func [
		filename 	[file!]
		/local
			script	[string!]
			handle	[integer!]
			options [block!]
	][
		options: prefs/options
		handle: active-doc
		script: read filename

		set-doc handle script

		; some necessary stuff
		if options/convert-eol-on-load? [
			scintilla/convert-eols handle options/eol-mode
		]
		editor/start-undo-collection handle
		scintilla/set-save-point handle
	]

	set-doc: func [
		handle 		[integer!]
		text 		[string!]
	] [
		scintilla/set-text handle text
	]

	make-doc: func [
		"Add new doc to DOCS pool and return handle."
		name 		[string!]
		/local
			spec 	[block!]
			doc 	[object!]
			face 	[object!]
	] [
		name: copy name
		; FIXME: context compose [...] throws error "compose is missing it's value arg"
		spec: compose [
			handle: 	0 ;first gui/tab-editors/pane/1/pane/1/state
			name: 		(name)
			filename: 	none ;%script1.red
		]
		face: gui/add-tab name
		doc: make doc-proto spec
		doc/handle: face/pane/1/state/1
		append-doc doc/handle doc

	;	NOTE: commented, watch for problems! If there are none, delete (30.3.16)
	;	prefs/set-options doc/handle join ide-home %default.options
		prefs/init-editor doc/handle
		editor/start-undo-collection doc/handle

		doc/handle
	]

	remove-doc: func [
		handle 		[integer!]
		/local 
			pos 	[block!]
	] [
		unless docs/:handle [return none]	; NOTE: return error! instead?
		docs/:handle: none
	]

	save-docs-set: func [
		/local
			data 		[block!]
	] [
		data: make block! (length? docs) / 2
		while [
			docs: next docs
			all [not tail? docs file? docs/1]
		][
			append data docs/1
		]
		save cfg-path compose/deep [docs: [(data)] selected: (selected)]
	]

	get-doc-name: func [
		"Return doc	filename with saved mark."
		handle		[integer!]
		/local
			doc
	] [
		doc: docs/:handle
		rejoin [either doc/saved? [""] ["* "] either doc/filename [form next find/last doc/filename #"/"] ["Untitled"]]
	]

	get-doc-names: func [] [
		collect [
			foreach doc words-of docs [
				keep get-doc-name doc
			]
		]
	]

	init: func [/local cfg handle n][
		cfg-path: join ide/home-dir %cfg/docs.set
		cfg: load cfg-path
		docs: make hash! 16
		n: length? cfg/docs
		selected: either cfg/selected > n [n][cfg/selected]
		foreach name cfg/docs [					;-- open all docs
			handle: gui/add-tab "Untitled"
			files/add handle name
			files/open-doc name
			gui/update-doc-info name
		]
		gui/tab-editors/selected: selected
	]
]