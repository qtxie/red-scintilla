Red []

;TBD define only what we need here to reduce file size

#system [
	#include %defines.reds
	#include %wndproc.reds
]

scintilla: context [

	send-message: routine [
		handle	[integer!]
		msg		[integer!]
		wParam	[integer!]
		lParam	[integer!]
	][
		SendMessage handle msg wParam lParam
	]

; === Text retrieval and modification

	set-current-position: routine [
		handle 		[integer!]
		position 	[integer!]
	] [
		SendMessage handle SCI_SETCURRENTPOS position - 1 0
	]

	get-current-position: routine [
		handle 		[integer!]
		return: 	[integer!]
	] [
		1 + SendMessage handle SCI_GETCURRENTPOS 0 0
	]

	get-lines-on-screen: routine [
		handle 		[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_LINESONSCREEN 0 0
	]

	set-first-visible-line: routine [
		handle		[integer!]	
		line 		[integer!]
	] [
		SendMessage handle SCI_SETFIRSTVISIBLELINE line - 1 0
	]

	get-first-visible-line: routine [
		handle		[integer!]	
		return: 	[integer!]
	] [
		SendMessage handle SCI_GETFIRSTVISIBLELINE 0 0
	]

	go-to-line: routine [
		handle		[integer!]	
		return: 	[integer!]
	] [
		SendMessage handle SCI_GOTOLINE line - 1 0
	]

	get-modify: routine [
		handle 		[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_GETMODIFY 0 0
	]

	set-sel: routine [
		handle 		[integer!]
		anchor 		[integer!]
		current 	[integer!]
	] [
		SendMessage handle SCI_SETSEL anchor - 1 current - 1
	]

	go-to-position: routine [
		handle 		[integer!]
		position 	[integer!]
	] [
		SendMessage handle SCI_GOTOPOS position - 1 0
	]

	set-anchor: routine [
		handle 		[integer!]
		position 	[integer!]
	] [
		SendMessage handle SCI_SETANCHOR position - 1 0
	]

	get-anchor: routine [
		handle 		[integer!]
		return: 	[integer!]
	] [
		1 + SendMessage handle SCI_GETANCHOR 0 0
	]

	get-column: routine [
		handle 		[integer!]
		position 	[integer!]
		return: 	[integer!]
	] [
		1 + SendMessage handle SCI_GETCOLUMN position - 1 0	
	]

	find-column: routine [
		handle		[integer!]
		line 		[integer!]
		column 		[integer!]
		return: 	[integer!]
	] [
		1 + SendMessage handle SCI_FINDCOLUMN line - 1 column - 1
	]

	clear-all: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_CLEARALL 0 0
	]

	set-read-only: routine [
		handle 		[integer!]
		read-only? 	[logic!]
	] [
		SendMessage handle SCI_SETREADONLY as integer! read-only? 0
	]

	get-read-only: routine [
		handle 		[integer!]
		return: 	[logic!]
	] [
		as logic! SendMessage handle SCI_GETREADONLY 0 0
	]

	get-text-range: routine [ ; FIXME
		handle 			[integer!]
		index-from 		[integer!]
		index-to 		[integer!]
		text 			[string!]
		/local
			text-range 	[text-range!]
			c-text 		[c-string!]
			len 		[integer!]
			str 		[red-string!]
	] [
		len: -1
		c-text: unicode/to-utf8 text :len
		text-range: declare text-range!
		text-range/text: c-text
		text-range/index-from: index-from - 1
		text-range/index-to: index-to - 1
		SendMessage handle SCI_GETTEXTRANGE 0 as integer! text-range
		str: string/load text-range/text length? text-range/text UTF-8 ; ignore zero-terminator
	;	free as byte-ptr! c-text
		SET_RETURN(str)
	]

	get-text: routine [
		handle 		[integer!]
		length 		[integer!] "Length of text to get (1 byte for zero-terminator is added automatically)."
		/local 
			text 	[c-string!]
			str  	[red-string!]
	] [
		; Expects that Scintilla is switched to UTF-8 (handled in Scintilla/init)
		length: 1 + length
		text: as-c-string allocate length
		SendMessage handle SCI_GETTEXT length as integer! text
		str: string/load text length - 1 UTF-8 ; ignore zero-terminator
		free as byte-ptr! text
		SET_RETURN(str)
	]

	get-line: routine [
		handle 		[integer!]
		line 		[integer!]
		/local 
			length 	[integer!]
			text 	[c-string!]
			str  	[red-string!]
	] [
		; Expects that Scintilla is switched to UTF-8 (handled in Scintilla/init)
		length: line-length handle line
		text: as-c-string allocate length
		SendMessage handle SCI_GETLINE line - 1 as integer! text
		str: string/load text length UTF-8 ; ignore zero-terminator
		free as byte-ptr! text
		SET_RETURN(str)
	]

	line-length: routine [
		handle 		[integer!]
		line 		[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_LINELENGTH line - 1 0
	]

	set-text: routine [
		handle 		[integer!]
		text 		[string!]
		/local
			len 	[integer!]
	] [
		len: -1
		SendMessage handle SCI_SETTEXT 0 as integer! unicode/to-utf8 text :len
	]

	add-text: routine [
		handle 		[integer!]
		length 		[integer!]
		text 		[string!]		
		/local
			len 	[integer!]
	] [
		len: -1
		SendMessage handle SCI_ADDTEXT length as integer! unicode/to-utf8 text :len
	]

	set-selection-start: routine [
		handle 		[integer!]
		position 	[integer!]
	] [
		SendMessage handle SCI_SETSELECTIONSTART position - 1 0
	]

	get-selection-start: routine [
		handle 		[integer!]
		return: 	[integer!]
	] [
		1 + SendMessage handle SCI_GETSELECTIONSTART 0 0
	]

	set-selection-end: routine [
		handle 		[integer!]
		position 	[integer!]
	] [
		SendMessage handle SCI_SETSELECTIONEND position - 1 0
	]

	get-selection-end: routine [
		handle 		[integer!]
		return: 	[integer!]
	] [
		1 + SendMessage handle SCI_GETSELECTIONEND 0 0
	]

	set-empty-selection: routine [
		handle 		[integer!]
		position 	[integer!]
	] [
		SendMessage handle SCI_SETEMPTYSELECTION position - 1 0
	]

	select-all: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_SELECTALL 0 0
	]

	line-from-position: routine [
		handle		[integer!] 
		position 	[integer!]
		return: 	[integer!]	
	] [
		1 + SendMessage handle SCI_LINEFROMPOSITION position - 1 0
	]

	position-from-line: routine [
		handle		[integer!] 
		line 	 	[integer!]
		return: 	[integer!]	
	] [
		1 + SendMessage handle SCI_POSITIONFROMLINE line - 1 0
	]

	get-line-end-position: routine [
		handle		[integer!] 
		line 	 	[integer!]
		return: 	[integer!]	
	] [
		1 + SendMessage handle SCI_GETLINEENDPOSITION line - 1 0
	]

	position-from-point: routine [
		"NOTE: Should translate x/y coords to 1-based index? (same for all four routinetions)"
		handle 		[integer!]
		x			[integer!]
		y 			[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_POSITIONFROMPOINT x y
	]

	position-from-point-close: routine [
		handle 		[integer!]
		x			[integer!]
		y 			[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_POSITIONFROMPOINTCLOSE x y
	]

	char-position-from-point: routine [
		handle 		[integer!]
		x			[integer!]
		y 			[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_CHARPOSITIONFROMPOINT x y
	]

	char-position-from-point-close: routine [
		handle 		[integer!]
		x			[integer!]
		y 			[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_CHARPOSITIONFROMPOINTCLOSE x y
	]

	point-x-from-position: routine [
		handle 		[integer!]
		position	[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_POINTXFROMPOSITION 0 position - 1
	]

	point-y-from-position: routine [
		handle 		[integer!]
		position	[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_POINTYFROMPOSITION 0 position - 1
	]


	set-save-point: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_SETSAVEPOINT 0 0
	]

	scintilla-allocate: routine [
		handle 		[integer!]
		size 		[integer!]
	] [
		SendMessage handle SCI_ALLOCATE size 0
	]

	delete-range: routine [
		handle 		[integer!]
		position 	[integer!]
		length 		[integer!]
	] [
		SendMessage handle SCI_DELETERANGE position - 1 length
	]

	clear-document-style: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_CLEARDOCUMENTSTYLE 0 0
	]


	get-char-at: routine [
		handle 		[integer!]
		position 	[integer!]
		return: 	[integer!]
	] [
		; NOTE: 	Position of of course byte-index and not character-index
		;			But it looks that normal characters are returned as 000000xx
		;			and UTF-8 chars as FFFFFFxx
		;			So it should be possible to retrive whole UTF-8 sequence based on prefix
		SendMessage handle SCI_GETCHARAT position - 1 0
	]

	get-style-at: routine [
		handle		[integer!]
		position 	[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_GETSTYLEAT position - 1 0
	]


	release-all-extended-styles: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_RELEASEALLEXTENDEDSTYLES 0 0
	]

	allocate-extended-styles: routine [
		handle		[integer!]
		count 		[integer!]		
		return: 	[integer!]		"returns the number of the first allocated style"
	] [
		SendMessage handle SCI_ALLOCATEEXTENDEDSTYLES count 0
	]


; === Selection and Information

	get-length: routine [
		handle 		[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_GETLENGTH 0 0
	]

	get-line-count: routine [
		handle 		[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_GETLINECOUNT 0 0
	]

	hide-selection: routine [
		handle 		[integer!]
		hide?		[logic!]
	] [
		SendMessage handle SCI_HIDESELECTION as integer! hide? 0
	]

	get-selection: routine [
		handle 	[integer!]
		/local
			text 		[c-string!]
			str 		[red-string!]
	] [
		text: as c-string! allocate SendMessage handle SCI_GETSELTEXT 0 null
		SendMessage handle SCI_GETSELTEXT 0 as integer! text
		str: string/load text length? text UTF-8
		free as byte-ptr! text
		SET_RETURN(str)	 
	]

; FIXME: just a placeholder
	__get-current-line: routine [
		handle 		[integer!]
		textLen 	[integer!]
		text		[string!]
	] [
		; FIXME: missing text conversion
		SendMessage handle SCI_GETCURLINE textLen as integer! text
	]

	choose-caret-x: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_CHOOSECARETX 0 0
	]

	move-selected-lines-up: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_MOVESELECTEDLINESUP 0 0
	]

	move-selected-lines-down: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_MOVESELECTEDLINESDOWN 0 0
	]

	; TODO: rename this monstrosity
	set-mouse-selection-rectangular-switch: routine [
		handle 				[integer!]
		rectangular? 		[logic!]
	] [
		SendMessage handle SCI_SETMOUSESELECTIONRECTANGULARSWITCH as integer! rectangular? 0
	]

	; TODO: rename this monstrosity
	get-mouse-selection-rectangular-switch: routine [
		handle 				[integer!]
		return:	 			[logic!]
	] [
		as logic! SendMessage handle SCI_GETMOUSESELECTIONRECTANGULARSWITCH 0 0
	]

	selection-is-rectangle?: routine [
		handle 				[integer!]
		return:	 			[logic!]
	] [
		as logic! SendMessage handle SCI_SELECTIONISRECTANGLE 0 0
	]

; Selection modes:

; 'stream (SC_SEL_STREAM=0) 
; 'rectangular (SC_SEL_RECTANGLE=1) 
; 'lines - by lines (SC_SEL_LINES=2)
; 'thin - thin rectangular (SC_SEL_THIN=3)
;
; cancel mode: send same mode, or CANCEL=2325

	_set-selection-mode: routine [
		handle 				[integer!]
		mode	 			[integer!]
	] [
		SendMessage handle SCI_SETSELECTIONMODE mode 0
	]

	set-selection-mode: func [
		handle 				[integer!]
		mode	 			[word!]
		/local
			mode-id 		[integer!]
	] [
		mode-id: find [stream rectangular lines thin] mode
		either mode-id [mode-id: index? mode-id] [2325] ; 2325 = CANCEL
		mode-id: mode-id - 1 ; O-based index
;		#system [SendMessage handle SCI_SETSELECTIONMODE mode-id 0]
		_set-selection-mode handle mode-id
	]

	get-selection-mode: routine [
		handle 				[integer!]
		return:				[integer!]
	] [
		; FIXME: return word instead of numbr
		SendMessage handle SCI_GETSELECTIONMODE 0 0
	]

	get-line-selection-start-position: routine [
		handle 				[integer!]
		line 				[integer!]
	] [
		1 + SendMessage handle SCI_GETLINESELSTARTPOSITION line - 1 0
	]

	get-line-selection-end-position: routine [
		handle 				[integer!]
		line 				[integer!]
		return:				[integer!]
	] [
		1 + SendMessage handle SCI_GETLINESELENDPOSITION line - 1 0
	]

	move-caret-inside-view: routine [
		handle 				[integer!]
	] [
		SendMessage handle SCI_MOVECARETINSIDEVIEW 0 0
	]

	word-start-position: routine [
		handle 				[integer!]
		position 			[integer!]
		only-word-chars?	[logic!]
		return:				[integer!]
	] [
		1 + SendMessage handle SCI_WORDSTARTPOSITION position - 1 as integer! only-word-chars?
	]

	word-end-position: routine [
		handle 				[integer!]
		position 			[integer!]
		only-word-chars?	[logic!]
		return:				[integer!]
	] [
		1 + SendMessage handle SCI_WORDENDPOSITION position - 1 as integer! only-word-chars?
	]

	is-range-word?: routine [
		handle 				[integer!]
		start 				[integer!]
		end 				[integer!]
		return: 			[logic!]
	] [
		as logic! SendMessage handle SCI_ISRANGEWORD start - 1 end - 1
	]

; --- following four routinetions are codepage (Unicode) aware, which is great! (unlike a lot of other routinetions...)

	position-before: routine [
		handle 				[integer!]
		position 			[integer!]
		return:				[integer!]
	] [
		1 + SendMessage handle SCI_POSITIONBEFORE position - 1 0
	]

	position-after: routine [
		handle 				[integer!]
		position 			[integer!]
		return:				[integer!]
	] [
		1 + SendMessage handle SCI_POSITIONAFTER position - 1 0
	]

	position-relative: routine [
		"NOTE: must test what it really does before changing to 1-based index"
		handle 				[integer!]
		position 			[integer!]
		relative 			[integer!]
		return:				[integer!]
	] [
		SendMessage handle SCI_POSITIONRELATIVE position relative
	]

	count-characters: routine [
		handle 				[integer!]
		start 				[integer!]
		end 				[integer!]
		return:				[integer!]
	] [
		SendMessage handle SCI_COUNTCHARACTERS start - 1 end - 1
	]

; --- end of four codepage aware routinetions

	text-height: routine [
		handle 				[integer!]
		line 				[integer!]
		return:				[integer!]
	] [
		SendMessage handle SCI_TEXTHEIGHT line - 1 0
	]


; === Margins

	set-margin-type: routine [
		handle			[integer!]
		margin 			[integer!]
		type 			[integer!]
	] [
		SendMessage handle SCI_SETMARGINTYPEN margin - 1 type
	]

	get-margin-type: routine [
		handle			[integer!]
		margin 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETMARGINTYPEN margin - 1 0
	]

	set-margin-width: routine [
		handle			[integer!]
		margin 			[integer!]
		width 			[integer!]
	] [
		SendMessage handle SCI_SETMARGINWIDTHN margin - 1 width
	]

	get-margin-width: routine [
		handle			[integer!]
		margin 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETMARGINWIDTHN margin - 1 0
	]

	set-margin-sensitive: routine [
		handle			[integer!]
		margin 			[integer!]
		sensitive? 		[logic!]
	] [
		SendMessage handle SCI_SETMARGINSENSITIVEN margin - 1 as integer! sensitive?
	]

	get-margin-sensitive: routine [
		handle			[integer!]
		margin 			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_GETMARGINSENSITIVEN margin - 1 0
	]

	set-margin-mask: routine [
		handle			[integer!]
		margin 			[integer!]
		mask 			[integer!]
	] [
		SendMessage handle SCI_SETMARGINMASKN margin - 1 mask
	]

	get-margin-mask: routine [
		handle			[integer!]
		margin 			[integer!]
	] [
		SendMessage handle SCI_GETMARGINMASKN margin - 1 0
	]

	set-margin-cursor: routine [
		handle			[integer!]
		margin 			[integer!]
		cursor 	 		[integer!]
	] [
		SendMessage handle SCI_SETMARGINCURSORN margin - 1 cursor
	]

	get-margin-cursor: routine [
		handle			[integer!]
		margin 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETMARGINCURSORN margin - 1 0
	]

	set-margin-left: routine [
		handle			[integer!]
		width 			[integer!]
	] [
		SendMessage handle SCI_SETMARGINLEFT 0 width
	]

	get-margin-left: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETMARGINLEFT 0 0
	]

	set-margin-right: routine [
		handle			[integer!]
		width 			[integer!]
	] [
		SendMessage handle SCI_SETMARGINRIGHT 0 width
	]

	get-margin-right: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETMARGINRIGHT 0 0
	]

	set-fold-margin-color: routine [
		handle			[integer!]
		use-setting? 	[logic!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_SETFOLDMARGINCOLOUR as integer! use-setting? to-BGR color
	]

	set-fold-margin-hi-color: routine [
		handle			[integer!]
		use-setting? 	[logic!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_SETFOLDMARGINHICOLOUR as integer! use-setting? to-BGR color
	]

	margin-set-style: routine [
		handle			[integer!]
		line 			[integer!]
		style 			[integer!]
	] [
		SendMessage handle SCI_MARGINSETSTYLE line - 1 style
	]

	margin-get-style: routine [
		handle			[integer!]
		line 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_MARGINGETSTYLE line - 1 0
	]

	margin-text-clear-all: routine [
		handle			[integer!]
	] [
		SendMessage handle SCI_MARGINTEXTCLEARALL 0 0
	]

	margin-set-style-offset: routine [
		handle			[integer!]
		style 			[integer!]
	] [
		SendMessage handle SCI_MARGINSETSTYLEOFFSET style 0
	]

	margin-get-style-offset: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_MARGINGETSTYLEOFFSET 0 0
	]

	set-margin-options: routine [
		handle			[integer!]
		options 		[integer!]
	] [
		SendMessage handle SCI_SETMARGINOPTIONS options 0
	]

	get-margin-options: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETMARGINOPTIONS 0 0
	]


; === Caret, selection, and hotspot styles

	set-sel-fore: routine [
		handle 						[integer!]
		use-selection-fore-color 	[logic!]
		color 						[tuple!]
	] [
		SendMessage handle SCI_SETSELFORE as integer! use-selection-fore-color to-BGR color
	]

	set-sel-back: routine [
		handle 						[integer!]
		use-selection-back-color 	[logic!]
		color 						[tuple!]
	] [
		SendMessage handle SCI_SETSELBACK as integer! use-selection-back-color to-BGR color
	]

	set-sel-alpha: routine [
		handle 					[integer!]
		alpha 					[integer!]
	] [
		SendMessage handle SCI_SETSELALPHA alpha 0
	]

	get-sel-alpha: routine [
		handle 					[integer!]
		return: 				[integer!]
	] [
		SendMessage handle SCI_GETSELALPHA 0 0
	]

	set-sel-eol-filled: routine [
		handle 					[integer!]
		filled? 				[logic!]
	] [
		SendMessage handle SCI_SETSELEOLFILLED as integer! filled? 0
	]

	get-sel-eol-filled: routine [
		handle 					[integer!]
		return: 				[logic!]
	] [
		as logic! SendMessage handle SCI_GETSELEOLFILLED 0 0
	]

	set-caret-fore: routine [
		handle 					[integer!]
		color 					[tuple!]
	] [
		SendMessage handle SCI_SETCARETFORE to-BGR color 0
	]

	get-caret-fore: routine [
		handle 					[integer!]
		/local
			ret 				[red-tuple!]
			value 				[integer!]
			v1 					[integer!]
			v2 					[integer!]

	] [
		value: SendMessage handle SCI_GETCARETFORE 0 0
		v1: value // 256
		value: value / 256
		v2: value // 256
		value: value / 256
		ret: tuple/push 3 v1 v2 value
		SET_RETURN(ret)
	]

	set-caret-line-visible: routine [
		handle 					[integer!]
		show? 					[logic!]
	] [
		SendMessage handle SCI_SETCARETLINEVISIBLE as integer! show? 0
	]

	get-caret-line-visible: routine [
		handle 					[integer!]
		return:					[logic!]
	] [
		as logic! SendMessage handle SCI_GETCARETLINEVISIBLE 0 0
	]

	set-caret-line-back: routine [
		handle 					[integer!]
		color 					[tuple!]
	] [
		SendMessage handle SCI_SETCARETLINEBACK to-BGR color 0
	]

	get-caret-line-back: routine [
		handle 					[integer!]
		/local
			ret 				[red-tuple!]
			value 				[integer!]
			v1 					[integer!]
			v2 					[integer!]
	] [
		value: SendMessage handle SCI_GETCARETLINEBACK 0 0
		v1: value // 256
		value: value / 256
		v2: value // 256
		value: value / 256
		ret: tuple/push 3 v1 v2 value
		SET_RETURN(ret)
	]

	set-caret-line-back-alpha: routine [
		handle 					[integer!]
		alpha 					[integer!]
	] [
		SendMessage handle SCI_SETCARETLINEBACKALPHA alpha 0
	]

	get-caret-line-back-alpha: routine [
		handle 					[integer!]
		return:					[integer!]
	] [
		SendMessage handle SCI_GETCARETLINEBACKALPHA 0 0
	]

	set-caret-line-visible-always: routine [
		handle 					[integer!]
		always-visible? 		[logic!]
	] [
		SendMessage handle SCI_SETCARETLINEVISIBLEALWAYS as integer! always-visible? 0
	]

	get-caret-line-visible-always: routine [
		handle 					[integer!]
		return:					[logic!]
	] [
		as logic! SendMessage handle SCI_GETCARETLINEVISIBLEALWAYS 0 0
	]

	set-caret-period: routine [
		handle 					[integer!]
		period 					[integer!]
	] [
		SendMessage handle SCI_SETCARETPERIOD period 0 
	]

	get-caret-period: routine [
		handle 					[integer!]
		return:					[integer!]
	] [
		SendMessage handle SCI_GETCARETPERIOD 0 0
	]


 ; line caret (CARETSTYLE_LINE=1), a block caret (CARETSTYLE_BLOCK=2) or to not draw at all (CARETSTYLE_INVISIBLE=0)

	; TODO: word settings
	
	set-caret-style: routine [
		handle 					[integer!]
		style 					[integer!]
	] [
		SendMessage handle SCI_SETCARETSTYLE style 0
	]

	; TODO: word settings

	get-caret-style: routine [
		handle 					[integer!]
		return:					[integer!]
	] [
		SendMessage handle SCI_GETCARETSTYLE 0 0
	]

	set-caret-width: routine [
		handle 					[integer!]
		width 					[integer!]
	] [
		SendMessage handle SCI_SETCARETWIDTH width 0
	]

	get-caret-width: routine [
		handle 					[integer!]
		return:					[integer!]
	] [
		SendMessage handle SCI_GETCARETWIDTH 0 0
	]

	set-hotspot-active-fore: routine [
		handle 					[integer!]
		use-setting?			[logic!]
		color 					[tuple!]
	] [
		SendMessage handle SCI_SETHOTSPOTACTIVEFORE as integer! use-setting? to-BGR color
	]

	get-hotspot-active-fore: routine [
		handle 					[integer!]
		return:					[integer!]
	] [
		SendMessage handle SCI_GETHOTSPOTACTIVEFORE 0 0
	]

	set-hotspot-active-back: routine [
		handle 					[integer!]
		use-setting? 			[logic!]
		color 					[tuple!]
	] [
		SendMessage handle SCI_SETHOTSPOTACTIVEBACK as integer! use-setting? to-BGR color
	]

	get-hotspot-active-back: routine [
		handle 					[integer!]
		/local
			ret 				[red-tuple!]
			value 				[integer!]
			v1 					[integer!]
			v2 					[integer!]
	] [
		value: SendMessage handle SCI_GETHOTSPOTACTIVEBACK 0 0
		v1: value // 256
		value: value / 256
		v2: value // 256
		value: value / 256
		ret: tuple/push 3 v1 v2 value
		SET_RETURN(ret)
	]

	set-hotspot-active-underline: routine [
		handle 					[integer!]
		underline 				[logic!]
	] [
		SendMessage handle SCI_SETHOTSPOTACTIVEUNDERLINE as integer! underline 0
	]

	get-hotspot-active-underline: routine [
		handle 					[integer!]
		return:					[logic!]
	] [
		as logic! SendMessage handle SCI_GETHOTSPOTACTIVEUNDERLINE 0 0
	]

	set-hotspot-single-line: routine [
		handle 					[integer!]
		single-line 			[logic!]
	] [
		SendMessage handle SCI_SETHOTSPOTSINGLELINE as integer! single-line 0
	]

	get-hotspot-single-line: routine [
		handle 					[integer!]
		return:					[logic!]
	] [
		as logic! SendMessage handle SCI_GETHOTSPOTSINGLELINE 0 0
	]

	set-caret-sticky: routine [
		handle 						[integer!]
		useCaretStickyBehaviour 	[integer!]
	] [
		SendMessage handle SCI_SETCARETSTICKY useCaretStickyBehaviour 0
	]

	get-caret-sticky: routine [
		handle 					[integer!]
		return:					[integer!]
	] [
		SendMessage handle SCI_GETCARETSTICKY 0 0
	]

	toggle-caret-sticky: routine [
		handle 					[integer!]
	] [
		SendMessage handle SCI_TOGGLECARETSTICKY 0 0
	]


; === Undo and Redo

	undo: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_UNDO 0 0
	]

	can-undo?: routine [
		handle 		[integer!]
		return: 	[logic!]
	] [
		as logic! SendMessage handle SCI_CANUNDO 0 0
	]

	redo: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_REDO 0 0
	]

	can-redo?: routine [
		handle 		[integer!]
		return: 	[logic!]
	] [
		as logic! SendMessage handle SCI_CANREDO 0 0
	]

	empty-undo-buffer: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_EMPTYUNDOBUFFER 0 0
	]

	begin-undo-action: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_BEGINUNDOACTION 0 0
	]

	end-undo-action: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_ENDUNDOACTION 0 0
	]

	set-undo-collection: routine [
		handle 					[integer!]
		undo-collection?		[logic!]
	] [
		SendMessage handle SCI_SETUNDOCOLLECTION as integer! undo-collection? 0
	]

	get-undo-collection: routine [
		handle 		[integer!]
		return: 	[logic!]
	] [
		as logic! SendMessage handle SCI_GETUNDOCOLLECTION 0 0
	]

; === Cut, copy and paste

	cut: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_CUT 0 0
	]

	copy: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_COPY 0 0
	]

	paste: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_PASTE 0 0
	]

	clear: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_CLEAR 0 0
	]

	can-paste?: routine [
		handle 		[integer!]
		return: 	[logic!]
	] [
		as logic! SendMessage handle SCI_CANPASTE 0 0
	]	

	copy-allow-line: routine [
		handle 		[integer!]
	] [
		SendMessage handle SCI_COPYALLOWLINE 0 0
	]

	set-paste-convert-endings: routine [
		handle 		[integer!]
		convert? 	[logic!]
	] [
		SendMessage handle SCI_SETPASTECONVERTENDINGS as integer! convert? 0
	]

	get-paste-convert-endings: routine [
		handle 		[integer!]
		return:	 	[logic!]
	] [
		as logic! SendMessage handle SCI_GETPASTECONVERTENDINGS 0 0
	]

	copy-range: routine [
		handle 		[integer!]
		start 		[integer!]
		end 		[integer!]
	] [
		SendMessage handle SCI_COPYRANGE start - 1 end - 1
	]

	; FIXME: string conversion
	copy-text: routine [
		handle 		[integer!]
		length 		[integer!]
		text 		[string!]
	] [
		SendMessage handle SCI_COPYTEXT length as integer! text
	]

; === Error handling

; Error statuses:

; SC_STATUS_OK 				0 		No failures
; SC_STATUS_FAILURE 			1 		Generic failure
; SC_STATUS_BADALLOC 		2 		Memory is exhausted
; SC_STATUS_WARN_REGEX 	1001 	Regular expression is invalid

	; FIXME: accept words, not integers
	set-status: routine [
		handle			[integer!]
		status			[integer!]
	] [
		SendMessage handle SCI_SETSTATUS status 0
	]

	get-status: routine [
		handle			[integer!]
		return:			[integer!]
	] [
		SendMessage handle SCI_GETSTATUS 0 0
	]

; === Cursor

; Cursor types:

; SC_CURSORNORMAL 	-1 	The normal cursor is displayed.
; SC_CURSORWAIT 		 4 	The wait cursor is displayed when the mouse is over or owned by the Scintilla window.

	; FIXME: accept words, not integers
	set-cursor: routine [
		handle			[integer!]
		type			[integer!]
	] [
		SendMessage handle SCI_SETCURSOR type 0
	]

	get-cursor: routine [
		handle			[integer!]
		return:			[integer!]
	] [
		SendMessage handle SCI_GETCURSOR 0 0
	]


; === Mouse captures

	set-mouse-down-captures: routine [
		handle			[integer!]
		captures?		[logic!]
	] [
		SendMessage handle SCI_SETMOUSEDOWNCAPTURES as integer! captures? 0
	]

	get-mouse-down-captures: routine [
		handle			[integer!]
		return:			[logic!]
	] [
		as logic! SendMessage handle SCI_GETMOUSEDOWNCAPTURES 0 0
	]


; === Overtype

	set-overtype: routine [
		handle			[integer!]
		overtype?		[logic!]
	] [
		SendMessage handle SCI_SETOVERTYPE as integer! overtype? 0
	]

	get-overtype: routine [
		handle			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_GETOVERTYPE 0 0
	]


; === Annotations

	annotation-set-text: routine [
		handle			[integer!]
		line 			[integer!]
		text 			[string!]
		/local
			length 		[integer!]
			c-text 		[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 text :length
;		sci-system/ANNOTATIONSETTEXT handle line - 1 c-text
		SendMessage handle SCI_ANNOTATIONSETTEXT line - 1 as integer! c-text
	]

	annotation-get-text: routine [
		handle			[integer!]
		line 			[integer!]
		/local 
			length 		[integer!]
			text 		[c-string!]
			str  		[red-string!]
	] [
		length: 500 ; FIXME: get real length
		text: as-c-string allocate length
;		sci-system/ANNOTATIONGETTEXT handle line text
		SendMessage handle SCI_ANNOTATIONGETTEXT line as integer! text
		str: string/load text length - 1 UTF-8 ; ignore zero-terminator
		free as byte-ptr! text
		SET_RETURN(str)
	]

	annotation-set-style: routine [
		handle			[integer!]
		line 			[integer!]
		style 			[integer!]
	] [
		SendMessage handle SCI_ANNOTATIONSETSTYLE line style
	]

	annotation-get-style: routine [
		handle			[integer!]
		line 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_ANNOTATIONGETSTYLE line 0
	]

	annotation-set-styles: routine [
		handle			[integer!]
		line 			[integer!]
		styles 			[string!]
		/local
			length 		[integer!]
			c-text 		[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 styles :length
;		sci-system/ANNOTATIONSETSTYLES handle line c-text
		SendMessage handle SCI_ANNOTATIONSETSTYLES line as integer! c-text
	]

	annotation-get-styles: routine [
		handle			[integer!]
		line 			[integer!]
		/local 
			length 		[integer!]
			text 		[c-string!]
			str  		[red-string!]
	] [
		length: 500 ; FIXME: get real length
		text: as-c-string allocate length
;		sci-system/ANNOTATIONGETSTYLES handle line text
		SendMessage handle SCI_ANNOTATIONGETSTYLES line as integer! text
		str: string/load text length - 1 UTF-8 ; ignore zero-terminator
		free as byte-ptr! text
		SET_RETURN(str)
	]

	annotation-get-lines: routine [
		handle			[integer!]
		line 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_ANNOTATIONGETLINES line 0
	]

	annotation-clear-all: routine [
		handle			[integer!]
	] [
		SendMessage handle SCI_ANNOTATIONCLEARALL 0 0
	]

; ANNOTATION_HIDDEN 	0 	Annotations are not displayed.
; ANNOTATION_STANDARD 	1 	Annotations are drawn left justified with no adornment.
; ANNOTATION_BOXED 	2 	Annotations are indented to match the text and are surrounded by a box.
; ANNOTATION_INDENTED 	3 	Annotations are indented to match the text.

	; FIXME: just one func

	_annotation-set-visible: routine [
		handle 			[integer!]
		visible 		[integer!]
	] [
		SendMessage handle SCI_ANNOTATIONSETVISIBLE visible 0
	]

	annotation-set-visible: func [
		handle			[integer!]
		visible 		[word!]
		/local
			id 			[integer!]
	] [
		id: find [hidden standard boxed indented] visible
		id: either id [-1 + index? id] [0]
;		sci-system/ANNOTATIONSETVISIBLE handle visible
;		#system [SendMessage handle SCI_ANNOTATIONSETVISIBLE id 0]
		_annotation-set-visible handle id
	]

	; FIXME: return word instead of integer
	annotation-get-visible: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_ANNOTATIONGETVISIBLE 0 0
	]

	annotation-set-style-offset: routine [
		handle			[integer!]
		style 			[integer!]
	] [
		SendMessage handle SCI_ANNOTATIONSETSTYLEOFFSET style 0
	]

	annotation-get-style-offset: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_ANNOTATIONGETSTYLEOFFSET 0 0
	]


; === Markers

; NOTE: Only built-in markers support now, images will be added later

	marker-define: routine [
		handle			[integer!]
		marker-number 	[integer!]
		marker-symbols 	[integer!] "see SC_MARK_* in %defines.reds"
	] [
		SendMessage handle SCI_MARKERDEFINE marker-number - 1 marker-symbols
	]

	marker-symbol-defined: routine [
		handle			[integer!]
		marker-number 	[integer!]
	] [
		SendMessage handle SCI_MARKERSYMBOLDEFINED marker-number - 1 0
	]

; TODO: Pass object with colors

	marker-set-foreground: routine [
		handle			[integer!]
		marker-number 	[integer!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_MARKERSETFORE marker-number - 1 to-BGR color
	]

	marker-set-background: routine [
		handle			[integer!]
		marker-number 	[integer!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_MARKERSETBACK marker-number - 1 to-BGR color
	]

	marker-set-background-selected: routine [
		handle			[integer!]
		marker-number 	[integer!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_MARKERSETBACKSELECTED marker-number - 1 to-BGR color
	]

	marker-enable-highlight: routine [
		handle			[integer!]
		enabled? 		[logic!]
	] [
		SendMessage handle SCI_MARKERENABLEHIGHLIGHT as integer! enabled? 0
	]

	marker-set-alpha: routine [
		handle			[integer!]
		marker-number 	[integer!]
		alpha 			[integer!]
	] [
		SendMessage handle SCI_MARKERSETALPHA marker-number - 1 alpha
	]

	marker-add: routine [
		handle			[integer!]
		line 			[integer!]
		marker-number 	[integer!]
	] [
		SendMessage handle SCI_MARKERADD line - 1 marker-number - 1
	]

	marker-add-set: routine [
		handle			[integer!]
		line 			[integer!]
		marker-mask 	[integer!]
	] [
		SendMessage handle SCI_MARKERADDSET line - 1 marker-mask
	]

	marker-delete: routine [
		handle			[integer!]
		line 			[integer!]
		marker-number 	[integer!]
	] [
		SendMessage handle SCI_MARKERDELETE line - 1 marker-number - 1
	]

	marker-delete-all: routine [
		handle			[integer!]
		marker-number 	[integer!] "-1 deletes all markers from all lines"
	] [
		SendMessage handle SCI_MARKERDELETEALL marker-number - 1 0
	]

	marker-get: routine [
		handle			[integer!]
		line 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_MARKERGET line - 1 0
	]

	marker-next: routine [
		handle			[integer!]
		line-start 		[integer!]
		marker-mask 	[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_MARKERNEXT line-start - 1 marker-mask
	]

	marker-previous: routine [
		handle			[integer!]
		line-start 		[integer!]
		marker-mask 	[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_MARKERPREVIOUS line-start - 1 marker-mask
	]

	marker-line-from-handle: routine [
		handle			[integer!]
		marker-handle  	[integer!]
		return: 		[integer!]
	] [
		1 + SendMessage handle SCI_MARKERLINEFROMHANDLE marker-handle 0
	]

	marker-delete-handle: routine [
		handle			[integer!]
		marker-handle  	[integer!]
	] [
		SendMessage handle SCI_MARKERDELETEHANDLE marker-handle 0
	]

; === Scrolling and automatic scrolling

	line-scroll: routine [
		handle			[integer!]
		column 			[integer!]	
		line 			[integer!]
	] [
		; NOTE: These are relative numbers so no need for 0/1 translation
		SendMessage handle SCI_LINESCROLL column line
	]

	scroll-caret: routine [
		handle			[integer!]
	] [
		SendMessage handle SCI_SCROLLCARET 0 0
	]

	scroll-range: routine [
		handle			[integer!]
		secondary		[integer!]	
		primary 		[integer!]
	] [
		SendMessage handle SCI_SCROLLRANGE secondary - 1 primary - 1
	]

; TODO: have one routinetion using pair!

	set-x-caret-policy: routine [
		handle			[integer!]
		policy 			[integer!]	
		slop 	 		[integer!]
	] [
		SendMessage handle SCI_SETXCARETPOLICY policy slop
	]

	set-x-caret-policy: routine [
		handle			[integer!]
		policy 			[integer!]	
		slop 	 		[integer!]
	] [
		SendMessage handle SCI_SETYCARETPOLICY policy slop
	]

	set-visible-policy: routine [
		handle			[integer!]
		policy 			[integer!]	
		slop 	 		[integer!]
	] [
		SendMessage handle SCI_SETVISIBLEPOLICY policy slop
	]

	set-h-scroll-bar: routine [
		handle			[integer!]
		visible? 		[logic!]
	] [
		SendMessage handle SCI_SETHSCROLLBAR as integer! visible? 0
	]

	get-h-scroll-bar: routine [
		handle			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_GETHSCROLLBAR 0 0
	]

	set-v-scroll-bar: routine [
		handle			[integer!]
		visible? 		[logic!]
	] [
		SendMessage handle SCI_SETVSCROLLBAR as integer! visible? 0
	]

	get-v-scroll-bar: routine [
		handle			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_GETVSCROLLBAR 0 0
	]

	set-x-offset: routine [
		handle			[integer!]
		offset 			[integer!]
	] [
		SendMessage handle SCI_SETXOFFSET offset - 1 0
	]

	get-x-offset: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		1 + SendMessage handle SCI_GETXOFFSET 0 0
	]

	set-scroll-width: routine [
		handle			[integer!]
		width 			[integer!]
	] [
		SendMessage handle SCI_SETSCROLLWIDTH width 0
	]

	get-scroll-width: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETSCROLLWIDTH 0 0
	]

	set-scroll-width-tracking: routine [
		handle			[integer!]
		tracking? 		[logic!]
	] [
		SendMessage handle SCI_SETSCROLLWIDTHTRACKING as integer! tracking? 0
	]

	get-scroll-width-tracking: routine [
		handle			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_GETSCROLLWIDTHTRACKING 0 0
	]

	set-end-at-last-line: routine [
		handle			[integer!]
		at-last-line? 	[logic!]
	] [
		SendMessage handle SCI_SETENDATLASTLINE as integer! at-last-line? 0
	]

	get-end-at-last-line: routine [
		handle			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_GETENDATLASTLINE 0 0
	]

; === Whitespaces

	_set-view-ws: routine [
		handle 			[integer!]
		mode 			[integer!]		
	] [
		SendMessage handle SCI_SETVIEWWS mode 0
	]

	set-view-ws: func [
		handle 			[integer!]
		mode 			[word!]
		/local
			id 			[integer!]
	] [
		id: switch/default mode [
			'invisible 				[0]
			'visible-always 		[1]
			'visible-after-indent 	[2]
		] [0]
		_set-view-ws handle id
	]

	get-view-ws: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETVIEWWS 0 0
	]

	set-whitespace-fore: routine [
		handle			[integer!]
		useWhitespaceForeColour [logic!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_SETWHITESPACEFORE as integer! useWhitespaceForeColour to-BGR color
	]

	set-whitespace-back: routine [
		handle			[integer!]
		useWhitespaceBackColour [logic!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_SETWHITESPACEBACK as integer! useWhitespaceBackColour to-BGR color
	]

	set-whitespace-size: routine [
		handle			[integer!]
		size 			[integer!]
	] [
		SendMessage handle SCI_SETWHITESPACESIZE size 0
	]

	get-whitespace-size: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETWHITESPACESIZE 0 0
	]

	set-extra-ascent: routine [
		handle			[integer!]
		extraAscent 	[integer!]
	] [
		SendMessage handle SCI_SETEXTRAASCENT extraAscent 0
	]

	get-extra-ascent: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETEXTRAASCENT 0 0
	]

	set-extra-descent: routine [
		handle			[integer!]
		extraDescent 	[integer!]
	] [
		SendMessage handle SCI_SETEXTRADESCENT extraDescent 0
	]

	get-extra-descent: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETEXTRADESCENT 0 0
	]


; === Brace Highlighting

	brace-highlight: routine [
		handle			[integer!]
		pos1 			[integer!]
		pos2 			[integer!]
	] [
		SendMessage handle SCI_BRACEHIGHLIGHT pos1 - 1 pos2 - 1
	]

	brace-bad-light: routine [
		handle			[integer!]
		pos				[integer!]
	] [
		SendMessage handle SCI_BRACEBADLIGHT pos - 1 0
	]

	brace-highlight-indicator: routine [
		handle				[integer!]
		use-indicator? 		[logic!]
		indicator-number	[integer!]
	] [
		SendMessage handle SCI_BRACEHIGHLIGHTINDICATOR as integer! use-indicator? indicator-number
	]

	brace-badlight-indicator: routine [
		handle				[integer!]
		use-indicator? 		[logic!]
		indicator-number	[integer!]
	] [
		SendMessage handle SCI_BRACEBADLIGHTINDICATOR as integer! use-indicator? indicator-number
	]

	brace-match: routine [
		handle				[integer!]
		position 			[integer!]
;		max-restyle			[integer!] "Must be zero (reserved for future use)"
		return: 			[integer!]
	] [
		1 + SendMessage handle SCI_BRACEMATCH position - 1 0 ;max-restyle
	]


; === Folding

	visible-from-doc-line: routine [
		handle					[integer!]
		docLine 				[integer!]
		return: 				[integer!]
	] [
		1 + SendMessage handle SCI_VISIBLEFROMDOCLINE docLine - 1 0
	]

	doc-line-from-visible: routine [
		handle					[integer!]
		displayLine 			[integer!]
		return: 				[integer!]
	] [
		1 + SendMessage handle SCI_DOCLINEFROMVISIBLE displayLine - 1 0
	]

	show-lines: routine [
		handle					[integer!]
		lineStart 				[integer!]
		lineEnd 				[integer!]
	] [
		SendMessage handle SCI_SHOWLINES lineStart - 1 lineEnd - 1
	]

	hide-lines: routine [
		handle					[integer!]
		lineStart 				[integer!]
		lineEnd 				[integer!]
	] [
		SendMessage handle SCI_HIDELINES lineStart - 1 lineEnd - 1
	]

	get-line-visible: routine [
		handle					[integer!]
		lineEnd 				[integer!]
		return: 				[logic!]
	] [
		as logic! SendMessage handle SCI_GETLINEVISIBLE line - 1 0
	]

	get-all-lines-visible: routine [
		handle					[integer!]
		return: 				[logic!]
	] [
		as logic! SendMessage handle SCI_GETALLLINESVISIBLE 0 0
	]

	set-fold-level: routine [
		handle					[integer!]
		line 					[integer!]
		level 					[integer!]
	] [
		SendMessage handle SCI_SETFOLDLEVEL line - 1 level
	]

	get-fold-level: routine [
		handle					[integer!]
		line 					[integer!]
		return: 				[integer!]
	] [
		SendMessage handle SCI_GETFOLDLEVEL line - 1 0
	]

	set-automatic-fold: routine [
		handle					[integer!]
		automaticFold			[integer!]
	] [
		SendMessage handle SCI_SETAUTOMATICFOLD automaticFold 0
	]

	get-automatic-fold: routine [
		handle					[integer!]
		return:					[integer!]
	] [
		SendMessage handle SCI_GETAUTOMATICFOLD 0 0
	]

	set-fold-flags: routine [
		handle					[integer!]
		flags					[integer!]
	] [
		SendMessage handle SCI_SETFOLDFLAGS flags 0
	]

	get-last-child: routine [
		handle					[integer!]
		line 					[integer!]
		level 					[integer!]
		return: 				[integer!]
	] [
		1 + SendMessage handle SCI_GETLASTCHILD line - 1 level
	]

	get-fold-parent: routine [
		handle					[integer!]
		line 					[integer!]
		return: 				[integer!]
	] [
		1 + SendMessage handle SCI_GETFOLDPARENT line - 1 0
	]

	set-fold-expanded: routine [
		handle					[integer!]
		line 					[integer!]
		expanded 				[logic!]		
	] [
		SendMessage handle SCI_SETFOLDEXPANDED line - 1 as integer! expanded
	]

	get-fold-expanded: routine [
		handle					[integer!]
		line 					[integer!]
		return: 				[logic!]		
	] [
		as logic! SendMessage handle SCI_GETFOLDEXPANDED line - 1 0
	]

	contracted-fold-next: routine [
		handle					[integer!]
		lineStart 				[integer!]
		return: 				[integer!]		
	] [
		1 + SendMessage handle SCI_CONTRACTEDFOLDNEXT lineStart - 1 0
	]

	toggle-fold: routine [
		handle					[integer!]
		line 					[integer!]	
	] [
		SendMessage handle SCI_TOGGLEFOLD line - 1 0
	]

; SC_FOLDACTION_CONTRACT 	0 	Contract.
; SC_FOLDACTION_EXPAND 	1 	Expand.
; SC_FOLDACTION_TOGGLE 	2 	Toggle between contracted and expanded.

	; TODO: use words instead of integer

	fold-line: routine [
		handle					[integer!]
		line 					[integer!]	
		action 					[integer!]
	] [
		SendMessage handle SCI_FOLDLINE line - 1 action
	]

	fold-children: routine [
		handle					[integer!]
		line 					[integer!]	
		action 					[integer!]
	] [
		SendMessage handle SCI_FOLDCHILDREN line - 1 action
	]

	fold-all: routine [
		handle					[integer!]
		action 					[integer!]	
	] [
		SendMessage handle SCI_FOLDALL action 0
	]

	expand-children: routine [
		handle					[integer!]
		line 	 				[integer!]	
		level 					[integer!]	
	] [
		SendMessage handle SCI_EXPANDCHILDREN line level
	]

	ensure-visible: routine [
		handle					[integer!]
		line 	 				[integer!]	
	] [
		SendMessage handle SCI_ENSUREVISIBLE line 0
	]

	ensure-visible-force-policy: routine [
		handle					[integer!]
		line 	 				[integer!]	
	] [
		SendMessage handle SCI_ENSUREVISIBLEENFORCEPOLICY line 0
	]


; === Styling

	get-end-styled: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		1 + SendMessage handle SCI_GETENDSTYLED 0 0
	]

	start-styling: routine [
		handle			[integer!]
		position 		[integer!]
	] [
		SendMessage handle SCI_STARTSTYLING position - 1 0 ; second parameter is unused (see Scintilla docs)
	]

	set-styling: routine [
		handle			[integer!]
		length 			[integer!]
		style 			[integer!]
	] [
		SendMessage handle SCI_SETSTYLING length style
	]

	; SETSTYLINGEX: routine [
	; 	handle			[integer!]
	; 	length 			[integer!]
	; 	styles 			[byte-ptr!]
	; ] [
	; 	sci-system/SETSTYLINGEX length as integer! styles
	; 	SendMessage handle SCI_SETSTYLINGEX length as integer! styles
	; ]

	set-line-state: routine [
		handle			[integer!]
		line 			[integer!]
		value 			[integer!]
	] [
		SendMessage handle SCI_SETLINESTATE line - 1 value
	]

	get-line-state: routine [
		handle			[integer!]
		line 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETLINESTATE line - 1 0
	]

	get-max-line-state: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETMAXLINESTATE 0 0
	]

; === Style definition

	style-reset-default: routine [
		handle			[integer!]
	] [
		SendMessage handle SCI_STYLERESETDEFAULT 0 0
	]

	style-clear-all: routine [
		handle			[integer!]
	] [
		SendMessage handle SCI_STYLECLEARALL 0 0
	]

	style-set-font: routine [
		handle			[integer!]
		style-id 		[integer!]
		font-name		[string!]
		/local
			length 		[integer!]
	] [
		length: -1
;		sci-system/STYLESETFONT handle style-id unicode/to-utf8 font-name :length
		SendMessage handle SCI_STYLESETFONT style-id as integer! unicode/to-utf8 font-name :length
	]

	style-get-font: routine [
		handle			[integer!]
		style-id 		[integer!]
		/local
			text 		[c-string!]
			str 		[red-string!]
	] [
		text: as-c-string allocate 33 ; 32 is max length on Windows + 1 byte for zero terminator
;		sci-system/STYLEGETFONT handle style-id text
		as c-string! SendMessage handle SCI_STYLEGETFONT style-id as integer! text
		str: string/load text length? text UTF-8
		free as byte-ptr! text
		SET_RETURN(str)
	]

	style-set-size: routine [
		handle			[integer!]
		style-id 		[integer!]
		size 			[integer!] "size in points"
	] [
		SendMessage handle SCI_STYLESETSIZE style-id size
	]

	style-get-size: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[integer!] "size in points"
	] [
		SendMessage handle SCI_STYLEGETSIZE style-id 0
	]

	style-set-size-fractional: routine [
		handle			[integer!]
		style-id 		[integer!]
		size 			[integer!] "size in 1/100th points"
	] [
		SendMessage handle SCI_STYLESETSIZEFRACTIONAL style-id size
	]

	style-get-size-fractional: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[integer!] "size in 1/100th points"
	] [
		SendMessage handle SCI_STYLEGETSIZEFRACTIONAL style-id 0
	]

	style-set-bold: routine [
		handle			[integer!]
		style-id 		[integer!]
		bold? 			[logic!]
	] [
		SendMessage handle SCI_STYLESETBOLD style-id as integer! bold?
	]

	style-get-bold: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_STYLEGETBOLD style-id 0
	]

	style-set-weight: routine [
		handle			[integer!]
		style-id 		[integer!]
		weight 			[integer!]
	] [
		SendMessage handle SCI_STYLESETWEIGHT style-id weight
	]

	style-get-weight: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_STYLEGETWEIGHT style-id 0
	]

	style-set-italic: routine [
		handle			[integer!]
		style-id 		[integer!]
		italic? 		[logic!]
	] [
		SendMessage handle SCI_STYLESETITALIC style-id as integer! italic?
	]

	style-get-italic: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_STYLEGETITALIC style-id 0
	]

	style-set-underline: routine [
		handle			[integer!]
		style-id 		[integer!]
		underline? 		[logic!]
	] [
		SendMessage handle SCI_STYLESETUNDERLINE style-id as integer! underline?
	]

	style-get-underline: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_STYLEGETUNDERLINE style-id 0
	]

	style-set-fore: routine [
		handle			[integer!]
		style-id 		[integer!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_STYLESETFORE style-id to-BGR color
	]

	style-get-fore: routine [
		handle			[integer!]
		style-id 		[integer!]
		/local
			value 		[integer!]
			v1 			[integer!]
			v2 			[integer!]
			ret 		[red-tuple!]
	] [
		value: SendMessage handle SCI_STYLEGETFORE style-id 0
		v1: value // 256
		value: value / 256
		v2: value // 256
		value: value / 256
		ret: tuple/push 3 v1 v2 value
		SET_RETURN(ret)
	]

	style-set-back: routine [
		handle			[integer!]
		style-id 		[integer!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_STYLESETBACK style-id to-BGR color
	]

	style-get-back: routine [
		handle			[integer!]
		style-id 		[integer!]
		/local
			value 		[integer!]
			v1 			[integer!]
			v2 			[integer!]
			ret 		[red-tuple!]
	] [
		value: SendMessage handle SCI_STYLEGETBACK style-id 0
		v1: value // 256
		value: value / 256
		v2: value // 256
		value: value / 256
		ret: tuple/push 3 v1 v2 value
		SET_RETURN(ret)
	]

	style-set-eol-filled: routine [
		handle			[integer!]
		style-id 		[integer!]
		filled? 		[logic!]
	] [
		SendMessage handle SCI_STYLESETEOLFILLED style-id as integer! filled?
	]

	style-get-eol-filled: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_STYLEGETEOLFILLED style-id 0
	]

	style-set-charset: routine [
		handle			[integer!]
		style-id 		[integer!]
		char-set 		[integer!]
	] [
		SendMessage handle SCI_STYLESETCHARACTERSET style-id char-set
	]

	style-get-charset: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_STYLEGETCHARACTERSET style-id 0
	]

	style-set-case: routine [
		handle			[integer!]
		style-id 		[integer!]
		case-mode 		[integer!]
	] [
		SendMessage handle SCI_STYLESETCASE style-id case-mode
	]

	style-get-case: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_STYLEGETCASE style-id 0
	]

	style-set-visible: routine [
		handle			[integer!]
		style-id 		[integer!]
		visible? 		[logic!]
	] [
		SendMessage handle SCI_STYLESETVISIBLE style-id as integer! visible?
	]

	style-get-visible: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_STYLEGETVISIBLE style-id 0
	]

	style-set-changeable: routine [
		handle			[integer!]
		style-id 		[integer!]
		changeable? 	[logic!]
	] [
		SendMessage handle SCI_STYLESETCHANGEABLE style-id as integer! changeable?
	]

	style-get-changeable: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_STYLEGETCHANGEABLE style-id 0
	]

	style-set-hotspot: routine [
		handle			[integer!]
		style-id 		[integer!]
		hotspot? 		[logic!]
	] [
		SendMessage handle SCI_STYLESETHOTSPOT style-id as integer! hotspot?
	]

	style-get-hotspot: routine [
		handle			[integer!]
		style-id 		[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_STYLEGETHOTSPOT style-id 0
	]


; === Tabs and Indentation Guides

	set-tab-width: routine [
		handle			[integer!]
		chars 			[integer!]
	] [
		SendMessage handle SCI_SETTABWIDTH chars 0
	]

	get-tab-width: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETTABWIDTH 0 0
	]

	clear-tab-stops: routine [
		handle			[integer!]
		line 			[integer!]
	] [
		SendMessage handle SCI_CLEARTABSTOPS line - 1 0
	]

	add-tab-stop: routine [
		handle			[integer!]
		line 			[integer!]
		x 				[integer!]
	] [
		SendMessage handle SCI_ADDTABSTOP line - 1 x
	]

	get-next-tab-stop: routine [
		handle			[integer!]
		line 			[integer!]
		x 				[integer!]
	] [
		SendMessage handle SCI_GETNEXTTABSTOP line - 1 x
	]

	set-use-tabs: routine [
		handle			[integer!]
		use-tabs? 		[logic!]
	] [
		SendMessage handle SCI_SETUSETABS as integer! use-tabs? 0
	]

	get-use-tabs: routine [
		handle			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_GETUSETABS 0 0
	]

	set-indent: routine [
		handle			[integer!]
		chars 			[integer!]
	] [
		SendMessage handle SCI_SETINDENT chars 0
	]

	get-indent: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETINDENT 0 0
	]

	set-tab-indents: routine [
		handle			[integer!]
		tab-indents? 	[logic!]
	] [
		SendMessage handle SCI_SETTABINDENTS as integer! tab-indents? 0
	]

	get-tab-indents: routine [
		handle			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_GETTABINDENTS 0 0
	]

	set-backspace-unindents: routine [
		handle			[integer!]
		unindents? 		[logic!]
	] [
		SendMessage handle SCI_SETBACKSPACEUNINDENTS as integer! unindents? 0
	]

	get-backspace-unindents: routine [
		handle			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_GETBACKSPACEUNINDENTS 0 0
	]

	set-line-indentation: routine [
		handle			[integer!]
		line 			[integer!]
		indentation 	[integer!]
	] [
		SendMessage handle SCI_SETLINEINDENTATION line - 1 indentation
	]

	get-line-indentation: routine [
		handle			[integer!]
		line 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETLINEINDENTATION line - 1 0
	]

	get-line-indent-position: routine [
		handle			[integer!]
		line 			[integer!]
		return: 		[integer!]
	] [
		1 + SendMessage handle SCI_GETLINEINDENTPOSITION line - 1 0
	]

	; FIXME: just one func

	_set-indentation-guides: routine [
		handle			[integer!]
		indent-view 	[integer!]
	] [
		SendMessage handle SCI_SETINDENTATIONGUIDES indent-view 0
	]

	set-indentation-guides: func [
		handle			[integer!]
		indent-view 	[word!]
		/local
			id 			[integer!]
	] [
		id: switch/default indent-view [
			'none 			[0]
			'real 			[1]
			'look-forward 	[2]
			'look-both 		[3]
		] [0]
;		#system [SendMessage handle SCI_SETINDENTATIONGUIDES id 0]
		_set-indentation-guides handle id
	]

	; TODO: return word instead of integer!
	get-indentation-guides: routine [
		handle			[integer!]
		return:	 		[integer!]
	] [
		SendMessage handle SCI_GETINDENTATIONGUIDES 0 0
	]

	set-highlight-guide: routine [
		handle			[integer!]
		column 			[integer!]
	] [
		SendMessage handle SCI_SETHIGHLIGHTGUIDE column - 1 0
	]

	get-highlight-guide: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		1 + SendMessage handle SCI_GETHIGHLIGHTGUIDE 0 0
	]


; === Long lines

	; FIXME: just one func

	_set-edge-mode: routine [
		handle			[integer!]
		mode 			[integer!]
	] [
		SendMessage handle SCI_SETEDGEMODE mode 0
	]

	set-edge-mode: func [
		handle			[integer!]
		mode 			[word! none!]
		/logic
			id 			[integer!]
	] [
		id: switch/default mode [
			'none 		[0]
			'line 		[1]
			'background [2]
		] [0]
;		#system [SendMessage handle SCI_SETEDGEMODE id 0]
		_set-edge-mode handle id
	]

	get-edge-mode: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETEDGEMODE 0 0
	]

	set-edge-column: routine [
		handle			[integer!]
		column 			[integer!]
	] [
		SendMessage handle SCI_SETEDGECOLUMN column - 1 0
	]

	get-edge-column: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		1 + SendMessage handle SCI_GETEDGECOLUMN 0 0
	]

	set-edge-color: routine [
		handle			[integer!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_SETEDGECOLOUR to-BGR color 0
	]

	get-edge-color: routine [
		handle			[integer!]
		/local
			value 		[integer!]
			v1 			[integer!]
			v2 			[integer!]
			ret 		[red-tuple!]
	] [
		value: SendMessage handle SCI_GETEDGECOLOUR 0 0
		v1: value // 256
		value: value / 256
		v2: value // 256
		value: value / 256
		ret: tuple/push 3 v1 v2 value
		SET_RETURN(ret)
	]

; === Line endings

	_set-eol-mode: routine [
		handle			[integer!]
		mode			[integer!]
	] [
		SendMessage handle SCI_SETEOLMODE mode 0
	]

	set-eol-mode: func [
		handle			[integer!]
		mode			[word!]
		/local
			mode-id 	[integer!]
	] [
		mode-id: switch/default mode [
			'crlf 	[0]
			'cr 	[1]
			'lf 	[2]
		] [2]
;		#system [SendMessage handle SCI_SETEOLMODE mode-id 0]
		_set-eol-mode handle mode-id
	]

	; TODO: return word, not integer
	get-eol-mode: routine [
		handle			[integer!]
		return:			[integer!]
	] [
		SendMessage handle SCI_GETEOLMODE 0 0
	]

	_convert-eols: routine [
		handle			[integer!]
		mode			[integer!]
	] [
		SendMessage handle SCI_CONVERTEOLS mode 0
	]

	convert-eols: func [
		handle			[integer!]
		mode			[word!]
		/local
			mode-id 	[integer!]
	] [
		mode-id: switch/default mode [
			'crlf 	[0]
			'cr 	[1]
			'lf 	[2]
		] [2]
;		#system [SendMessage handle SCI_CONVERTEOLS mode-id 0]
		_convert-eols handle mode-id
	]

	set-view-eol: routine [
		handle			[integer!]
		visible			[logic!]
	] [
		SendMessage handle SCI_SETVIEWEOL as integer! visible 0
	]

	get-view-eol: routine [
		handle			[integer!]
		return:			[logic!]
	] [
		as logic! SendMessage handle SCI_GETVIEWEOL 0 0
	]

	get-line-end-types-supported: routine [
		handle			[integer!]
		return:			[integer!]
	] [
		SendMessage handle SCI_GETLINEENDTYPESSUPPORTED 0 0
	]

	set-line-end-types-allowed: routine [
		handle			[integer!]
		lineEndBitSet	[integer!]
	] [
		SendMessage handle SCI_SETLINEENDTYPESALLOWED lineEndBitSet 0
	]

	get-line-end-types-allowed: routine [
		handle			[integer!]
		return:			[integer!]
	] [
		SendMessage handle SCI_GETLINEENDTYPESALLOWED 0 0
	]

	get-line-end-types-active: routine [
		handle			[integer!]
		return:			[integer!]
	] [
		SendMessage handle SCI_GETLINEENDTYPESACTIVE 0 0
	]


; === Searching and replacing

	set-target-start: routine [
		handle			[integer!]
		position		[integer!]
	] [
		SendMessage handle SCI_SETTARGETSTART position - 1 0
	]

	get-target-start: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		1 + SendMessage handle SCI_GETTARGETSTART 0 0
	]

	set-target-end: routine [
		handle			[integer!]
		position		[integer!]
	] [
		SendMessage handle SCI_SETTARGETEND position - 1 0
	]

	get-target-end: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		1 + SendMessage handle SCI_GETTARGETEND 0 0
	]

	set-target-range: routine [
		handle			[integer!]
		start 			[integer!]
		end 			[integer!]
	] [
		SendMessage handle SCI_SETTARGETRANGE start - 1 end - 1
	]

	target-from-selection: routine [
		handle			[integer!]
	] [
		SendMessage handle SCI_TARGETFROMSELECTION 0 0
	]

	target-whole-document: routine [
		handle			[integer!]
	] [
		SendMessage handle SCI_TARGETWHOLEDOCUMENT 0 0
	]

; search flags overview

; SCFIND_MATCHCASE 		A match only occurs with text that matches the case of the search string.
; SCFIND_WHOLEWORD 	A match only occurs if the characters before and after are not word characters.
; SCFIND_WORDSTART 		A match only occurs if the character before is not a word character.
; SCFIND_REGEXP 			The search string should be interpreted as a regular expression.
; SCFIND_POSIX 				Treat regular expression in a more POSIX compatible manner by interpreting bare ( and ) for tagged sections rather than \( and \).

	; FIXME: accpet word/block , not integer

	set-search-flags: routine [
		handle			[integer!]
		flags 			[integer!]
	] [
		SendMessage handle SCI_SETSEARCHFLAGS flags 0
	]

	get-search-flags: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETSEARCHFLAGS 0 0
	]

	search-text: routine [
		handle			[integer!]
		text 			[string!]
		return: 		[integer!] ; "-1: not found, otherwise returns position"
		/local 
			length 		[integer!]
			size 		[integer!]
	] [
		length: -1
		size: length? unicode/to-utf8 text :length
;		sci-system/SEARCHINTARGET handle size unicode/to-utf8 text :length
		1 + SendMessage handle SCI_SEARCHINTARGET size as integer! unicode/to-utf8 text :length
	]

	get-target-text: routine [
		handle 			[integer!]
		/local 
			length 		[integer!]
			text 		[c-string!]
			str  		[red-string!]
	] [
		length: (get-target-end handle) - (get-target-start handle) + 1
		text: as c-string! allocate length
		SendMessage handle SCI_GETTARGETTEXT 0 as integer! text
		text/length: as byte! 0
		str: string/load text length? text UTF-8
		free as byte-ptr! text
		SET_RETURN(str)
	]

	replace-selection: routine [
		handle 			[integer!]
		text 			[string!]
		/local
			length 		[integer!]
	] [
		length: -1
;		sci-system/REPLACESEL handle unicode/to-utf8 text :length
		SendMessage handle SCI_REPLACESEL 0 as integer! unicode/to-utf8 text :length
	]

	replace-target: routine [
		handle 			[integer!]
		text 			[string!]
		length 			[integer!]
		/local
			len 		[integer!]
	] [
		len: -1
		SendMessage handle SCI_REPLACETARGET length as integer! unicode/to-utf8 text :len
	]

	append-text: routine [
		handle		[integer!]	
		text 		[string!]
		/local
			length 	[integer!]
			size 	[integer!]
			c-text 	[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 text :length
		size: length? c-text
		SendMessage handle SCI_APPENDTEXT size as integer! c-text
	]

	insert-text: routine [
		handle		[integer!]	
		position 	[integer!]
		text 		[string!]
		/local
			length 	[integer!]
			c-text 	[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 text :length
		SendMessage handle SCI_INSERTTEXT position - 1 as integer! c-text
	]

	change-insertion: routine [
		handle		[integer!]	
		length 		[integer!]
		text 		[string!]
		/local
			size 	[integer!]
			c-text 	[c-string!]
	] [
		size: -1
		c-text: unicode/to-utf8 text :size
		SendMessage handle SCI_CHANGEINSERTION length as integer! c-text
	]


; === Autocomplete
	ac-show: routine [
		handle 			[integer!]
		lenEntered 		[integer!]
		list 			[string!]
		/local
			length 	[integer!]
			c-text 	[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 list :length
;		sci-system/AUTOCSHOW handle lenEntered c-text
		SendMessage handle SCI_AUTOCSHOW lenEntered as integer! c-text
	]

	ac-cancel: routine [
		handle 			[integer!]
	] [
		SendMessage handle SCI_AUTOCCANCEL 0 0
	]

	ac-active: routine [
		handle 			[integer!]
	] [
		SendMessage handle SCI_AUTOCACTIVE 0 0
	]

	ac-pos-start: routine [
		handle 			[integer!]
	] [
		SendMessage handle SCI_AUTOCPOSSTART 0 0
	]

	ac-complete: routine [
		handle 			[integer!]
	] [
		SendMessage handle SCI_AUTOCCOMPLETE 0 0
	]

	ac-stops: routine [
		handle 			[integer!]
		chars 			[string!]
		/local
			length 		[integer!]
			c-text 		[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 chars :length
;		sci-system/AUTOCSTOPS handle c-text
		SendMessage handle SCI_AUTOCSTOPS 0 as integer! c-text
	]

	; TODO: char! to byte!/integer! conversion

	ac-set-separator: routine [
		handle 			[integer!]
;		char 			[char!]
		char 			[integer!]
	] [
;		sci-system/AUTOCSETSEPARATOR handle as byte! char
		SendMessage handle SCI_AUTOCSETSEPARATOR char 0
	]

	ac-get-separator: routine [
		handle 			[integer!]
;		return: 		[char!]
	] [
		SendMessage handle SCI_AUTOCGETSEPARATOR 0 0
	]

	ac-select: routine [
		handle 			[integer!]
		string 			[string!]
		/local
			length 		[integer!]
			c-text 		[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 string :length
;		sci-system/AUTOCSELECT handle c-text
		SendMessage handle SCI_AUTOCSELECT 0 as integer! c-text
	]

	ac-get-current: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_AUTOCGETCURRENT 0 0
	]

	ac-get-current-text: routine [
		handle 			[integer!]
		text 			[string!]
		/local
			length 	[integer!]
			c-text 	[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 text :length
;		sci-system/AUTOCGETCURRENTTEXT handle c-text
		SendMessage handle SCI_AUTOCGETCURRENTTEXT 0 as integer! text
	]

	ac-set-cancel-at-start: routine [
		handle 			[integer!]
		cancel? 		[logic!]
	] [
		SendMessage handle SCI_AUTOCSETCANCELATSTART as integer! cancel? 0
	]

	ac-get-cancel-at-start: routine [
		handle 			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_AUTOCGETCANCELATSTART 0 0
	]

	ac-set-fillups: routine [
		handle 			[integer!]
		chars 			[string!]
		/local
			length 	[integer!]
			c-text 	[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 chars :length
;		sci-system/AUTOCSETFILLUPS handle c-text
		SendMessage handle SCI_AUTOCSETFILLUPS 0 as integer! c-text
	]

	ac-set-choose-single: routine [
		handle 			[integer!]
		choose-single? 	[logic!]
	] [
		SendMessage handle SCI_AUTOCSETCHOOSESINGLE as integer! choose-single? 0
	]

	ac-set-choose-single: routine [
		handle 			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_AUTOCGETCHOOSESINGLE 0 0
	]

	ac-set-ignore-case: routine [
		handle 			[integer!]
		ignore-case? 	[logic!]
	] [
		SendMessage handle SCI_AUTOCSETIGNORECASE as integer! ignore-case? 0
	]

	ac-get-ignore-case: routine [
		handle 			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_AUTOCGETIGNORECASE 0 0
	]

	ac-set-case-insensitive-behaviour: routine [
		handle 			[integer!]
		behaviour 	 	[integer!]
	] [
		SendMessage handle SCI_AUTOCSETCASEINSENSITIVEBEHAVIOUR behaviour 0
	]

	ac-get-case-insensitive-behaviour: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_AUTOCGETCASEINSENSITIVEBEHAVIOUR 0 0
	]

	ac-set-multi: routine [
		handle 			[integer!]
		multi 	 	 	[integer!]
	] [
		SendMessage handle SCI_AUTOCSETMULTI multi 0
	]

	ac-get-multi: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_AUTOCGETMULTI 0 0
	]

	ac-set-order: routine [
		handle 			[integer!]
		order 	 	 	[integer!]
	] [
		SendMessage handle SCI_AUTOCSETORDER order 0
	]

	ac-get-order: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_AUTOCGETORDER 0 0
	]

	ac-set-autohide: routine [
		handle 			[integer!]
		auto-hide? 		[logic!]
	] [
		SendMessage handle SCI_AUTOCSETAUTOHIDE as integer! auto-hide? 0
	]

	ac-get-autohide: routine [
		handle 			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_AUTOCGETAUTOHIDE 0 0
	]

	ac-set-drop-rest-of-word: routine [
		handle 			[integer!]
		drop-rest? 		[logic!]
	] [
		SendMessage handle SCI_AUTOCSETDROPRESTOFWORD as integer! drop-rest? 0
	]

	ac-get-drop-rest-of-word: routine [
		handle 			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_AUTOCGETDROPRESTOFWORD 0 0
	]

	register-image: routine [
		handle 			[integer!]
		type 			[integer!]
		xpmData 		[integer!] ; FIXME: pointer to what?
	] [
		SendMessage handle SCI_REGISTERIMAGE type xpmData
	]

	register-rgba-image: routine [
		handle 			[integer!]
		type 			[integer!]
		pixels 			[integer!] ; FIXME: pointer to what?
	] [
		SendMessage handle SCI_REGISTERRGBAIMAGE type pixels
	]

	clear-registered-images: routine [
		handle 			[integer!]
	] [
		SendMessage handle SCI_CLEARREGISTEREDIMAGES 0 0
	]

	; TODO: char! to byte! conversion

	ac-set-type-separator: routine [
		handle 			[integer!]
		char 			[integer!]
	] [
		SendMessage handle SCI_AUTOCSETTYPESEPARATOR char 0
	]

	; FIXME: return char!

	ac-get-type-separator: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_AUTOCGETTYPESEPARATOR 0 0
	]

	ac-set-max-height: routine [
		handle 			[integer!]
		row-count 		[integer!]
	] [
		SendMessage handle SCI_AUTOCSETMAXHEIGHT row-count 0
	]

	ac-get-max-height: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_AUTOCGETMAXHEIGHT 0 0
	]

	ac-set-max-width: routine [
		handle 			[integer!]
		char-count 		[integer!]
	] [
		SendMessage handle SCI_AUTOCSETMAXWIDTH char-count 0
	]

	ac-get-max-width: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_AUTOCGETMAXWIDTH 0 0
	]


; === Call tips

	calltip-show: routine [
		handle 			[integer!]
		pos-start 		[integer!]
		definition 		[string!]
		/local
			length 		[integer!]
			c-text 		[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 definition :length
;		sci-system/CALLTIPSHOW handle posStart c-text
		SendMessage handle SCI_CALLTIPSHOW pos-start as integer! c-text
	]

	calltip-cancel: routine [
		handle 			[integer!]
	] [
		SendMessage handle SCI_CALLTIPCANCEL 0 0
	]

	calltip-active?: routine [
		handle 			[integer!]
		return: 		[logic!]
	] [
		as logic! SendMessage handle SCI_CALLTIPACTIVE 0 0
	]

	calltip-pos-start: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		1 + SendMessage handle SCI_CALLTIPPOSSTART 0 0
	]

	calltip-set-pos-start: routine [
		handle 			[integer!]
		posStart 		[integer!]
	] [
		SendMessage handle SCI_CALLTIPSETPOSSTART posStart - 1 0
	]

	calltip-set-hlt: routine [
		handle 			[integer!]
		highlightStart 	[integer!]
		highlightEnd 	[integer!]
	] [
		SendMessage handle SCI_CALLTIPSETHLT highlightStart - 1 highlightEnd - 1
	]

	calltip-set-back: routine [
		handle 			[integer!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_CALLTIPSETBACK to-BGR color 0
	]

	calltip-set-fore: routine [
		handle 			[integer!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_CALLTIPSETFORE to-BGR color 0
	]

	calltip-set-fore-hlt: routine [
		handle 			[integer!]
		color 			[tuple!]
	] [
		SendMessage handle SCI_CALLTIPSETFOREHLT to-BGR color 0
	]

	calltip-use-style: routine [
		handle 			[integer!]
		tabsize 		[integer!]
	] [
		SendMessage handle SCI_CALLTIPUSESTYLE tabsize 0
	]

	callip-set-position: routine [
		handle 			[integer!]
		above? 			[logic!]
	] [
		SendMessage handle SCI_CALLTIPSETPOSITION as integer! above? 0
	]


; === Indicators

; TODO: pass an indicator object to use just one routinetion instead of bazilion of them 

	indicator-set-style: routine [
		handle				[integer!]
		indicator-number  	[integer!]
		indicator-style 	[integer!]
	] [
		SendMessage handle SCI_INDICSETSTYLE indicator-number - 1 indicator-style
	]

	indicator-get-style: routine [
		handle				[integer!]
		indicator-number 	[integer!]
		return: 		 	[integer!]
	] [
		SendMessage handle SCI_INDICGETSTYLE indicator-number - 1 0
	]

	indicator-set-fore-color: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		color 			 	 	[tuple!]
	] [
		SendMessage handle SCI_INDICSETFORE indicator-number - 1 to-BGR color
	]

	indicator-get-fore-color: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		/local
			ret 				[red-tuple!]
			value 				[integer!]
			v1 					[integer!]
			v2 					[integer!]

	] [
		value: SendMessage handle SCI_INDICGETFORE indicator-number - 1 0
		v1: value // 256
		value: value / 256
		v2: value // 256
		value: value / 256
		ret: tuple/push 3 v1 v2 value
		SET_RETURN(ret)
	]

	indicator-set-alpha: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		alpha 		 	 		[integer!]
	] [
		SendMessage handle SCI_INDICSETALPHA indicator-number - 1 alpha
	]

	indicator-get-alpha: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		return: 			 	[integer!]
	] [
		SendMessage handle SCI_INDICGETALPHA indicator-number - 1 0
	]

	indicator-set-outline-alpha: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		alpha 		 	 		[integer!]
	] [
		SendMessage handle SCI_INDICSETOUTLINEALPHA indicator-number - 1 alpha
	]

	indicator-get-outline-alpha: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		return: 			 	[integer!]
	] [
		SendMessage handle SCI_INDICGETOUTLINEALPHA indicator-number - 1 0
	]

	indicator-set-under: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		under? 			 		[logic!]
	] [
		SendMessage handle SCI_INDICSETUNDER indicator-number - 1 as integer! under?
	]

	indicator-get-under: routine [
		handle					[integer!]
		indicator-number 	 	[integer!]
		return: 			 	[logic!]
	] [
		as logic! SendMessage handle SCI_INDICGETUNDER indicator-number - 1 0
	]

	indicator-set-hover-style: routine [
		handle					[integer!]
		indicator-number 	 	[integer!]
		indicator-style 	 	[integer!]
	] [
		SendMessage handle SCI_INDICSETHOVERSTYLE indicator-number - 1 indicator-style
	]

	indicator-get-hover-style: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		return: 			 	[integer!]
	] [
		SendMessage handle SCI_INDICGETHOVERSTYLE indicator-number - 1 0
	]

	indicator-set-hover-foregound: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		color 		 	 		[tuple!]
	] [
		SendMessage handle SCI_INDICSETHOVERFORE indicator-number - 1 to-BGR color
	]

	indicator-get-hover-foregound: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		/local
			ret 				[red-tuple!]
			value 				[integer!]
			v1 					[integer!]
			v2 					[integer!]
	] [
		value: SendMessage handle SCI_INDICGETHOVERFORE indicator-number - 1 0
		v1: value // 256
		value: value / 256
		v2: value // 256
		value: value / 256
		ret: tuple/push 3 v1 v2 value
		SET_RETURN(ret)
	]

	indicator-set-flags: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		flags 				 	[integer!]
	] [
		SendMessage handle SCI_INDICSETFLAGS indicator-number - 1 flags
	]

	indicator-get-flags: routine [
		handle					[integer!]
		indicator-number  		[integer!]
		return: 			 	[integer!]
	] [
		SendMessage handle SCI_INDICGETFLAGS indicator-number - 1 0
	]

	set-indicator-current: routine [
		handle					[integer!]
		indicator 				[integer!]
	] [
		SendMessage handle SCI_SETINDICATORCURRENT indicator - 1 0
	]

	get-indicator-current: routine [
		handle					[integer!]
		indicator 				[integer!]
		return: 			 	[integer!]
	] [
		1 + SendMessage handle SCI_GETINDICATORCURRENT 0 0
	]

	set-indicator-value: routine [
		handle					[integer!]
		value 					[integer!]
	] [
		SendMessage handle SCI_SETINDICATORVALUE value 0
	]

	get-indicator-value: routine [
		handle					[integer!]
		indicator 				[integer!]
		return: 			 	[integer!]
	] [
		SendMessage handle SCI_GETINDICATORVALUE 0 0
	]

	indicator-fill-range: routine [
		handle					[integer!]
		position				[integer!]
		fill-length 			[integer!]
	] [
		SendMessage handle SCI_INDICATORFILLRANGE position - 1 fill-length
	]

	indicator-clear-range: routine [
		handle					[integer!]
		position				[integer!]
		clear-length 			[integer!]
	] [
		SendMessage handle SCI_INDICATORCLEARRANGE position - 1 clear-length
	]

	indicator-all-on-for: routine [
		handle					[integer!]
		position				[integer!]
	] [
		SendMessage handle SCI_INDICATORALLONFOR position - 1 0
	]

	indicator-value-at: routine [
		handle					[integer!]
		indicator 				[integer!]
		position 		 	 	[integer!]
	] [
		SendMessage handle SCI_INDICATORVALUEAT indicator - 1 position - 1
	]

	indicator-start: routine [
		handle					[integer!]
		indicator 				[integer!]
		position 		 	 	[integer!]
	] [
		SendMessage handle SCI_INDICATORSTART indicator - 1 position - 1
	]

	indicator-end: routine [
		handle					[integer!]
		indicator 				[integer!]
		position 		 	 	[integer!]
	] [
		SendMessage handle SCI_INDICATOREND indicator - 1 position - 1
	]

	find-indicator-show: routine [
		handle					[integer!]
		start 	 				[integer!]
		end 	 		 	 	[integer!]
	] [
		SendMessage handle SCI_FINDINDICATORSHOW start - 1 end - 1
	]

	find-indicator-flash: routine [
		handle					[integer!]
		start 	 				[integer!]
		end 	 		 	 	[integer!]
	] [
		SendMessage handle SCI_FINDINDICATORFLASH start - 1 end - 1
	]

	find-indicator-hide: routine [
		handle					[integer!]
		start 	 				[integer!]
		end 	 		 	 	[integer!]
	] [
		SendMessage handle SCI_FINDINDICATORHIDE 0 0
	]


; === Zooming

	zoom-in: routine [
		handle			[integer!]
	] [
		SendMessage handle SCI_ZOOMIN 0 0
	]

	zoom-out: routine [
		handle			[integer!]
	] [
		SendMessage handle SCI_ZOOMOUT 0 0
	]

	set-zoom: routine [
		handle			[integer!]
		level 			[integer!]
	] [
		SendMessage handle SCI_SETZOOM level 0
	]

	get-zoom: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETZOOM 0 0
	]

; === LEXER

	set-lexer: routine [
		handle 			[integer!]
		lexer 			[integer!]
	] [
		SendMessage handle SCI_SETLEXER lexer 0
	]

	get-lexer: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETLEXER 0 0
	]

	set-keywords: routine [
		handle			[integer!]
		keyword-set		[integer!]
		keywords		[string!]
		/local
			len			[integer!]
			list		[c-string!]
	][
		len: -1
		list: unicode/to-utf8 keywords :len
		SendMessage handle SCI_SETKEYWORDS keyword-set as-integer list
	]

	colorise: routine [
		handle 			[integer!]
		start 			[integer!]
		end 			[integer!]
	] [
		SendMessage handle SCI_COLOURISE start - 1 end - 1
	]


; === Other settings

	set-buffered-draw: routine [
		handle			[integer!]
		is-buffered?	[logic!]
	] [
		SendMessage handle SCI_SETBUFFEREDDRAW as integer! is-buffered? 0
	]

	get-buffered-draw: routine [
		handle		[integer!]
		return: 	[logic!]
	] [
		as logic! SendMessage handle SCI_GETBUFFEREDDRAW 0 0
	]

	set-phases-draw: routine [
		handle		[integer!]
		phases		[integer!]
	] [
		SendMessage handle SCI_SETPHASESDRAW phases 0
	]

	get-phases-draw: routine [
		handle		[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_GETPHASESDRAW 0 0
	]

	set-two-phase-draw: routine [
		handle		[integer!]
		two-phase?	[logic!]
	] [
		SendMessage handle SCI_SETTWOPHASEDRAW as integer! two-phase? 0
	]

	get-two-phase-draw: routine [
		handle		[integer!]
		return: 	[logic!]
	] [
		as logic! SendMessage handle SCI_GETTWOPHASEDRAW 0 0
	]

	set-technology: routine [
		handle		[integer!]
		technology	[integer!]
	] [
		SendMessage handle SCI_SETTECHNOLOGY technology 0
	]

	get-technology: routine [
		handle		[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_GETTECHNOLOGY 0 0
	]

	set-font-quality: routine [
		handle 			[integer!]
		font-quality	[integer!]
	] [
		SendMessage handle SCI_SETFONTQUALITY font-quality 0
	]

	get-font-quality: routine [
		handle		[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_GETFONTQUALITY 0 0
	]
	
	set-code-page: routine [
		handle			[integer!]
		code-page 		[integer!]
	] [
		SendMessage handle SCI_SETCODEPAGE code-page 0
	]

	get-code-page: routine [
		handle			[integer!]
		return: 		[integer!]
	] [
		SendMessage handle SCI_GETCODEPAGE 0 0
	]

	set-ime-interaction: routine [
		handle			[integer!]
		ime-interaction [integer!]
	] [
		SendMessage handle SCI_SETIMEINTERACTION ime-interaction 0
	]

	get-ime-interaction: routine [
		handle		[integer!]
		return: 	[integer!]
	] [
		SendMessage handle SCI_GETIMEINTERACTION 0 0
	]

	set-word-chars: routine [
		handle		[integer!]
		characters 	[string!]
		/local
			length 	[integer!]
			c-text 	[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 characters :length
		;sci-system/SETWORDCHARS handle c-text 
		SendMessage handle SCI_SETWORDCHARS 0 as integer! c-text
	]

	; FIXME: do the conversion before return
	get-word-chars: routine [
		handle		[integer!]
		/local 
			text 	[c-string!]
			str  	[red-string!]
	] [
		text: as c-string! allocate 256 ; FIXME: is it enough? What about Unicode?
		SendMessage handle SCI_GETWORDCHARS 0 as integer! text
		; str: string/load text length? text UTF-8
		; free as byte-ptr! text
		; SET_RETURN(str)
	]

	set-whitespace-chars: routine [
		handle		[integer!]
		characters 	[string!]
		/local
			length 	[integer!]
			c-text 	[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 characters :length
;		sci-system/SETWHITESPACECHARS handle c-text 
		SendMessage handle SCI_SETWHITESPACECHARS 0 as integer! c-text
	]

	; FIXME: do conversion before return
	get-whitespace-chars: routine [
		handle		[integer!]
		/local 
			text 	[c-string!]
			str  	[red-string!]
	] [
;		text: sci-system/GETWHITESPACECHARS handle
		text: as c-string! allocate 256 ; FIXME: is it enough? What about Unicode?
		SendMessage handle SCI_GETWHITESPACECHARS 0 as integer! text
		; str: string/load text length? text UTF-8
		; free as byte-ptr! text
		; SET_RETURN(str)
	]

	set-punctuation-chars: routine [
		handle		[integer!]
		characters 	[string!]
		/local
			length 	[integer!]
			c-text 	[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 characters :length
;		sci-system/SETPUNCTUATIONCHARS handle c-text 
		SendMessage handle SCI_SETPUNCTUATIONCHARS 0 as integer! characters
	]

	get-punctuation-chars: routine [
		handle		[integer!]
		/local 
			text 	[c-string!]
			str  	[red-string!]
	] [
;		text: sci-system/GETPUNCTUATIONCHARS handle
		text: as c-string! allocate 256 ; FIXME: is it enough? What about Unicode?
		as c-string! SendMessage handle SCI_GETPUNCTUATIONCHARS 0 as integer! text
		; str: string/load text length? text UTF-8
		; free as byte-ptr! text
		; SET_RETURN(str)
	]

	set-chars-default: routine [
		handle		[integer!]
	] [
		SendMessage handle SCI_SETCHARSDEFAULT 0 0
	]

	grab-focus: routine [
		handle		[integer!]
	] [
		SendMessage handle SCI_GRABFOCUS 0 0
	]

	set-focus: routine [
		handle		[integer!]
		focus? 	[logic!]
	] [
		SendMessage handle SCI_SETFOCUS as integer! focus? 0
	]

	get-focus: routine [
		handle		[integer!]
		return: 	[logic!]
	] [
		as logic! SendMessage handle SCI_GETFOCUS 0 0
	]

	set-property: routine [
		handle 		[integer!]
		key 		[string!]
		value 		[string!]
		/local
			length 	[integer!]
			k-text 	[c-string!]
			v-text 	[c-string!]
	] [
		length: -1
		k-text: unicode/to-utf8 key :length
		v-text: unicode/to-utf8 value :length
		SendMessage handle SCI_SETPROPERTY as integer! k-text as integer! v-text
	]

	get-property: routine [
		handle 		[integer!]
		key 		[string!]
		return: 	[string!]
		/local
			length 	[integer!]
			c-text 	[c-string!]
			text 	[c-string!]
	] [
		length: -1
		c-text: unicode/to-utf8 key :length
		text: as-c-string allocate 64 ; 64 bytes should be enough for everyone
		length: SendMessage handle SCI_GETPROPERTY as integer! c-text as integer! text
		string/load text length UTF-8
	]

; === Custom routines ========================================================

	first-char-on-line: routine [
		"Return first non-whitespace character"
		handle 		[integer!]
		line 		[integer!]
		return: 	[integer!]
		/local
			string 	[c-string!]
			pos 	[integer!]
			length 	[integer!]
	] [
		length: 1 + SendMessage handle SCI_LINELENGTH line - 1 0
		string: as c-string! allocate length
		SendMessage handle SCI_GETLINE line - 1 as integer! string
		pos: 0
		until [
			pos: pos + 1
			any [
				pos = length
				all [string/pos <> #" " string/pos <> #"^-"]
			]
		]
		either pos = length [0] [as integer! string/pos]
	]

	get-current-line: routine [
		handle 			[integer!]
		return: 		[integer!]
	] [
		line-from-position handle get-current-position handle
	]

	toggle-marker: routine [
		handle 		[integer!]
		line 		[integer!]
		marker 		[integer!]		
		/local
			mask 	[integer!]
	] [
		; click in bookmark margin
	;	mask: sci-system/MARKERGET handle line
		mask: SendMessage handle SCI_MARKERGET line - 1 0
		either zero? mask [
			; marker does not exist, add it
	;		sci-system/MARKERADD handle line marker
			SendMessage handle SCI_MARKERADD line - 1 marker - 1
		] [
			; marker does exist, remove it
	;		sci-system/MARKERDELETE handle line marker 
			SendMessage handle SCI_MARKERDELETE line - 1 marker - 1
		]
	]
]