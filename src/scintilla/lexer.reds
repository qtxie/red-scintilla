Red/System [
	Title:		"Red Lexer"
	Author: 	"Boleslav Březovský"
	File:		%lexer.reds
	Known-bugs: [
		"RS-KEYWORD-RULE can crash with ACCESS VIOLATION error"
	]
	Note: "Not used now as we use the Red Lexer inside SciLexer.dll"
]

large-text-buffer: as c-string! allocate 2'097'152 	; 2MB for scripts (should be enough, but checks should be added later) 

matched?: func [
	value 		[integer!]
	pattern 	[integer!]
	return: 	[logic!]
] [
	pattern = (value and pattern)
]

eol?: func [
	char 		[byte!]
	return: 	[logic!]
] [
	any [
		char = as byte! 13 char = as byte! 10 		; cr, lf
		char = as byte! 0 							; eof
	]
]

delimiter?: func [
	string 		[c-string!]
	return: 	[logic!]
	/local
		char 		[byte!]
] [
	char: string/1
	any [
		all [char >= as byte! 0 char <= as byte! 32]
		char = #"(" char = #")"
		char = #"[" char = #"]"
		char = #"{" char = #"}"
		char = #"^"" char = #";"
		char = as byte! 127
	]
]

brace-end?: func [
	char 		[integer!]
	return: 	[logic!]
] [
	any [char = 41 char = 93 char = 125]
]

match-string?: func [
	"Check if the beginning of the string matches given substring"
	string 		[c-string!]
	substring	[c-string!]
	return:		[logic!]
][
	; main loop
	until [
		if string/1 <> substring/1 [return false]
	; end condition
		string: string + 1
		substring: substring + 1
		substring/1 = null-byte
	]
	true
]

match-string-case?: func [
	"Check if the beginning of the string matches given substring (case insensitive)"
	string 		[c-string!]
	substring	[c-string!]
	return:		[logic!]
	/local
		match? 	[logic!] 		
][
	match?: false
	; main loop
	until [
		match?: false
		if any [
			string/1 = substring/1
			(string/1 + 32) = substring/1
			string/1 = (substring/1 + 32)
		] [match?: true]
		unless match? [return false]
	; end condition
		string: string + 1
		substring: substring + 1
		substring/1 = null-byte
	]
	true
]

get-word: func [
	"Return word on current position"
	handle 		[integer!]
	position 	[integer!]
	text 		[c-string!] "Buffer for result"
] [
	get-range handle
		1 + SendMessage handle SCI_WORDSTARTPOSITION position as integer! true
		1 + SendMessage handle SCI_WORDENDPOSITION position as integer! true
		text
	text
]

find-string: func [
	"Find substring in string"
	string	[c-string!]
	match	[c-string!]
	return:	[c-string!]	; return index
][
	; main loop
	until [
		if match-string? string match [
			return string
		]
	;	end condition
		string: string + 1
		string/1 = null-byte
	]
	; no match, return empty string
	""
]

found-char?: func [
	"Find char in string"
	string			[c-string!]
	match			[byte!]
	return:			[logic!]
][
	; main loop
	until [
		if match = string/1 [return true]
	;	end condition
		string: string + 1
		string/1 = null-byte
	]
	; no match, return empty string
	false
]

match-line-end: func [
	"Match CR/LF or end of text"
	string 			[c-string!]
	return: 		[integer!]
] [
	either any [
		string/1 = as byte! 10
		string/1 = as byte! 13
		string/1 = as byte! 0
	] [
		as integer! string + 1
	] [-1]
]

skip-to: func [
	string 			[c-string!]
	char 			[byte!]
	return: 		[c-string!]
] [
	until [
		if char = string/1 [return string]
		string: string + 1
		string/1 = null-byte
	]
	""
]

copy-to: func [
	string 			[c-string!]
	char 			[byte!]
	return: 		[c-string!]
	/local
		index 		[integer!]
		length 		[integer!]
		output 		[c-string!]
] [
	length: (as integer! skip-to string char) - (as integer! string) + 1
	if length < 1 [return ""]
	output: as c-string! allocate length
	index: 1
	until [
		output/index: string/index
		index: index + 1
		index = length
	]
;	length: length + 1
	output/length: null-byte
	output
]

init-array: func [
	length 		[integer!]
	return: 	[byte-ptr!]
	/local
		array 	[byte-ptr!]
		index 	[integer!]
] [
	array: allocate length
	index: 1
	while [index <= length] [
		array/index: as byte! 0
		index: index + 1
	]
	array
]

set-bit: func [
	array 		[byte-ptr!]
	bit 	 	[integer!]
	value 		[logic!]
	/local
		pos 	[byte-ptr!]

] [
	pos: array + (bit >> 3)
	pos/value: either value [
		; set bit
		pos/value or (as-byte 128 >> (bit and 7))
	] [
		; reset bit
		pos/value and (as-byte 128 >> (bit and 7) xor 255)
	]
]

get-bit: func [
	array 		[byte-ptr!]
	bit 	 	[integer!]
	return: 	[logic!]
	/local
		pos 	[byte-ptr!]
] [
	pos: array + (bit >> 3)
	pos/value and (as-byte 128 >> (bit and 7)) <> null-byte
]


#enum lexer-styles! [
	STYLE-DEFAULT
	STYLE-KEYWORD
	STYLE-STRING
	STYLE-NUMBER
	STYLE-COMMENT
	STYLE-OPERATOR
	STYLE-WORD
	STYLE-SET-WORD
	STYLE-LIT-WORD
	STYLE-GET-WORD
	STYLE-PATH
	STYLE-SET-PATH
	STYLE-GET-PATH
	STYLE-LIT-PATH
	STYLE-REFINEMENT
	STYLE-BINARY
	STYLE-EMAIL
	STYLE-FILE
	STYLE-URL
	STYLE-TUPLE
	STYLE-ISSUE
	STYLE-CHARACTER
	STYLE-DATE
	STYLE-LOGIC
	STYLE-MONEY
	STYLE-PAIR
	STYLE-TIME
	STYLE-DATATYPE
	STYLE-WHITESPACE
	STYLE-BRACE
]

rs-keywords: "if unless either loop until while break continue any all case switch throw catch push pop assert alias declare length? size? zero? as print print-line return func function with context does"

; === RULES

whitespace-rule: func [
	string 			[c-string!]
	return: 		[integer!]
] [
	as integer! whitespace? string/1
]

digit-rule: func [
	string 			[c-string!]
	return: 		[integer!]
] [
	either all [string/1 >= #"0" string/1 <= #"9"] [1] [0]
]

hex-digit-rule: func [
	string 			[c-string!]
	return:		[integer!]
] [
	either any [
		all [string/1 >= #"0" string/1 <= #"9"]
		all [string/1 >= #"a" string/1 <= #"f"]
		all [string/1 >= #"A" string/1 <= #"F"]
	] [1] [0]
]

alpha-rule: func [
	string 			[c-string!]
	return:		[integer!]
] [
	either any [
		all [string/1 >= #"a" string/1 <= #"z"]
		all [string/1 >= #"A" string/1 <= #"Z"]
	] [1] [0]
]

word-sign-rule: func [
	string 			[c-string!]
	return:			[integer!]
] [
	either found-char? "?!`*&|=_~^^'+-." string/1 [1] [0]
]

word-rule: func [
	string 			[c-string!]
	return:			[integer!]
	/local
		start 		[c-string!]
		matched? 	[logic!]
] [
	start: string
	if 
	all [
		zero? alpha-rule string
		zero? word-sign-rule string
	] 
	[return 0]
	string: string + 1
	matched?: false
	until [
		matched?: false
		unless zero? alpha-rule string [matched?: true string: string + 1 continue]
		unless zero? digit-rule string [matched?: true string: string + 1 continue]
		unless zero? word-sign-rule string [matched?: true string: string + 1 continue]		
		unless matched? [break]
		false
	]
	as integer! string - start
]

set-word-rule: func [
	string 			[c-string!]
	return:		[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	string: string + word-rule string
	either #":" = string/1 [as integer! string - start + 1] [0]
]

lit-word-rule: func [
	string 			[c-string!]
	return:		[integer!]
	/local
		start 		[c-string!]
		word 		[integer!]
] [
	start: string
	unless #"'" = string/1 [return 0]
	string: string + 1
	word: word-rule string
	if zero? word [return 0]
	as integer! string + word - start
]

get-word-rule: func [
	string 			[c-string!]
	return:		[integer!]
	/local
		start 		[c-string!]
		word 		[integer!]
] [
	start: string
	unless #":" = string/1 [return 0]
	string: string + 1
	word: word-rule string
	if zero? word [return 0]
	as integer! string + word - start
]

issue-rule: func [
	string 			[c-string!]
	return:		[integer!]
	/local
		start 		[c-string!]
		word 		[integer!]
] [
	start: string
	unless #"#" = string/1 [return 0]
	string: string + 1
	word: word-rule string
	if zero? word [return 0]
	as integer! string + word - start
]

refinement-rule: func [
	string 			[c-string!]
	return:			[integer!]
	/local
		start 		[c-string!]
		word 		[integer!]
] [
	start: string
	unless #"/" = string/1 [return 0]
	string: string + 1
	word: word-rule string
	if zero? word [return 0]
	as integer! string + word - start
]

datatype-rule: func [
	string 			[c-string!]
	return:			[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	string: string - 1 + word-rule string
	either #"!" = string/1 [as integer! string - start + 1] [0]
]

file-rule: func [
	string 			[c-string!]
	return:			[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	unless #"%" = string/1 [return 0]
	string: string + 1
	; check for allowed characters (very simple version, needs improving)
	while [not delimiter? string] [string: string + 1]
	either string = (start + 1) [0] [as integer! string - start]
]

operator-rule: func [
	string 			[c-string!]
	return: 		[integer!]
] [
	case [
		all [
			whitespace? string/2
			found-char? "+-*/<>=?" string/1
		] [
			1
		]
		all [
			whitespace? string/3
			any [
				match-string? string "**"
				match-string? string "//"
				match-string? string "<<"
				match-string? string "<="
				match-string? string ">>"
				match-string? string "=>"
				match-string? string "<>"
				match-string? string "=="
				match-string? string "=?"
				match-string? string "??"
			]
		] [
			2
		]
		true [0]
	]
]

comment-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	unless #";" = string/1 [return 0]
	until [
		string: string + 1
		eol? string/1
	]
	as integer! string - start
]

string-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		matched? 	[logic!]
] [
	start: string
	matched?: false
	; match first "
	unless #"^"" = string/1 [return 0]
	string: string + 1
	until [
		if  #"^"" = string/1 [
			matched?: true
			string: string + 1
			break
		]
		string: string + 1
		eol? string/1
	] 
	either matched? [as integer! string - start] [0] 
]

multiline-string-rule: func [	
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		level 		[integer!]
][
	start: string
	level: 1 ; level of inner braces (will be zero after all brace match)
	unless #"{" = string/1 [return 0]
	string: string + 1
	until [
		case [
			; eof (matched as string)
			#"^@" = string/1 [
				return as integer! string - start
			]
			; escaped braces
			all [
				#"^^" = string/1 
				any [#"{" = string/2 #"}" = string/2]
			] [string: string + 2]
			; inner braces (must match)
			#"{" = string/1 [
				level: level + 1
				string: string + 1
			]
			; closing brace
			#"}" = string/1 [
				level: level - 1
				string: string + 1
			]
			; everything else
			true 	[string: string + 1]
		]
		; UNTIL condition
		zero? level
	]
	as integer! string - start
]

hex-number-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	while [not zero? hex-digit-rule string] [string: string + 1]
	if start = string [return 0]
	either any [
		all [#"h" = string/1 whitespace? string/2]
		all [#"H" = string/1 whitespace? string/2]
	] [as integer! string - start + 1] [0]
]

integer-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	while [not zero? digit-rule string] [
		string: string + 1
		if #"'" = string/1 [
			string: string  + 1
		]
	]
	as integer! string - start
]

tuple-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		elements 	[integer!]
		count 		[integer!]
] [
	start: string
	elements: 1
	count: integer-rule string
	while [
		all [
			not zero? count
			not zero? as integer! string/1
		]
	] [
		if any [
			count = 0
			count > 3
			all [count = 3 string/1 > #"2"]
			all [count = 3 string/1 = #"2" string/2 > #"5"]
			all [count = 3 string/1 = #"2" string/2 = #"5" string/3 > #"5"]
			elements > 8
		] [return 0]
		string: string + count
		if #"." = string/1 [
			string: string + 1
			elements: elements + 1
			count: integer-rule string
			continue
		]
		count: 0 ; if we are here, number is not a tuple
	]
	either elements > 2 [as integer! string - start] [0]
]

sign-rule: func [
	string 			[c-string!]
	return:  		[integer!]
] [
	as integer! any [#"+" = string/1 #"-" = string/1]
]

float-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		pos 		[c-string!]
] [
	start: string
	string: string + integer-rule string
	if string = start [return 0]
	if all [string/1 <> #"." string/1 <> #","]  [return 0]
	string: string + 1
	string: string + integer-rule string
	as integer! string - start
]

scientific-notation-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		pos 		[c-string!]
		start 		[c-string!]
] [
	start: string
	unless any [
		#"e" = string/1
		#"E" = string/1
	] [return 0]
	string: string + 1
	string: string + sign-rule string
	pos: string
	string: string + integer-rule string
	if string = pos [return 0]
	as integer! string - start
]

basic-number-rule: func [
	"Matches unsigned integer or float number"
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	string: string + float-rule string
	if start = string [string: string + integer-rule string]
	as integer! string - start
]

number-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		count 		[integer!]
] [
	start: string
	string: string + sign-rule string
	count: basic-number-rule string
	if count = 0 [return 0]
	string: string + count
	string: string + scientific-notation-rule string
	as integer! string - start
]

pair-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		pos 		[c-string!]
] [
	start: string
	string: string + sign-rule string
	string: string + integer-rule string
	if string = start [return 0]
	unless any [#"x" = string/1 #"X" = string/1] [return 0]
	string: string + 1
	string: string + sign-rule string
	pos: string
	string: string + integer-rule string
	if string = pos [return 0]
	as integer! string - start
]

ampm-rule: func [
	string 			[c-string!]
	return: 		[integer!]
] [
	either all [
		found-char? "aApP" string/1
		found-char? "mM" string/2
	] [2] [0]
]

time-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		pos 		[c-string!]
		last?		[logic!]
] [
	; FIXME: "12:" is valid time
	last?: false
	start: string
	string: string + sign-rule string
	string: string + integer-rule string
	if any [
		string = start
		#":" <> string/1
	] [return 0]
	string: string + 1
	pos: string
	; TODO: there has to be way to do optional rule without temp variable
	string: string + float-rule string
	either string = pos [
		string: string + integer-rule string
	] [
		last?: true
	]
	if all [not last? #":" = string/1] [
		string: string + 1
		string: string + basic-number-rule string
		string: string + ampm-rule string
	]
	as integer! string - start
]

day-rule: func [
	"Check for (0)0-31"
	string 			[c-string!]
	return: 		[integer!]
] [
	; FIXME: 00 will pass
	case [
		all [#"3" = string/1 any [#"0" = string/2 #"1" = string/2]]					[2]
		all [#"0" <= string/1 #"2" >= string/1 #"0" <= string/2 #"9" >= string/2] 	[2]
		all [#"0" <= string/1 #"9" >= string/1]										[1]
		true 																		[0]
	]
]

month-number-rule: func [
	"Check for (0)1-12"
	string 			[c-string!]
	return: 		[integer!]
] [
	case [
		all [#"1" = string/1 #"0" <= string/2 #"2" >= string/2]		[2]
		all [#"0" = string/1 #"1" <= string/2 #"9" >= string/2] 	[2]
		all [#"1" <= string/1 #"9" >= string/1]						[1]
		true 														[0]
	]
]

month-word-rule: func [
	string 			[c-string!]
	return: 		[integer!]
] [
	either any [
		match-string-case? string "jan"
		match-string-case? string "feb"
		match-string-case? string "mar"
		match-string-case? string "apr"
		match-string-case? string "may"
		match-string-case? string "jun"
		match-string-case? string "jul"
		match-string-case? string "aug"
		match-string-case? string "sep"
		match-string-case? string "oct"
		match-string-case? string "nov"
		match-string-case? string "dec"
	] [3] [0]
]

year-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		length 	[integer!]	
] [
	start: string
	while [not zero? digit-rule string] [string: string + 1]
	length: as integer! string - start
	either any [length = 2 length = 4] [length] [0]
]

date-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		delimiter 	[byte!]
		count 		[integer!]
] [
	; TODO: Year can be first
	start: string
	string: string + day-rule string
	delimiter: string/1
	unless found-char? "-/" delimiter [return 0]
	string: string + 1
	count: month-number-rule string
	if zero? count [count: month-word-rule string]
	if zero? count [return 0]
	string: string + count
	unless delimiter = string/1 [return 0]
	string: string + 1
	count: year-rule string
	if zero? count [return 0]
	string: string + count
	as integer! string - start
]

binary-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	case [
		match-string? string "#{"		[string: string + 2]
		match-string? string "2#{" 		[string: string + 3]
		match-string? string "64#{" 	[string: string + 4]
		true [true]
	]
	if start = string [return 0]
	while [true] [
		if #"}" = string/1 [break]
		if whitespace? string/1 [return 0]
		string: string + 1
	]
	as integer! string - start + 1
]

path-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		matched? 	[logic!]
		matched 	[c-string!]
] [
	start: string
	string: string + word-rule string
	matched?: false
	matched: string
	if all [
		string > start
		#"/" = string/1
	] [
		string: string + 1
		while [true] [
		; TODO: how to assign result of *-rule so I don't have to call it twice?
			case [
				not zero? word-rule string [
					string: string + word-rule string
					matched?: true
				]
				not zero? get-word-rule string [
					string: string + get-word-rule string
					matched?: true
				]
				not zero? integer-rule string [
					string: string + integer-rule string
					matched?: true
				]
				#"/" = string/1 [
					string: string + 1
					matched?: false
				]
				true [
					break
				]
			]
		]
	]
	either all [string > matched  matched?] [as integer! string - start] [0]
]

set-path-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	string: string + path-rule string
	either #":" = string/1 [as integer! string - start + 1] [0] 
]

get-path-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	either #":" = string/1 [
		string: string + 1
		string: string + path-rule string
		as integer! string - start
	] [0] 
]

lit-path-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
] [
	start: string
	either #"'" = string/1 [
		string: string + 1
		string: string + path-rule string
		as integer! string - start
	] [0] 
]

character-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		count 		[integer!]
		keywords 	[c-string!]
] [
	keywords: "null line tab page esc back del"
	unless all [
		#"#" = string/1
		#"^"" = string/2
	] [return 0]
	case [
		all [string/3 >= as byte! 32 string/3 <= as byte! 127 string/4 = #"^""] [4]
		all [
			string/3 = #"^^"
			any [
				all [string/4 >= #"a" string/4 <= #"z"]
				all [string/4 >= #"A" string/4 <= #"Z"]
				found-char? "/.-@^^^"" string/4
			] 
			string/5 = #"^""
		] [5]
		all [
			string/3 = #"^^"
			string/4 = #"("
		] [
			string: string + 4
			count: keyword-rule string keywords
			either count > 0 [
				string: string + count
				either all [#")" = string/1 #"^"" = string/2] [count + 6] [0]
			] [
				count: 1
				while [not zero? hex-digit-rule string] [
					string: string + 1 
					count: count + 1
				]
				either all [#")" = string/1 #"^"" = string/2 count > 2 count < 7] [count + 6] [0] 
			]
		]
		true [0]
	]
]

email-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		matched? 	[logic!]
		start 		[c-string!]
		count 		[integer!]
] [
	; NOTE: Parses @handle as email! which should be supported anyway IMO ;-) 
	start: string
	matched?: false
	while [string/1 > #" "] [
		if string/1 = #"@" [matched?: true]
		string: string + 1
	]
	count: as integer! string - start
	either all [
		matched?
		count > 1
	] [count] [0]
]

url-rule: func [
	string 			[c-string!]
	return: 		[integer!]
	/local
		start 		[c-string!]
		pos 		[c-string!]
] [
	start: string
	while [not zero? alpha-rule string] [string: string + 1]
	if any [
		string = start
		string/1 <> #":"
	] [return 0]
	string: string + 1
	pos: string
	while [string/1 > #" "] [string: string + 1]
	either pos = string [0] [as integer! string - start]
]

keyword-rule: func [
	string 			[c-string!]
	keywords 		[c-string!] "List of keywords separated by whitespaces"
	return: 		[integer!]
	/local
		matched? 	[logic!]
		submatched? [logic!]
		index 		[integer!]
] [
	; TODO: skip more than one whitespace
	; TODO: case-insensitive match
	index: 1
	matched?: false
	submatched?: true
	while [
		not matched?
	] [
		if #"^@" = keywords/1 [return 0] ; nothing matched
		if whitespace? keywords/1 [
			; keyword ended
			if submatched? [
				matched?: true
				continue
			]
			; keyword not matched, rewind string and check next keyword
			index: 1
			submatched?: true
			keywords: keywords + 1
			continue
		]
		submatched?: submatched? and (string/index = keywords/1)
		; TODO: if not submatched, fast forward to another keyword
		keywords: keywords + 1
		index: index + 1
	]
	return index - 1
]

rs-keyword-rule: func [
	string 		[c-string!]
	return: 	[integer!]
] [
	keyword-rule string rs-keywords
]

; =================================================

match?: func [
	line-text 	[c-string!]
	count 		[integer!]
	return: 	[logic!]
] [
	if all [
		not zero? count
		any [start? delimiter? line-text - 1] ; check if delimiter precedes
		delimiter? line-text + count 
	] [
		bytes-matched: count
		return true
	]
	false
]

stylize: func [
	handle 		[integer!]
	count 		[integer!]
	style 		[integer!]
	return: 	[integer!]
] [
	SendMessage handle SCI_SETSTYLING count style
	count
]

; TODO: should be in some struct! (but that's throwing compilation problems)
bytes-matched: 0
start?: true
line-buffer: as c-string! allocate 1024 ; TODO: Free on exit
; NOTE: Line buffer has hardcoded size of 1024 bytes here. What if we want longer lines?

probe-bitmap: func [
	bitmap 				[byte-ptr!]
	length 				[integer!]
	/local
		index 			[integer!]
] [
	index: 1
	while [index <= length] [
		print either get-bit bitmap index ["x"] ["."]
		index: index + 1
	]
	print-line ""
]

stylize-line: func [
	handle 				[integer!]
	line 				[integer!]
] [
	stylize-range 
		handle
;		sci-system/POSITIONFROMLINE handle line
;		sci-system/LINELENGTH handle line
		1 + SendMessage handle SCI_POSITIONFROMLINE line - 1 0
		SendMessage handle SCI_LINELENGTH line 0 
]


; TODO: rewrite using GETTEXTRANGE
stylize-range: func [
	handle 				[integer!]
	position 			[integer!]
	length 				[integer!]
	/local
		text 			[c-string!]
		cur-position 	[integer!]
		end-position 	[integer!]
		doc-end 		[integer!]
		start 			[integer!]
		end 			[integer!]
		text-range 		[text-range!]

		ret 			[integer!]
] [
;	text: as c-string! allocate length + 1
	SendMessage handle SCI_STARTSTYLING position - 1 0
	doc-end: 1 + SendMessage handle SCI_GETLENGTH 0 0
	end-position: position + length
	cur-position: 1 + SendMessage handle SCI_GETCURRENTPOS 0 0
	if end-position > doc-end [end-position: doc-end]
	text-range: declare text-range!
	text-range/text: large-text-buffer
	text-range/index-from: position - 1
	text-range/index-to: end-position - 1
	ret: SendMessage handle SCI_GETTEXTRANGE 0 as integer! text-range
	text: text-range/text
;-
	start: find-string-start handle 1 position
	; exception for multiline strings
	if all [
		not zero? start 
		start <= position
	] [
		print-line ">> multiline exception"
		end: 1 + SendMessage handle SCI_BRACEMATCH start - 1 0
		if zero? end [end: doc-end]
		stylize handle end - start + 1 STYLE-STRING
		exit
	]

	start?: true ; WARN: GLOBAL VAR (currently)
	until [
		case [
			; TODO: just set style and do styling after CASE
			; FIXME: RS-KEYWORD-RULE can break with ACCESS VIOLATION
		;	match? line-text rs-keyword-rule line-text 	[line-text: line-text + stylize handle bytes-matched STYLE-KEYWORD]

			match? text multiline-string-rule text 	[text: text + stylize handle bytes-matched STYLE-STRING] 		; 2
			match? text string-rule text 			[text: text + stylize handle bytes-matched STYLE-STRING] 		; 2
			match? text character-rule text 		[text: text + stylize handle bytes-matched STYLE-CHARACTER] 	; 21
			match? text binary-rule text 			[text: text + stylize handle bytes-matched STYLE-BINARY]		; 15
			match? text operator-rule text 			[text: text + stylize handle bytes-matched STYLE-OPERATOR]		; 5
			match? text path-rule text 				[text: text + stylize handle bytes-matched STYLE-PATH] 			; 10
			match? text set-path-rule text 			[text: text + stylize handle bytes-matched STYLE-SET-PATH] 		; 11
			match? text get-path-rule text 			[text: text + stylize handle bytes-matched STYLE-GET-PATH] 		; 12
			match? text lit-path-rule text 			[text: text + stylize handle bytes-matched STYLE-LIT-PATH] 		; 13
			match? text datatype-rule text 			[text: text + stylize handle bytes-matched STYLE-DATATYPE] 		; 27 ; NOTE: must be before WORD-RULE (something! is valid word)
			match? text email-rule text 			[text: text + stylize handle bytes-matched STYLE-EMAIL] 		; 16
			match? text url-rule text 				[text: text + stylize handle bytes-matched STYLE-URL] 			; 18
			match? text word-rule text 				[text: text + stylize handle bytes-matched STYLE-WORD] 			; 6
			match? text set-word-rule text			[text: text + stylize handle bytes-matched STYLE-SET-WORD] 		; 7
			match? text get-word-rule text 			[text: text + stylize handle bytes-matched STYLE-GET-WORD] 		; 8
			match? text lit-word-rule text 			[text: text + stylize handle bytes-matched STYLE-LIT-WORD] 		; 9
			match? text issue-rule text 			[text: text + stylize handle bytes-matched STYLE-ISSUE] 		; 20
			match? text refinement-rule text 		[text: text + stylize handle bytes-matched STYLE-REFINEMENT] 	; 14
			match? text file-rule text 				[text: text + stylize handle bytes-matched STYLE-FILE] 			; 17
			match? text tuple-rule text 			[text: text + stylize handle bytes-matched STYLE-TUPLE] 		; 19
			match? text date-rule text 				[text: text + stylize handle bytes-matched STYLE-DATE] 			; 22
			match? text time-rule text 				[text: text + stylize handle bytes-matched STYLE-TIME] 			; 26
			match? text number-rule text 			[text: text + stylize handle bytes-matched STYLE-NUMBER] 		; 3
			match? text hex-number-rule text 		[text: text + stylize handle bytes-matched STYLE-NUMBER] 		; 3
			match? text pair-rule text 				[text: text + stylize handle bytes-matched STYLE-PAIR] 			; 25
			match? text comment-rule text 			[text: text + stylize handle bytes-matched STYLE-COMMENT] 		; 4

			; missing: 23 (logic), 24 (money)

			; match braces
			any [
				#"[" = text/1
				#"]" = text/1
				#"(" = text/1
				#")" = text/1
			] [
			;	sci-system/SETSTYLING handle 1 STYLE-BRACE
				SendMessage handle SCI_SETSTYLING 1 STYLE-BRACE
				text: text + 1
			]

			; match whitespace
			whitespace? text/1 [	
			;	sci-system/SETSTYLING handle 1 STYLE-WHITESPACE
				SendMessage handle SCI_SETSTYLING 1 STYLE-WHITESPACE
				text: text + 1
			]

			; everything else
			true [
				text: text + stylize handle 1 0
			]
		]
		start?: false
		any [
			0 = as integer! text/1
			position > end-position
		]
	]
]

do-styling: func [
	notification	[SCNotification!]
	/local
		handle 		[integer!]
		line 		[integer!]
		position 	[integer!]
] [
	handle: as integer! notification/hwndFrom ; TODO: macro?
;	line: sci-system/LINEFROMPOSITION handle notification/position ; gets line from SCNotification!
;	line: sci-system/LINEFROMPOSITION handle sci-system/GETCURRENTPOS handle
	position: SendMessage handle SCI_GETCURRENTPOS 0 0
	line: 1 + SendMessage handle SCI_LINEFROMPOSITION position 0
	stylize-line handle line
]
