Red/System [
	Title: "Scintilla exter"
]

get-line-start: func [
	handle 		[integer!]
	position 	[integer!]
	return: 	[integer!]
	/local
		line 	[integer!]
] [
	line: SendMessage handle SCI_LINEFROMPOSITION position - 1 0
	1 + SendMessage handle SCI_POSITIONFROMLINE line 0
]

get-current-line: func [
	handle 		[integer!]
	return: 	[integer!]
	/local
		position [integer!]
] [
	position: SendMessage handle SCI_GETCURRENTPOS 0 0
	1 + SendMessage handle SCI_LINEFROMPOSITION position 0
]

get-range: func [
	handle 			[integer!]
	index-from 		[integer!]
	index-to 		[integer!]
	text 			[c-string!]
	return: 		[c-string!]
	/local
		text-range 	[text-range!]
		c-text 		[c-string!]
] [
	text-range: declare text-range!
	text-range/text: text
	text-range/index-from: index-from - 1
	text-range/index-to: index-to - 1
	SendMessage handle SCI_GETTEXTRANGE 0 as integer! text-range
	text-range/text
]

search-in-target: func [
	handle 		[integer!]
	string 		[c-string!]
	start 		[integer!]
	end 		[integer!]
	return: 	[integer!]
] [
	SendMessage handle SCI_SETTARGETRANGE start - 1 end - 1
	1 + SendMessage handle SCI_SEARCHINTARGET length? string as integer! string
]

; NOTE: move to lexer?
highlight-braces: func [
	handle 		[integer!]
	position 	[integer!]
	/local
		start 	[integer!]
		end 	[integer!]
		column 	[integer!]
] [
	; TODO: maybe cache the results for faster behaviour? is it worth it?
	start: find-brace-start handle position
	end: 1 + SendMessage handle SCI_BRACEMATCH start - 1 0
	column: 1 + SendMessage handle SCI_GETCOLUMN end - 1 0

	; check if the matches are not in comment or in string
	if any [
		not zero? find-comment-start handle start
		not zero? find-comment-start handle end
		in-simple-string? handle start
		in-simple-string? handle end
	] [exit] 

	; colorize
	either zero? end [
		SendMessage handle SCI_BRACEBADLIGHT start - 1 0
		SendMessage handle SCI_SETHIGHLIGHTGUIDE column 0
	] [
		SendMessage handle SCI_BRACEHIGHLIGHT start - 1 end - 1
		SendMessage handle SCI_SETHIGHLIGHTGUIDE column 0

	]	
]

in-simple-string?: func [
	handle 		[integer!]
	position 	[integer!]
	return: 	[logic!]
	/local
		line 		[integer!]
		count 		[integer!]
		start 		[integer!]
] [
	; TODO: ignore false positives: ^"
	count: 0
	start: get-line-start handle position
	until [
		start: search-in-target handle "^"" start position
		either zero? start [
			break
		] [
			count: count + 1
			start: start + 1
			continue
		]
		; there's no end condition, it's handled in the EITHER blocks
		true
	]
	; [count and 1] returns 1 in case of odd and 0 in case of even numbers
	; odd number of " means that the last string is not closed
	as logic! count and 1
]

in-multiline-string?: func [
	handle 		[integer!]
	position 	[integer!]
	return: 	[logic!]
	/local
		start 	[integer!]
		end 	[integer!]
] [
	start: find-string-start handle 1 position
	end: 1 + SendMessage handle SCI_BRACEMATCH start - 1 0
	all [
		start < position
		not zero? end
	]
]

whitespace?: func [
	char 		[byte!]
	return: 	[logic!]
] [
	any [
		char = as byte! 32 							; space
		char = as byte! 9 							; tab
		char = as byte! 13 char = as byte! 10 		; cr, lf
		char = as byte! 0 							; eof
	]
]

brace-start?: func [
	char 		[integer!]
	return: 	[logic!]
] [
	any [char = 40 char = 91 char = 123]
]

new-line?: func [
	char 		[integer!]
	return: 	[logic!]
] [
	any [char = 13 char = 10]
]

find-comment-start: func [
	"Return position of comment start on current line, or zero, when there's no comment on this line"
	handle 		[integer!]
	position 	[integer!] "Position on line (comments starting after position are ignored)"
	return: 	[integer!]
	/local
		start 	[integer!]
] [
	start: get-line-start handle position
	position: search-in-target handle ";" start position
	either any [
		in-simple-string? handle position
		in-multiline-string? handle position
	] [0] [position]
]

find-string-start: func [
	handle 		[integer!]
	pos-from 	[integer!] 	"Should be 1 or latest cached position (no caching yet)"
	pos-to 		[integer!]	"Current position"
	return: 	[integer!]
	/local
		start 	[integer!]
		end 	[integer!]
		ret 	[integer!]
] [
	start: search-in-target handle "{" pos-from pos-to
	; there is no string
	if zero? start [return 0]
	; start is part of simple string ("...{...")
	if in-simple-string? handle start [return 0]
	; start is in comment (false positive)
	ret: find-comment-start handle start
	unless zero? ret [return 0]

	; NOTE: if we have a string like "...{...}...{..^/..}..."
	;	than the second { is not matched with BRACEMATCH
	;	this seems like a Scintilla limitation
	; 	not sure what's the nest way to solve this
	;	probably a custom BRACEMATCH function?	
	end: SendMessage handle SCI_BRACEMATCH start - 1 0
	; we are inside unfinished string
	if zero? end [return start]
	; finally, we are inside string!
	if pos-to <= end [return start]
	; this is some different string, find another one
	ret: find-string-start handle end + 1 pos-to
	unless zero? ret [return ret]
	; all strings matched we are after them
	0
]

find-brace-start: func [
	handle 		[integer!]
	position 	[integer!]
	return: 	[integer!]
	/local
		char 	[byte!]
] [
	; FIXME: does not support recursion
	until [
		; check for doc start
		if 0 = position [return 0]
		; check for brace start
		char: as byte! SendMessage handle SCI_GETCHARAT position - 1 0
		; go back one char
		position: position - 1
		; check if matched 
		any [
			#"[" = char
			#"(" = char
			#"{" = char
		]
	]
	; we went back, so increase again
	position + 1
]

bracket-opening?: func [
	handle 	[integer!]
	line 	[integer!]
	return: [logic!]
] [
	as logic! search-in-target handle "[" 
		1 + SendMessage handle SCI_POSITIONFROMLINE line - 1 0
		1 + SendMessage handle SCI_GETLINEENDPOSITION line - 1 0
]

bracket-closing?: func [
	handle 	[integer!]
	line 	[integer!]
	return: [logic!]
] [
	as logic! search-in-target handle "]"
		1 + SendMessage handle SCI_POSITIONFROMLINE line - 1 0
		1 + SendMessage handle SCI_GETLINEENDPOSITION line - 1 0
]

get-block-start: func [
	handle 		[integer!]
	position 	[integer!]
	return: 	[integer!]
] [
	search-in-target handle "[" position 1
]

get-block-end: func [
	handle 		[integer!]
	position 	[integer!]
	return: 	[integer!]
] [
	search-in-target handle "]" position SendMessage handle SCI_GETLENGTH 0 0
]

empty-line?: func [
	handle 	[integer!]
	line 	[integer!]
	return:	[logic!]
] [
	(SendMessage handle SCI_GETLINEINDENTPOSITION line - 1 0)
	=
	(SendMessage handle SCI_GETLINEENDPOSITION line - 1 0)
]

do-indentation: func [
	handle 					[integer!]
	line 					[integer!]
	/local
		indent-size 		[integer!]
		line-start 			[integer!]
		block-indent 		[integer!]
		temp				[integer!]
		line-length 		[integer!]
		line-text 			[c-string!]
		eol 				[integer!]
] [
	; some preparation
	indent-size: SendMessage handle SCI_GETINDENT 0 0
	line-start: 1 + SendMessage handle SCI_POSITIONFROMLINE line - 1 0

	; ; print-line ["^/do-indetation: size:" indent-size ", line-start: " line-start " for line " line]
	; ; print-line ["open? " bracket-opening? handle line - 1 ", close? " bracket-closing? handle line]

	eol: either zero? SendMessage handle SCI_GETEOLMODE 0 0 [2] [1] ; zero is CRLF, two bytes
	line-length: SendMessage handle SCI_LINELENGTH line - 2 0
	line-text: as c-string! allocate line-length
	SendMessage handle SCI_GETLINE line - 2 as integer! line-text
	line-length: line-length - eol

	; is there more than one line?
	if line > 1 [
		case [
			bracket-closing? handle line [
				; print-line ["case: bracket closing"]
				temp: 1 + SendMessage handle SCI_LINEFROMPOSITION get-block-start handle line-start - 1 0
				block-indent: SendMessage handle SCI_GETLINEINDENTATION temp - 1 0 
				SendMessage handle SCI_SETLINEINDENTATION line - 1 block-indent
				if empty-line? handle line - 1 [
					SendMessage handle SCI_SETLINEINDENTATION line - 2 block-indent
				]
			]

			; is there opening bracket on the end of previous line?
			brace-start? as integer! line-text/line-length [
				; print-line ["case: bracket opening"]
				temp: SendMessage handle SCI_GETLINEINDENTATION line - 2 0
				SendMessage handle SCI_SETLINEINDENTATION line - 1 indent-size + temp
			]

			true [
				temp: SendMessage handle SCI_GETLINEINDENTATION line - 2 0
				SendMessage handle SCI_SETLINEINDENTATION line - 1 temp
				SendMessage handle SCI_GOTOPOS SendMessage handle SCI_GETLINEINDENTPOSITION line - 1 0 0 
			]
		]
	]
	free as byte-ptr! line-text
	fix-cursor-position handle
]

fix-cursor-position: func [
	handle 			[integer!]
	/local
		position 	[integer!]
		line 		[integer!]
		indent 		[integer!]
] [
	position: 1 + SendMessage handle SCI_GETCURRENTPOS 0 0
	line: get-current-line handle
	indent: 1 + SendMessage handle SCI_GETLINEINDENTPOSITION line - 1 0
;	; print-line ["fix pos: pos: " position ", line: " line ", indent: " indent]

	if position < indent [
;		; print-line ["fixing position to: " indent - 1]
		SendMessage handle SCI_GOTOPOS indent - 1 0
	]
]

process-margin-click: func [
	notification 		[SCNotification!]
	/local
		mask 			[integer!]
		handle 			[integer!]
		line 			[integer!]
] [
	handle: as integer! notification/hwndFrom
	line: SendMessage handle SCI_LINEFROMPOSITION notification/position 0
	mask: SendMessage handle SCI_MARKERGET line 0

	; form for mask:  1 << margin
	if 1 = notification/margin [
		either zero? ((1 << 1) and mask) [
			SendMessage handle SCI_MARKERADD line 1
		] [
			SendMessage handle SCI_MARKERDELETE line 1
		]
	]
	if 2 = notification/margin [
		either zero? ((1 << 2) and mask) [
			SendMessage handle SCI_MARKERADD line 2
		] [
			SendMessage handle SCI_MARKERDELETE line 2
		]
	]
]

small-text-buffer: as c-string! allocate 80 		; for words, etc.
; TODO: free on exit

process-code: func [
	handle 			[integer!]
	nmhdr  			[SCNotification!]
	/local
		position 	[integer!]
		line 		[integer!]
		in-string? 	[logic!]
		red-text 	[red-string!]
		temp 		[integer!]
] [
	position: 1 + SendMessage handle SCI_GETCURRENTPOS 0 0
	switch nmhdr/code [
		; 2000					;-- Use it if really need, use the lexer inside SciLexer by default
		;SCN_STYLENEEDED [
		;	; print-line "SCN_STYLENEEDED"
		;	position: 1 + SendMessage handle SCI_GETENDSTYLED 0 0
		;	line: 1 + SendMessage handle SCI_LINEFROMPOSITION position - 1 0
		;	position: 1 + SendMessage handle SCI_POSITIONFROMLINE line - 1 0
		;	; FIXME: this passes text from start of current line
		;	;	but when we are inside multiline string, this is problematic
		;	;	we need to check if we are in string  and when yes,
		;	;	pass text from the string beginning
		;	stylize-range handle position nmhdr/position - position + 1
		;]

		; 2001
		SCN_CHARADDED [
			; print-line "SCN_CHARADDED"
			line: get-current-line handle
			if new-line? nmhdr/ch [
				do-indentation handle line
			]
			if nmhdr/ch = as-integer #"]" [
				temp: SendMessage handle SCI_GETLINEINDENTATION line - 1 0 
				SendMessage handle SCI_SETLINEINDENTATION line - 1 temp - 4
			]
			; TODO: IN-STRING should be matched just by lexer's state
			;in-string?: any [
			;	in-simple-string? handle position
			;	in-multiline-string? handle position
			;]
			;; - autocomplete
			;get-range handle
			;	1 + SendMessage handle SCI_WORDSTARTPOSITION position - 1 as integer! true
			;	position
			;	small-text-buffer
			;red-text: string/load small-text-buffer length? small-text-buffer UTF-8
			;unless in-string? [
			;	#call [editor/autocomplete-word handle red-text]
			;]

			; - calltip
			; TODO: use GET-WORD here
			;get-range handle
			;	1 + SendMessage handle SCI_WORDSTARTPOSITION position - 1 as integer! true
			;	1 + SendMessage handle SCI_WORDENDPOSITION position - 1 as integer! true
			;	small-text-buffer
			;red-text: string/load small-text-buffer length? small-text-buffer UTF-8
			;either whitespace? as byte! nmhdr/ch [
			;	; close calltip on whitespace 
			;	SendMessage handle SCI_CALLTIPCANCEL 0 0
			;] [
			;	unless in-string? [
			;		#call [editor/show-calltip handle position red-text]
			;	]
			;]
		]

		; 2002
		;SCN_SAVEPOINTREACHED [
			; print-line "SCN_SAVEPOINTREACHED"
			; if editor, change save state
		;	#call [editor/set-save-state handle true]
		;]

		; 2003
		;SCN_SAVEPOINTLEFT [
			; print-line "SCN_SAVEPOINTLEFT"
			; if editor, change save state
		;	#call [editor/set-save-state handle false]
		;]

		; 2007
		;SCN_UPDATEUI [
			; print-line "SCN_UPDATEUI"
			; remove indicators (found text) - removed only when nothing is selected
		;	if  (SendMessage handle SCI_GETSELECTIONSTART 0 0)
		;		>=
		;		(SendMessage handle SCI_GETSELECTIONEND 0 0) [
		;			SendMessage handle SCI_SETINDICATORCURRENT 1 0
		;			position: SendMessage handle SCI_GETLENGTH 0 0
		;			SendMessage handle SCI_INDICATORCLEARRANGE 1 position - 1
		;		] 

		;	highlight-braces handle position
			; - fix cursor position
			;fix-cursor-position handle
			
			; - update status line
			;#call [gui/update-status as integer! nmhdr/hwndFrom]
		;]

		; 2010
		;SCN_MARGINCLICK [
			; print-line ["SCN_MARGINCLICK: pos:" 1 + nmhdr/position ", margin:" 1 + nmhdr/margin]
		;	process-margin-click nmhdr
		;]

		default [0]
	]
]


customWndProc: func [
	hWnd		[handle!]
	msg			[integer!]
	wParam		[integer!]
	lParam		[integer!]
	/local
		nmhdr 	[SCNotification!]
][
	switch msg [
		;WM_COMMAND [
		;	if WIN32_LOWORD(wParam) = SCINTILLA_BASE_ID [
			;TODO: process commands 
			;	; print-line "++COMMAND--"
			;	; print-line ["cmd_wParam: " WIN32_LOWORD(wParam) " & " WIN32_HIWORD(wParam)]
			;	; print-line ["cmd_lParam: " WIN32_LOWORD(lParam) " & " WIN32_HIWORD(lParam)]	
		;	]
		;]
		WM_NOTIFY [
			nmhdr: as SCNotification! lParam
			if nmhdr/idFrom = SCINTILLA_BASE_ID [
				process-code as integer! nmhdr/hwndFrom nmhdr
			]
		]
		default [0]
	]
]

oldSciWndProc: gui/register-class [					;-- returns old events handler
	#u16 "Scintilla"								;-- widget original name
	#u16 "RedEditor"								;-- new internal name
	symbol/make "editor"							;-- Red-level style name
	0												;-- exStyle flags
	0												;-- style flags
	SCINTILLA_BASE_ID								;-- base ID for instances
	null											;-- style custom event handler
	:customWndProc									;-- parent custom event handler
]