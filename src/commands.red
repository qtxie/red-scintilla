Red [

]

commands: context [
	not-implemented: does [
	; FIXME: compiles but throws ***path none is not valid for unset type
		gui/status-face/text: "NOT IMPLEMENTED YET"
		show gui/status-face
	; /FIXME
		debug-print "NOT IMPLEMENTED YET"
		"not IMPLEMENTED"
	]
	new-project:			does [
		gui/status-face/text: "new project: NOT IMPLEMENTED YET"
		show gui/status-face
		debug-print "new project in menu"
		""
	]
	open-project: 			:not-implemented
	save-project: 			:not-implemented
	save-as-project: 		:not-implemented
	exit-ide:				does [quit]
	
	clear-file: does [
		files/clear-doc files/active-doc
	]

	new-file: func [
		/with
			name	[file! string!]
		/local
			handle	[integer!]
	][
		unless with [name: "Untitled"]
		handle: gui/add-tab "Untitled"
		files/add handle name
	]

	open-file: has [
		filename
		doc
		selected
	][
		if all [
			filename: request-file/title "Open file"
			exists? filename
		] [
			;either doc: files/find-doc 'filename filename [
				; doc is already open, just switch tab
			;	files/select-doc doc/handle
			;	gui/select-tab index? find gui/list-project/data files/get-doc-name doc/handle
			;] [
				; doc is not yet opened, open it
				;either empty? editor/get-document files/active-doc [clear-file] [new-file]
				new-file/with filename
				files/open-doc filename
				gui/update-doc-info filename
				;files/save-doc-set
				;gui/select-tab length? gui/list-project/data
			;]
		]
	]

	save-file: has [script handle filename] [
		;check-header
		handle: files/active-doc
		filename: select files/docs handle
		if string? filename [filename: request-filename]
		if filename [
			change find/tail files/docs handle filename
			script: editor/get-document handle
			write filename script
			scintilla/set-save-point handle
			gui/update-doc-info filename
		]
	]

	check-overwrite: has [overwrite?] [
		overwrite?: true
		if exists? current-doc/filename [
			overwrite?: gui/show-overwrite current-doc/filename
		]
		overwrite?
	]
	save-as-file: has [script filename] [
		all [
			request-filename
			check-overwrite
			save-file
		]
	]
	request-filename: has [filename] [
		if filename: request-file/title/save "Save file" [
			; TODO: recognize if it's Red(/System) script
			unless find [%.red %.reds] suffix? filename [
				append filename %.red
			]
		]
		filename
	]
	close-file:	has [
		names
		handle
		selected
	] [
		; TODO: check if file is saved
		either 1 = length? gui/tab-editors/data [
			; only one doc, do not remove, but clean it
			clear-file

		] [
			handle: files/active-doc
			selected: gui/tab-editors/selected
			remove/part at files/docs selected * 2 - 1 2
			remove at gui/tab-editors/pane selected
			remove at gui/tab-editors/data selected
			selected: max 1 selected - 1
			handle: pick files/docs selected * 2 - 1
			files/active-doc: handle
			files/selected: selected
		]
		; update UI
		gui/select-tab selected
	]
	revert-file:			:not-implemented
	get-open-files: has [] [
		print mold files/docs
	]
	
	undo: 					does [
		undo?: scintilla/can-undo? files/active-doc
		if undo? [scintilla/undo files/active-doc]
	]
	redo: 					does [
		redo?: scintilla/can-redo? files/active-doc
		if redo? [scintilla/redo files/active-doc]
	]
	copy: 				does [scintilla/copy files/active-doc]
	cut:				does [scintilla/cut files/active-doc]
	paste: 				does [scintilla/paste files/active-doc]
	indent-line:			has [line indentation] [
		line: 					scintilla/get-current-line files/active-doc
		indentation: 			scintilla/get-line-indentation files/active-doc line
		scintilla/set-line-indentation files/active-doc line indentation + scintilla/get-indent files/active-doc
	]
	unindent-line:			has [line indentation] [
		line: 					scintilla/get-current-line files/active-doc
		indentation: 			scintilla/get-line-indentation files/active-doc line
		scintilla/set-line-indentation files/active-doc line indentation - scintilla/get-indent files/active-doc
	]
	swap-line-up:			has [handle position line line-length line-text line-start prev-line-start] [
		handle: 				files/active-doc
		position: 				scintilla/get-current-position handle
		line: 					scintilla/get-current-line handle
		line-length: 			scintilla/line-length handle line
		line-text: 				scintilla/get-line handle line
		line-start: 			scintilla/position-from-line handle line
		prev-line-start: 		scintilla/position-from-line handle line - 1
		scintilla/delete-range handle line-start line-length
		scintilla/insert-text handle prev-line-start line-text
		scintilla/go-to-position handle prev-line-start
	]
	swap-line-down:			has [handle position line line-length line-text line-start next-line-start] [
		handle: 				files/active-doc
		position: 				scintilla/get-current-position handle
		line: 					scintilla/get-current-line handle
		line-length: 			scintilla/line-length handle line
		line-text: 				scintilla/get-line handle line
		line-start: 			scintilla/position-from-line handle line
		scintilla/delete-range handle line-start line-length
		next-line-start: 		scintilla/position-from-line handle line + 1
		scintilla/insert-text handle next-line-start line-text
		scintilla/go-to-position handle next-line-start
	]	
	toggle-comment: 		has [
		handle 			[integer!]
		line 			[integer!]
		end-line 		[integer!]
		commented? 		[logic!]
		c-line 			[integer!]
		position 		[integer!]
		comment-func 	[function!]
	] [
		; mark undo action start
		scintilla/end-undo-action handle
		scintilla/begin-undo-action handle
		; 1. single line: find if first char is #";", if yes uncomment, otherwise comment
		; 2. multiple lines: find if all lines have comment in ENABLE-COMMENT format, if yes, uncomment, if no comment 
		
		handle: 	files/active-doc
		line: 		scintilla/line-from-position handle scintilla/get-selection-start handle
		end-line: 	scintilla/line-from-position handle scintilla/get-selection-end handle

		either line = end-line [
			; one line
			either 59 = scintilla/first-char-on-line handle line [	; match #";"
				editor/uncomment-line handle line
			] [
				editor/comment-line handle line
			] 
		] [
			; multiple-lines
			commented?: true
			c-line: line
			until [
				commented?: all [
					position: scintilla/position-from-line handle c-line
					equal? 59 scintilla/get-char-at handle position 	; #";"
					equal? 32 scintilla/get-char-at handle position + 1 ; #" "
				]
				unless commented? [break]
				c-line: c-line + 1
				c-line > end-line 
			]
			; FIXME: This code doesn’t work currently, it’s not possible to pass function from object yet
			; comment-func: either commented? [:editor/uncomment-line] [:editor/comment-line]
			; until [
			; 	comment-func handle line
			; 	line: line + 1
			; 	line > end-line
			; ]
			until [
				either commented? [
					editor/uncomment-line handle line
				] [
					editor/comment-line handle line
				]
				line: line + 1
				line > end-line
			]
		]
		; mark undo action end
		scintilla/end-undo-action handle
		scintilla/begin-undo-action handle
	]

	toggle-block-comment: has [handle start end] [
		handle: files/active-doc
		start: scintilla/get-selection-start handle
		end: scintilla/get-selection-end handle
		scintilla/insert-text handle start "comment [^/"
		scintilla/insert-text handle end + 10 "^/]^/" ; FIXME: what if CRLF is used?
	]

	edit-preferences: does [
		temp-options: make editor-options []
		gui/init-prefs temp-options
	]

; ====================

	next-tab: has [handle selected] [
		selected: gui/tab-editors/selected + 1
		if selected > length? gui/tab-editors/pane [selected: 1]
		gui/tab-editors/selected: selected
		handle: gui/tab-editors/pane/:selected/pane/1/state/1
		current-doc: files/docs/:handle
		;show gui/tab-editors
	]

	prev-tab: has [handle selected] [
		selected: gui/tab-editors/selected - 1
		if zero? selected [selected: length? gui/tab-editors/pane]
		gui/tab-editors/selected: selected
		handle: gui/tab-editors/pane/:selected/pane/1/state/1
		current-doc: files/docs/:handle		
		;show gui/tab-editors
	]

; ====================

	open-search-pane: does [
		gui/replace-pane/visible?: no
		gui/search-pane/visible?: yes ;not search-pane/visible?
		show [gui/search-pane gui/replace-pane]
	]

	set-search-text: has [sel search-object] [
		sel: scintilla/get-selection files/active-doc
		gui/field-search/text: sel
		show gui/field-search
		search-object: make search-object-proto []
		search-object/phrase: sel 
		search-object/all?: true
;		actions/find-phrase
	]

	search-text: does [
		; NOTE: Search/replace pane is currently always visible (needs resizing fixes)
	;	open-search-pane
		set-search-text
		find-phrase
	]

; =========================

	find-phrase: 			does [
		print "find-phrase called"
		search-object/replace?: no
		editor/find-text/only files/active-doc search-object
	]
	find-next:				:not-implemented
	find-prev: 				:not-implemented
	replace: 				does [
		search-object/replace?: yes
		editor/find-text/only files/active-doc search-object
	]
	replace-next:			:not-implemented
	quick-find:				:not-implemented						 ; select current word and find
	find-in-project:		:not-implemented
	
	toggle-bookmark:		func [
		"Set/unset bookmark for current line"
	] [
		scintilla/toggle-marker
			files/active-doc
			scintilla/line-from-position files/active-doc scintilla/get-current-position files/active-doc ; TODO: scintilla/scintilla/get-current-line
			1
	]
	next-bookmark:			:not-implemented
	prev-bookmark:			:not-implemented
	clear-bookmarks:		:not-implemented

	run: has [script result handle console cmd][
		console: join ide/home-dir %bin/console.exe
		handle: files/active-doc
		script: select files/docs handle
		cmd: append form to-local-file console reduce [" " form to-local-file script]
		call/console cmd
	]

	run-selection: has [script result] [
		script: 				scintilla/get-selection files/active-doc
		result: 				editor/do-script script
	]

	close-annotation: does [
		scintilla/annotation-clear-all files/active-doc
	]

	compile: has [ret temp-path] [
		; either current-doc/filename [save-as-file] [save-file]
		; prin "Compile..."
		; view/no-wait/flags [
		; 	text bold "Please wait, compiling..."
		; 	button "OK" [unview]
		; ] [modal popup]
		; ret: call/wait/console {rebol --cgi --secure none red.r "delme.red" > temp.txt} 
		; do-events
		; print "Done"
		; print ret
		; print read %temp.txt
	
		; doc has file 
		if any [
			not current-doc/filename
			not current-doc/saved?
		] [
			either gui/show-not-saved/custom current-doc/filename "File must be saved before compiling." [
				save-file
			] [
				exit
			]
		]
		print ["compiler can run for" current-doc/filename]
		print ["sp: " system/options/path]
		; temp-path: system/options/path
		; print [options/red-path type? options/red-path]
		; system/options/path: options/red-path
		; call/wait probe rejoin [{rebol --cgi --secure none red.r } current-doc/filename { > temp.txt}]
		; system/options/path: temp-path

		gui/view-compiler

	]

	check-header: has [s script] [
		script: editor/get-document files/active-doc
		script: attempt [load script]
		either all [
			s: find script 'Red
			block? s/2
		] [
			; OK
		] [
			view/flags [
				title "Missing header"
				text 250x50 bold "Script is missing header.^/Do you want to include one?"
				return
				button "Yes" [
					; insert header
					scintilla/insert-text files/active-doc 1 "Red []^/"
					unview
				]
				button "No" [
					; continue
					unview
				]
			] [modal popup]
		]
	]

	find-header: func [
		"Locate header in script"
		/local
			script
			header-rule
			start
	] [
		script: editor/get-document files/active-doc
		header-rule: [
			; FIXME: I can’t put start: at #"R", so it’s after and must be compensated with BACK later
			[#"R" | #"r"] start: [#"E" | #"e"] [#"D" | #"d"]
			any space ; TODO: tab, newline, ...
			#"["
		]
		parse script [
			some [
				[header-rule to end]
			| 	skip
			]
		]
		back start
	]

	edit-header: has [handle script header-rule mark start end] [
		gui/show-header-editor
		start: find-header
		mark: index? find start #"["
		handle: files/active-doc
		; TODO: move scintilla specific code to EDITOR ctx
		; replace header in doc
		if all [start mark current-doc/header] [
			end: scintilla/brace-match handle mark
			scintilla/set-selection-start handle index? start
			scintilla/set-selection-end handle end + 1
			scintilla/replace-selection handle head append "Red " gui/get-header
		]
		; TODO: change tab name, when required

	]

	parse-header: func [
		"Parse script for header and return as object!"
		/local
			start 
			mark 
			end
			text
	] [
		start: find-header
		mark: index? find start #"["
		if mark [
			end: scintilla/brace-match files/active-doc mark
			scintilla/set-target-range files/active-doc mark end + 1 
			text: scintilla/get-target-text files/active-doc
		]
		load text
	]

	; ---

	show-min-ui: does [
		print "Enable Min UI"
		gui/tab-editors/offset: 2x2
		gui/tab-editors/size: 1200x700 - 4x4
		show gui/main
		foreach face gui/tab-editors/pane [
			face/size: 1200x700 - 6x6
			show face
			face/pane/1/size: 1194x674
			face/pane/1/offset: 2x2
			show face/pane/1
		]
		show gui/tab-editors
	]
	
	show-toolbar:			has [] [
		debug-print "show toolbar"
		gui/toolbar-pane/visible?: not gui/toolbar-pane/visible?
		print [index? find gui/toolbar-pane/parent/pane gui/toolbar-pane ]
		foreach face gui/toolbar-pane/parent/pane [
			face/offset/y: either gui/toolbar-pane/visible? [
				face/offset/y + gui/toolbar-pane/size/y
			] [
				face/offset/y - gui/toolbar-pane/size/y
			]
		]
		show gui/main
	]

	show-project-browser: 	has [] [
		gui/list-project/visible?: not gui/list-project/visible?
		either gui/list-project/visible? [
			gui/tab-editors/offset/x: gui/tab-editors/offset/x + gui/list-project/size/x
			gui/tab-editors/size/x: gui/tab-editors/size/x - gui/list-project/size/x
		] [
			gui/tab-editors/offset/x: gui/tab-editors/offset/x - gui/list-project/size/x
			gui/tab-editors/size/x: gui/tab-editors/size/x + gui/list-project/size/x
		]
	;	gui/tab-editors/pane/1/offset: gui/tab-editors/pane/1/offset
	;	show gui/tab-editors/pane/1
		show gui/main
	]

	documentation:			:not-implemented
	changelog:				:not-implemented
	about: 					:not-implemented

	mark-word: has [handle ret search] [
		handle: files/active-doc
		send-message handle 2308 0 0 ; SCI_WORDLEFT
		send-message handle 2311 0 0 ; SCI_WORDRIGHTEXTEND
		highlight-all-matches
	]

	; "If there's some selection, all matches are highlighted"
	highlight-all-matches: has [
		handle
	] [
		handle: files/active-doc
		scintilla/target-from-selection handle
		editor/find-text/only handle make search-object-proto [
			phrase: scintilla/get-target-text handle 
			all?: true
			replace?: no
		]
	]

	;install-themes: does [
	;	; we expect that UNZIP is already downloaded and download only themes
	;	write/binary in-home %themes-pack.zip read/binary http://www.qyz.cz/red/themes-pack.zip
	;	call {unzip themes-pack.zip}
	;	; Because there's no CALL/WAIT yet, we need to delay execution with a window
	;	view/flags center-face layout [text "Themes downloaded." button "Ok" [unview]] [modal popup]
	;]

	;check-install: does [
	;	unless exists? join ide/home-dir %red-master/ [
	;		print "install required"
	;		gui/show-intro
	;	]
	;	view-main
	;]
]