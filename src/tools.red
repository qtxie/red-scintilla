Red []


string-size?: routine [
	"Return string size in bytes"
	string 			[string!]
	return: 		[integer!]
	/local
		length 		[integer!]
] [
	length: -1
	length? unicode/to-utf8 string :length
]

join: func [
	value 			[series!]
	rest 			[any-type!]
] [
	head append copy value rest
]

rejoin: func [
	"Crude REJOIN version, because I can't live without it"
	block 			[block!]
	/local
		string 		[string!]
] [
	string: make string! 300 ; good enough for my requirements
	block: reduce block
	foreach value block [append string form value]
	string
]

form-error: func [
	error 			[error!]
	/local
		cat 		[object!]
		msg 		[block!]
] [
	cat: system/catalog/errors
	msg: select select cat error/type error/id
	rejoin ["** " error/type " error: " form reduce bind msg error] ; FIXME: uppercase throws an error here
]

true?: func [
	value 			[any-type!]
	return: 		[logic!]
] [
	not not value
]
