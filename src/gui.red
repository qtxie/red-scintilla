Red [
	Title:			"Red IDE GUI"
	Author: 		"Boleslav Březovský, Qingtian Xie"
	File:			%gui.red
	Needs: 			'View
]

; === main context  ==========================================================

gui: context [
	editor-size: none

	get-tab-handle: func [
		face 		[object!]
		index 		[integer!]
	] [
		if zero? index [index: 1]
		first face/pane/:index/pane/1/state
	]

	make-editor: does [
		make face! [
			type: 'editor offset: 0x0 size: 10x10
			actors: object [
				on-change: func [face [object!] event [event!]]['done]
			]
		]
	]

	add-tab: func [
		"Add new tab to tab-panel and list and return tab’s face."
		name	[file! string!]
		return: [integer!]
		/local
			panel  [object!]
			handle [integer!]
	][
		append tab-editors/data name
		append tab-editors/pane make face! [
			type: 'panel
			pane: reduce [make-editor]
		]
		show tab-editors
		tab-editors/selected: length? tab-editors/data
		panel: last tab-editors/pane
		panel/pane/1/size: panel/size				;-- resize editor
		handle: first panel/pane/1/state			;-- return editor handle
		editor/init handle
		show tab-editors
		handle
	]

	update-status: func [				
	; Update status is to be called from Red/System level, so we pass just minimal info 
	; and the function must get everything else from global context (or module later)
		handle 				[integer!]
		/local
			position 		[integer!]
			line 			[integer!]
			total-lines 	[integer!]
			lines-on-screen [integer!]
			column 			[integer!]
			info-text 		[integer!]
	] [
		position: scintilla/get-current-position handle
		line: scintilla/line-from-position handle position
		total-lines: scintilla/get-line-count handle
		lines-on-screen: scintilla/get-lines-on-screen handle
		column: scintilla/get-column handle position
		info-text: rejoin ["Line " line "/" total-lines ", Column " column ", Position " position]
		status-face/text: info-text
		show status-face
		scintilla/grab-focus handle
		; update doc status
		doc-status/text: either zero? scintilla/get-modify handle ["SAVED"] ["CHANGED"]
		show doc-status
	]

	update-doc-info: func [
		"Set full path to windows title and just filename to tab and list"
		; TODO: add settings where should be short/long version used
		filename
		/local
			selected 			[integer!]
			short-filename 		[string!]
			long-filename 		[string!]
			save-mark 			[string!]
	] [
		;current-doc/filename: filename
		long-filename: either filename [form filename] ["Untitled"]
		;current-doc/name: long-filename
		;save-mark: either current-doc/saved? [""] ["* "]
		save-mark: ""
		main/text: rejoin ["Red IDE : " save-mark long-filename]
	;	file-info-text/text: long-filename

		short-filename: copy either filename [form next find/last filename #"/"] ["Untitled"]
		insert short-filename save-mark
		selected: tab-editors/selected 
		tab-editors/data/:selected: short-filename ;current-doc/name
		show main
		;show [list-project tab-editors]
	]

	; === keymap ================================================================= 

	; NOTE: Scintilla uses some predefined keys. See %scintilla/src/KeyMap.cxx for full list
	; 		here's an excerpt of some used keys:
	; {'X', 			SCI_CTRL,	SCI_CUT},
	; {'C', 			SCI_CTRL,	SCI_COPY},
	; {'V', 			SCI_CTRL,	SCI_PASTE},
	; {'A', 			SCI_CTRL,	SCI_SELECTALL},
	; {'L', 			SCI_CTRL,	SCI_LINECUT},
	; {'L', 			SCI_CSHIFT,	SCI_LINEDELETE},
	; {'T', 			SCI_CSHIFT,	SCI_LINECOPY},
	; {'T', 			SCI_CTRL,	SCI_LINETRANSPOSE},
	; {'D', 			SCI_CTRL,	SCI_SELECTIONDUPLICATE},
	; {'U', 			SCI_CTRL,	SCI_LOWERCASE},
	; {'U', 			SCI_CSHIFT,	SCI_UPPERCASE},

	; TODO: keymap should be external for easy editing

	; keymap dialect:
	; #"char"  - directly match char
	; "modifier+char" - match char with modifier

	get-keymap-key: func [
		"Make key in keymap dialect"
		key 		[char! word! none!]
		flags 		[block!]
		/logic
			keymap-key 	[char! string!]
	] [
		keymap-key: clear ""
		unless key [return key]
		if find flags 'control [append keymap-key "Ctrl+"]
		if find flags 'shift   [append keymap-key "Shift+"]
		append keymap-key form key
		select keymap keymap-key
	]

	keymap: make map! [
		"Ctrl+Q" 			exit-ide

		"Ctrl+N" 			new-file
		"Ctrl+O"			open-file
		"Ctrl+S" 			save-file
		"Ctrl+Shift+S" 		save-as-file
		"Ctrl+W" 			close-file

		"Ctrl+F"			search-text
		"F3"				find-next
		"Shift+F3"			find-prev
		"Ctrl+H"			replace
		"Ctrl+Shift+H"		replace-next
		"Ctrl+F3"			quick-find ; select current word and find
	;	"Find In Project"					find-in-project	

		"Ctrl+F2"			toggle-bookmark
		"F2"				next-bookmark
		"Shift+F2"			prev-bookmark
		"Ctrl+Shift+F2" 	clear-bookmarks

		"Ctrl+M" 			mark-word

		"^["				close-annotation
		"F5"				run
		"Shift+F5"			run-selection
		"F8"				compile

		"Ctrl+Shift+Up" 	swap-line-up
		"Ctrl+Shift+Down" 	swap-line-down

		"Ctrl+^-" 			next-tab
		"Ctrl+Shift+^-" 	prev-tab

	]

	; === menu ===================================================================

	menu-proto: [
		; "&Project" [
		; 	"&New Project"						new-project
		; 	"&Open Project" 					open-project
		; 	"Save..."							save-project
		; 	"Save as..."						save-as-project
		; 	---
		; 	"Exit"								exit-ide
		; ]
		"&File" [
			"&New File^-Ctrl+N"					new-file
			"&Open File^-Ctrl+O" 				open-file
			"Save^-Ctrl+S"						save-file
			"Save as...^-Ctrl+Shift+S"			save-as-file
			---
			"&Close File^-Ctrl+W"				close-file 
			"&Revert File"						revert-file 
			---
			"Exit" 								exit-ide	
		]
		"Edit" [
			"Undo^-Ctrl+Z"						undo
			"Redo^-Ctrl+Y"						redo
			---
			"Copy^-Ctrl+C"						copy
			"Cut^-Ctrl+X"						cut
			"Paste^-Ctrl+V"						paste
			---
			"Line" [
				"Indent^-Ctrl+["					indent-line
				"Unindent^-Ctrl+]"					unindent-line
				"Swap line up^-Ctrl+Shift+Up"		swap-line-up
				"Swap line down^-Ctrl+Shift+Down"	swap-line-down				
			]
			"Comment" [
				"Toggle Comment^-Ctrl+;" 				toggle-comment
				"Toggle Block Comment^-Ctrl+Shift+;"	toggle-block-comment
			]
			---
			"Preferences" 						edit-preferences
		]
		"Find" [
			"Find...^-Ctrl+F"					find-phrase
			"Find Next^-F3"						find-next
			"Find Previous^-Shift+F3"			find-prev
			---
			"Replace...^-Ctrl+H"				replace
			"Replace Next^-Ctrl+Shift+H"		replace-next
			---
			"Quick Find^-Ctrl+F3"				quick-find ; select current word and find
			"Find In Project"					find-in-project
		]
		"Goto" [
			"Toggle Bookmark^-Ctrl+F2"			toggle-bookmark
			"Next Bookmark^-F2"					next-bookmark
			"Prev Bookmark^-Shift+F2"			prev-bookmark
			"Clear Bookmarks^-Ctrl+Shift+F2" 	clear-bookmarks
		]
		"Tools" [
			"Run^-F5"							run
			"Run selection^-Shift+F5"			run-selection
			"Compile^-F8"						compile
			---
			"Header editor" 					edit-header
		]
		"View" [
			"Min UI" 							show-min-ui
			"Show toolbar" 						show-toolbar
			"Show project browser" 				show-project-browser
		]
		"Help" [
			"Documentation"						documentation
			"Changelog" 						changelog
			"About Red Ide"						about
		]
	]

	gui-console-ctx: context [
		copy-text:   routine [face [object!]][terminal/copy-text   face]
		paste-text:  routine [face [object!]][terminal/paste-text  face]
		select-text: routine [face [object!]][terminal/select-text face]

		font-name: pick ["Fixedsys" "Consolas"] make logic! find [5.1.0 5.0.0] system/view/platform/version

		console: make face! [
			type: 'console offset: 0x0 size: 600x200
			font: make font! [name: font-name size: 11]
			menu: [
				"Copy^-Ctrl+C"		 copy
				"Paste^-Ctrl+V"		 paste
				"Select All^-Ctrl+A" select-all
			]
			actors: object [
				on-menu: func [face [object!] event [event!]][
					switch event/picked [
						copy		[copy-text   face]
						paste		[paste-text  face]
						select-all	[select-text face]
					]
				]
			]
		]
	]

	set-output-face: func [handle][
		scintilla/set-read-only handle true
		scintilla/set-scroll-width handle 10
		scintilla/set-scroll-width-tracking handle true
		scintilla/set-code-page handle 65001				;-- UTF8 codepage	
	]

; ============================================================================
; ===   layouts                                                            ===
; ============================================================================

; --- main UI ----------------------------------------------------------------

	toolbar:		none
	tab-editors:	none
	notebook:		none
	output:			none
	console:		none
	cu-check:      none
	check-case:    none
	check-word:    none
	check-sel:     none
	field-search:  none
	field-replace: none
	info-face:     none
	status-face:   none
	doc-status:    none
	spliter:     none
	; currently unused
	search-pane:   none
	replace-pane:  none

	main: [
		title "Red IDE"
		origin 1x1 space 0x0 below

		toolbar: panel [
			origin 0x0 space 0x0
			button %res/new.png		[commands/new-file]
			button %res/run.png		[commands/run]
			button %res/compile.png	[commands/compile]
			button %res/console.png	[commands/get-open-files]
		]
		tab-editors: tab-panel 600x400 []				;-- add tabs in runtime
		spliter: base 240.240.240 600x3 loose
		notebook: tab-panel 600x200 []					;-- add widgets in runtime
	]

	init: does [
		main: layout gui/main
		main/visible?: no
		main/extra: main/size							;-- save old size
		spliter/extra: spliter/offset					;-- save old offset

		; patch MAIN with menu and keyboard handler (not part of VID yet)
		main/menu: menu-proto
		main/actors: object [
			on-menu: func [
				face [object!] 
				event [event!]
				/local
					picked
			] [
				picked: event/picked
				; force execution, doesn'ŧ work by itself
				do [commands/:picked]
			]
			on-key-down: func [
				face [object!] 
				event [event!]
				/local
					act
			] [
				act: gui/get-keymap-key event/key event/flags
				unless act [return event]
				do commands/:act
				'done
			]
			on-resizing: func [face [object!] event [event!] /local delta][
				delta: face/size - face/extra
				tab-editors/size: tab-editors/size + delta
				spliter/size/x: tab-editors/size/x
				spliter/offset/y: spliter/offset/y + delta/y
				notebook/size/x: tab-editors/size/x
				notebook/offset/y: notebook/offset/y + delta/y
				face/extra: face/size
				spliter/extra: spliter/offset
				show [tab-editors spliter notebook]
				resize-tabs tab-editors
				resize-tabs notebook
				show [tab-editors notebook]
			]
			on-close: func [
				face [object!] 
				event [event!]
			][
				prefs/save-options
				files/save-docs-set
				;unview/all
				;foreach handle words-of files/docs [
				;	; doc not saved
				;	all [
				;		not zero? scintilla/get-modify handle
				;		show-not-saved files/docs/:handle/filename
				;		current-doc: files/docs/:handle
				;		commands/save-file
				;	]
				;]
				;files/save-doc-set
			]
		]

		tab-editors/actors: object [
			on-change: func [face [object!] event [event!] /local n ed][
				n: event/picked
				ed: face/pane/:n/pane/1
				system/view/platform/set-focus ed
			]
			on-dbl-click: func [face [object!] event [event!]][
				commands/close-file
			]
		]

		spliter/actors: object [
			on-drag: func [face [object!] event [event!]][
				face/offset/x: 0
			]
			on-drop: func [face [object!] event [event!] /local y][
				y: face/offset/y - face/extra/y
				tab-editors/size/y: tab-editors/size/y + y
				notebook/size/y: notebook/size/y - y
				notebook/offset/y: notebook/offset/y + y
				face/extra: face/offset
				show notebook
				show [tab-editors notebook]
				resize-tabs notebook
				resize-tabs tab-editors
				show [tab-editors notebook]
			]
		]

		resize-tabs: func [tabs [object!]][
			foreach panel tabs/pane [
				panel/pane/1/size: panel/size
			]
		]

		output: make-editor
		append notebook/data "output"
		append notebook/pane make face! [
			type: 'panel
			pane: reduce [output]
		]
		append notebook/data "console"
		append notebook/pane make face! [
			type: 'panel
			pane: reduce [gui-console-ctx/console]
		]

		move find/same main/pane spliter tail main/pane

		view/no-wait/flags main [resize]

		set-output-face first output/state

		;-- resize the 'area according to panel's size
		;-- it's a bit ugly, tab-panel is not very convenient.
		resize-tabs notebook
		show notebook
	]

	select-tab: func [
		index
	] [
		tab-editors/selected: index
		show tab-editors
	]

; --- prefs UI ---------------------------------------------------------------

	list-themes: 	none	; face ref.
	slider-zoom: 	none
	text-zoom: 		none
	check-long-lines: none
	list-long-lines: none
	drop-long-lines: none
	drop-tab-width: none
	check-use-tabs: none
	check-tab-indents: none
	check-backspace-unindents: none
	list-indentation-guides: none
	check-view-eol: none
	list-eol-mode: none
	check-convert-eol-on-load?: none
	list-ws-mode: none
	text-font-size: none

	font-list: ["Consolas" "Courier" "Courier New" "Fixedsys" "Lucida Console" "System" "Terminal"]

	apply-option: func [
		"Support function to simplify code for option settings"
		options 		[object!]
		name			[word!]
		value
	] [
		options/:name: value
		prefs/set-options current-doc/handle options
	]

	prefs-win: layout [
		title "Red IDE preferences"
		panel [
			below
			group-box 150x150 "Theme" [
				list-themes: text-list 135x130 data [] [
					print "======================================================"
					print [mold face/data face/selected]
					print mold prefs/themes
					print mold pick face/data face/selected
					print mold select prefs/themes pick face/data face/selected
					temp-options/theme: select prefs/themes pick face/data face/selected
					;prefs/set-theme current-doc/handle temp-options/theme
				]
			]
			button "Install more themes" [
				; FIXME: uses global TEMP to overcome TEXT-LIST problems
				commands/install-themes
			;	append clear list-themes/data prefs/get-themes
				list-themes/data: probe prefs/get-themes
				show list-themes
			]
			group-box 150x180 "Font" [
				list-font: text-list 135x130 data font-list [
					apply-option temp-options 'font-name pick face/data face/selected
				]
				return
				slider-font-size: slider 100 [
					apply-option temp-options 'font-size 6 + to integer! 20 * face/data
					text-font-size/text: form prefs/font-proto/size
					show text-font-size
				]
				text-font-size: text left "12"
			]
		]
		panel [
			text 100 bold "Zoom level"
			slider-zoom: slider [
				apply-option temp-options 'zoom-level -5 + to integer! 20 * face/data
				text-zoom/text: form temp-options/zoom-level
				show text-zoom
			]
			text-zoom: text left "0"
			return
			check-long-lines: check "Long lines" [
				apply-option temp-options 'long-lines? face/data
			]
			list-long-lines: drop-list data ["None" "Line" "Background"] [
				apply-option temp-options 'long-lines-mode pick [
					none line background
				] face/selected
			]
			text 50 "Column:"
			drop-long-lines: drop-down data ["78" "80" "100" "120" "132"] [
				unless error? try [to integer! face/text] [
					apply-option temp-options 'long-lines-column to integer! face/text
				]
			]
			return
			group-box "Tabs and indentation" [
				text "Tab width"
				drop-tab-width: drop-down data ["1" "2" "3" "4" "6" "8"] [
					unless error? try [to integer! face/text] [
						apply-option temp-options 'tab-width to integer! face/text
					]
				]
				check-use-tabs: check "Use tabs" [
					apply-option temp-options 'use-tabs? face/data
				]
				return
				check-tab-indents: check "Tab indents" [
					apply-option temp-options 'tab-indents? face/data
				] 
				check-backspace-unindents: check 150 "Backspace unindents" [
					apply-option temp-options 'backspace-unindents? face/data
				] 
				return
				text "Indentation guides"
				list-indentation-guides: drop-list data [
					"None" "Real" "Look forward" "Look both"	
				] [
					apply-option temp-options 'indentation-guides pick [
						none real look-forward look-both
					] face/selected
				]
			]
			return
			group-box "Whitespaces" [
				text "EOL mode"
				list-eol-mode: drop-list data [
					"CR+LF (Windows)" "CR (ZX Spectrum)" "LF (Unix)"
				] [
					apply-option temp-options 'eol-mode pick [crlf cr lf] face/selected
				]
				check-view-eol: check "View EOL" [
					apply-option temp-options 'view-eol? face/data
				]
				return
				text "Whitespace mode"
				list-ws-mode: drop-list data [
					"Invisible" "Visible always" "Visible after indent"
				] [
					apply-option temp-options 'ws-mode pick [
						invisible visible-always visible-after-indent
					] face/selected
				]
				check-convert-eol-on-load?: check 150 "Convert EOL on load" [
					apply-option temp-options 'convert-eol-on-load? face/data
				]
			]
			return
			group-box "Running scripts" [
				text "Show output in:"
				list-run-output: drop-list data ["editor" "console"] [
					apply-option temp-options 'run-output select face/data face/selected
				]
			]
		]
		return
		panel [
			across
			button "Save" [
				editor-options: temp-options
				prefs/save-options editor-options
				unview
			]
			button "Cancel" [
				prefs/set-options current-doc/handle editor-options
				unview
			]
		]
	]

	init-prefs: func [
		"Show and init prefs"
		options [object!]
	] [
		;list-themes/data: prefs/get-themes
		slider-zoom/data: to percent! options/zoom-level + 5.0 / 20.0
		text-zoom/text: form options/zoom-level

		check-long-lines/data: options/long-lines?
		list-long-lines/selected: index? find [none line background] options/long-lines-mode
		drop-long-lines/text: form options/long-lines-column

		drop-tab-width/text: form options/tab-width
		check-tab-indents/data: options/tab-indents?
		check-backspace-unindents/data: options/backspace-unindents?
		check-use-tabs/data: options/use-tabs?
		list-indentation-guides/selected: index? find [
			none real look-forward look-both
		] options/indentation-guides

		check-view-eol/data: options/view-eol?
		list-eol-mode/selected: index? find [crlf cr lf] options/eol-mode 
		check-convert-eol-on-load?/data: options/convert-eol-on-load?
		list-ws-mode/selected: index? find [
			invisible visible-always visible-after-indent
		] options/ws-mode

		unless empty? list-themes/data [
			; FIXME: does not check if theme exists/is valid...
			list-themes/selected: index? find list-themes/data select load join %themes/ options/theme 'name
		]

		list-font/selected: index? find font-list options/font-name
		slider-font-size/data: options/font-size - 6 / 20.0

		list-run-output/selected: index? find list-run-output/data form options/run-output

		show center-face prefs-win
	]

; --- intro UI ---------------------------------------------------------------

	intro-win: [
		title "Red IDE installation"
		text 360x80 {It seems that you don’t have Red && Rebol installed
in Red IDE path. Red IDE needs those files for compilation. It can download
it for you (recommended), or you may chose the folder where the required
files are located manually.}
		return
		button "Install" [commands/install unview]
		button "Locate" [editor-options/red-path: request-dir unview]
	]

	show-intro: does [
		intro-win: layout intro-win
		intro-win/actors: object [
			on-close: func [f] [print "close" ];unview install]
		]
		view intro-win
	]

; --- compiler UI ------------------------------------------------------------

	drop-source: none
	drop-output: none
	slider-verbosity: none
	text-verbosity: none
	drop-target: none
	check-debug: none
	check-library: none
	check-no-runtime: none

	compiler-win: layout [
		title "Red Compiler"

		style path-button: button 24x24 "..."

		panel [
			text "Source:"
			path-button [
				drop-source/text: form request-file
				insert drop-source/data reduce [drop-source/text none]
				drop-source/data: unique drop-source/data
				drop-output/text: src-to-exe drop-source/text
			]
			drop-source: drop-down 300 
			;	[print mold face/data print face/selected]
				on-select [drop-output/text: src-to-exe drop-source/text]
			return
			text "Output:"
			path-button [
				drop-output/text: append form request-dir last split src-to-exe drop-source/text #"/"
			]
			drop-output: field 300
			return
			text "Verbosity:"
			slider-verbosity: slider 250 [
				level: to integer! face/data * 11
				text-verbosity/text: form reduce case [
					zero? level [["Quiet"]]
					level <= 3 	[[level "- Red only"]] 
					true 		[[level "- Red && Red/System"]]
				]
			]
			text-verbosity: text 120 "Quiet"
			return
			text "Target:"
			drop-target: drop-list data [
				"MSDOS" "Windows"
				"Linux" "Linux-ARM" "RPi"
				"Darwin"
				"Syllable"
				"FreeBSD"
				"Android" "Android-x86"
			]
		]
		panel [
			below
			check-debug: check "Debug mode"
			check-no-runtime: check "No runtime"
			check-library: check "Shared library"
		]
		return
		button "Compile" [compile]
		status: base 20x20 orange
	]

	view-compiler: does [
		drop-source/text: form current-doc/filename
		
		view/flags compiler-win [modal popup]
	]

; === support functions ======================================================

	; --- generic dialog func ------------------------------------------------

	show-dialog: func [
		title 			[string!]
		main-text 		[string!]
		/with 
			sub-text 	[string!]
		/local 
			result 		[logic!]
	] [
		result: false
		view/flags layout compose [
			title (title)
			text bold 300x30 (main-text)
			(either with [compose [return text 300x50 (sub-text) return]] ['return])
			button "Yes" [result: true unview]
			button "No" [result: false unview]
		] [popup]
		result
	]

	; --- overwrite dialog ---------------------------------------------------

	show-overwrite: func [
		filename
	] [
		show-dialog 
			"Confirm overwrite" 
			rejoin ["File " filename " already exists.^/ Do you want to overwrite it?"]
	]

	; --- not saved dialog ---------------------------------------------------

	show-not-saved: func [
		filename
		/custom
			info
	] [
		unless custom [
			info: "Do you want to save it before closing?"
		]
		show-dialog/with 
			"File not saved" 
			rejoin [
				either filename [
					rejoin ["File " filename " has been changed." ]
				] [
					"This file has not been saved yet."
				]
				newline
				info
				newline 
			] 
			"Your changes will be lost, if you don’t save them."
	]	

	; --- header editor ------------------------------------------------------

	; NOTE: this is here to simplify form processing
	;		HEADER-INFO specifies types of fields for collection
	;		HEADER is context where VID labels are kept
	;		this is done by binding the layout to that context:
	;		VIEW LAYOUT BIND SOME-LAYOUT LABEL-CTX
	;		When collecting data from form, we go thru HEADER-INFO
	;		and get right value based on type (first item: text, drop-list)
	;		next step (not implemented now) is to convert that value
	;		to right type (second item)

	header-info: context [
		title: 		[text string!]
		date: 		[text string!]
		author: 	[text string!]
		contact: 	[text string!]
		version: 	[text string!]
		file: 		[text string!]
		home: 		[text string!]
		author: 	[text string!]
		owner: 		[text string!]
		rights: 	[text string!]
		license: 	[drop-list string!]
		needs: 		[text string!]
		tabs: 		[text string!]
		purpose: 	[text string!]
		notes: 		[text string!]
	]

	header: make header-info []

	header-layout: [
		; FIXME: STYLE doesn’t seem to work right now
	;	style label: text right
	;	style wide-field: field 150

		title: "Header editor"

		group-box "Script" [
			text right "Title" 		title: field 150 "" return
			text right "File"		file: field 150 "" return
			text right "Date"		date: field 150 "" return
			text right "Version" 	version: field 150 "" return
		]
		group-box "Author" [
			text right "Author" 	author: field 150 "" return
			text right "Contact"	contact: field 150 "" return
			text right "Home"		home: field 150 "" return
		]
		return
		group-box "License" [
			text right "Owner" 		owner: field 150 "" return
			text right "Rights" 	rights: field 150 "" return
			text right "License" 	license: drop-list data ["public domain" "MIT" "BSD" "BSL" "Apache" "GPL" "LGPL" "CC"] return
		]
		group-box "Environment" [
			text right "Needs"		needs: field 150 "" return
			text right "Tabs"		tabs: field 150 "4" return
		]
		return
		group-box "Various info" [
			text right "Purpose" 	purpose: area 150x100 ""
			text right "Notes" 		notes: area 150x100 ""
		]
		return
		button "Ok" [current-doc/header: get-header unview]
		button "Cancel" [unview]
	]

	show-header-editor: has [lay data value face] [
		data: commands/parse-header
		lay: layout bind header-layout header
		foreach label words-of header-info [
			if value: select data label [
				switch first select header-info label [
					text [
						face: header/:label
						face/text: form data/:label
					]
					drop-list []
				]
			]
		]
		current-doc/header: none
		view lay
	]

	get-header: has [
		out
	] [
		out: make header []
		foreach word words-of out [
			out/:word: switch header-info/:word/1 [
				text 		[header/:word/text]
				drop-list	[pick header/:word/data header/:word/selected]
			]
		]
		replace/all out: mold out "    " "^-"
		skip out 13 ; skip MAKE OBJECT!
	]

	; --- resizing support ---------------------------------------------------

	spacex: ""

	resize-func: func [
		face 	[object!]
		diff 	[pair!]
		move? 	[logic!]
	] [
		print ["resize by " diff ", " face/type move? copy/part mold face/text 10]
	;	return true
		if diff = 0x0 [return false]
		; --- recursively search face tree
		if block? face/pane [
		;	move?: false
			print ["RES: " spacex "search"]
			insert spacex "x"
			foreach f face/pane [
				print [spacex "call resize-func"]
				move?: resize-func f diff move?
			]
			remove spacex
			move?: false
		] 
		; --- resizing
		if all [
			not equal? 'window face/type ; prevent endless loop
			object? face/extra
			not move?
		] [
			print [spacex "size change" mold face/extra]
			face/size: face/size + diff
			if in face/extra 'min-size [
				if face/size/x < face/extra/min-size/x [face/size/x: face/extra/min-size/x]
				if face/size/y < face/extra/min-size/y [face/size/y: face/extra/min-size/y]
				show face
			]
			return true ; TRUE means face was resized and we must switch to move mode
		]
		; --- moving
		if move? [
			print [spacex "offset change"]
			face/offset: face/offset + diff
			return true
		]
		false
	]

	resize-handler: [
		if event/type = 'resizing [
			print "---------resizing---------------------------------------------"
			print [event/face/type "e/o" event/offset "m/s" main/size "m/o-s" main/extra/old-size]
			if 'window = event/face/type [
				resize-func main event/offset - main/extra/old-size false
			]
			print "--------------------------------------------------------------"
			main/extra/old-size: main/size
			show main
		]
		if event/type = 'resize [
			print "resize"
		]
		none
	]
]

; === some other support

ide-update-status: func [
	handle 	[integer!]
] [
	print "IIIIIIIIDDDDEEEEEE UPDATE"
	gui/update-status handle
]


