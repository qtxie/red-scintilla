Red [
	Title: 		"Red IDE - Themes and options"
]

; --- Options context --------------------------------------------------------

editor-options: none

; === Parsers ================================================================

prefs: context [
	options:	 none
	theme:		 none
	options-blk: none

	parse-margin: func [
		handle 				[integer!]
		data 				[block!]
		/local
			width-rule 		[block!]
			type-rule 		[block!]
			sensitive-rule 	[block!]
			margin-rule 	[block!]
			margin-init 	[paren!]
			margin-action 	[paren!]
			width 			[integer!]
			type 			[word!]
			type-id 		[integer!]
			symbol 			[integer!]
			index 			[integer!]
			sensitive? 		[logic!]
	] [
		; margin: [<index> <margin-type> <properties>]
		; margin-type: [bookmark line-number fold]
		; properties [width [integer!] symbol [word!]]
		;
		; bookmark: [bookmark 1 width 16 sensitive]
		; line-numbers: [line-numbers 2 width 50]
		; fold: [fold 3 width 16]
		;
		; TODO?: index optional or fully automatic?

		width-rule: [opt 'width set width integer!]
		type-rule: [
			'bookmark 		(type-id: 0 symbol: 31)
		|	'line-numbers 	(type-id: 1)
		| 	'fold 			(type-id: 0 symbol: 6)
		]
		sensitive-rule: [opt ['sensitive (print "be sensitive" sensitive?: true)]]
		margin-rule: [
			margin-init
			set type type-rule
			set index integer!
			width-rule
			sensitive-rule
			margin-action
		]
		margin-init: quote (
			print "margin init"
			type: symbol: 0
			width: 16
			sensitive?: false
		)
		margin-action: quote (
			print [type "index:" index "type:" type-id "symbol:" symbol "sensitive?" sensitive? "width:" width]
			scintilla/set-margin-type handle index type-id
			scintilla/set-margin-width handle index width
			scintilla/set-margin-sensitive handle index sensitive?
			scintilla/marker-define handle index symbol
			either equal? type 'fold [
				scintilla/set-margin-mask handle index FE000000h
			] [
				scintilla/set-margin-mask handle index -1; 1FFFFFFh ;1 << symbol
			]
		)
	;	parse data [some margin-rule]

	; temporary hardcode

	; set line numbers
		scintilla/set-margin-type handle 1 1
		scintilla/set-margin-width handle 1 50
		scintilla/set-margin-sensitive handle 1 false

	; set bookmark
		scintilla/set-margin-type handle 2 0
		scintilla/set-margin-width handle 2 16
		scintilla/set-margin-sensitive handle 2 true
		scintilla/marker-define handle 2 31
		; margin mask must use (marker - 1) before shifting
		; it needs to get block probably and do the conversion by itself
		scintilla/set-margin-mask handle 2 1 << 1

	; set fold margin
		scintilla/set-margin-type handle 3 0
		scintilla/set-margin-width handle 3 16
		scintilla/set-margin-sensitive handle 3 true
		scintilla/marker-define handle 3 12 ; 12 = SC_MARK_BOXPLUS ; 25-31 are used for folding
		scintilla/set-margin-mask handle 3 2 << 1 ;FE000000h
	]

	; === Fonts ==================================================================

	font-proto: context [
		name:		"Lucida Console" 	; [string!]
		size: 		11 					; [float!]
		style: 		none 				; [bold italic underline]
		fore:		17.17.17 			; [tuple!]
		back: 		255.255.248 		; [tuple!]
		weight: 	none 				; [integer!]
		eol-filled?: 	false 			; [logic!]
	]

	make-font: func [
		desc 				[block!]
		/with
			def-font 		[object! none!]
		/local
			font 			[object!]
			color-rule 		[block!]
			color 			[tuple!]
			fore? 			[logic!]
			style-rule 		[block!]
			style-name 		[word!]
			style 			[block!]
			size-rule 		[block!]
			weight-rule 	[block!]
			number 			; [number!]
			name-rule		[block!]
			name 			[string!]
	] [
		unless with [def-font: font-proto]

		font: make def-font copy []
		fore?: true
		style: make block! 3

		color-rule: [
			set color tuple! (
				either fore? [fore?: false font/fore: color] [font/back: color]
			) 
		]
		style-rule: [
			set style-name ['bold | 'italic | 'underline] (
				append style style-name
			)
		]
		size-rule: 		[set number [integer! | float!] (font/size: number)]
		weight-rule: 	['weight set number number! (font/weight: number)]
		name-rule: 		[set name string! (font/name: name)]
		parse desc [
			some [
				color-rule
			|	style-rule
			|	size-rule
			|	weight-rule
			|	name-rule
			]
		]
		unless empty? style [font/style: style]
		font
	]

	set-font: func [
		handle 			[integer!]
		style 			[integer!]
		font 			[object!]
		/local
			bold? 		[logic!]
			italic? 	[logic!]
			underline? 	[logic!]
	] [
		bold?: italic?: underline?: false
		if font/name [scintilla/style-set-font handle style font/name] 
		scintilla/style-set-size-fractional handle style to integer! 100 * font/size ; FIXME: imperfect rounding ;)
		if font/style [
			if find font/style 'bold [bold?: true]
			if find font/style 'italic [italic?: true]
			if find font/style 'underline [underline?: true]
		] 
		scintilla/style-set-bold handle style bold?
		scintilla/style-set-italic handle style italic?
		scintilla/style-set-underline handle style underline?
		if font/weight [
			scintilla/style-set-weight handle style font/weight
		]	
		scintilla/style-set-fore handle style font/fore
		scintilla/style-set-back handle style font/back	
	]

	get-font: func [
		handle 			[integer!]
		style 			[integer!]
		/local
			font 		[object!]
			bold? 		[logic!]
			italic? 	[logic!]
			underline? 	[logic!]
			styles 		[block!]
			weight 		[integer!]
	] [
		font: make font-proto []
		styles: make block! 3
	;	font/name: scintilla/style-get-font handle style
		font/size: (scintilla/style-get-size-fractional handle style) / 100
		font/fore: scintilla/style-get-fore handle style
		font/back: scintilla/style-get-back handle style
		all [
			weight: scintilla/style-get-weight handle style 
			weight <> 400
			font/weight: weight
		]
		if scintilla/style-get-bold handle style [append styles 'bold]
		if scintilla/style-get-italic handle style [append styles 'italic]
		if scintilla/style-get-underline handle style [append styles 'underline]
		unless empty? styles [font/style: styles]
		font
	]

	; === Options ================================================================

	
	set-options: func [
		handle 		[integer!]
	][
		do bind [
			; margins	
			;parse-margin handle margins

			; zoom		
			scintilla/set-zoom handle zoom-level
			
			; long lines
			either long-lines? [
				scintilla/set-edge-mode handle long-lines-mode
				scintilla/set-edge-column handle long-lines-column
			] [
				scintilla/set-edge-mode handle none
			]
			
			; line ending
			scintilla/set-view-eol handle view-eol?
			scintilla/set-eol-mode handle eol-mode
			scintilla/set-paste-convert-endings handle convert-eol-on-load?

			; whitespaces
			; TODO: whitespace view options
			scintilla/set-view-ws handle ws-mode

			; indentation
			scintilla/set-tab-width handle tab-width
			scintilla/set-indent handle tab-width
			scintilla/set-use-tabs handle use-tabs?
			scintilla/set-tab-indents handle tab-indents?
			scintilla/set-backspace-unindents handle backspace-unindents? 
			scintilla/set-indentation-guides handle indentation-guides
		
			; default font
			font-proto/name: font-name
			font-proto/size: font-size

			; theme
			set-theme handle theme 
		] options
	]

	save-options: func [/local out][
		out: options-blk
		out/win-pos: gui/main/offset
		out/win-size: gui/main/size
		save cfg-path out
	]

	; === Themes =================================================================

	theme-proto: context [
		caret:			127.127.127
		caret-line:		127.127.127
		long-line: 		127.127.127

	; inner scintilla styles
		default: 		[]
		line-number: 	[]
		brace-light: 	[]
		brace-bad:		[]
		control-char: 	[]
		indent-guide:	[]
		call-tip: 		[]

	; custom styles
		keyword: 		[]
		string: 		[]
		number: 		[]
		comment: 		[]
		operator:		[]
		word:			[]
		set-word: 		[]
		lit-word:		[]
		get-word: 		[]
		path: 			[]
		set-path:		[]
		get-path: 		[]
		lit-path: 		[]
		refinement: 	[]
		binary: 		[]
		email:			[]
		file: 			[]
		url: 			[]
		tuple:			[]
		issue: 			[]
		character: 		[]
		date: 			[]
		logic: 			[]
		money: 			[]
		pair: 			[]
		time: 			[]
		datatype: 		[]
		whitespace: 	[]
		brace: 			[]
	]

	theme-default: context [
		name: 				"Solarized (dark theme)"
		caret: 				253.246.227
		caret-line: 		7.54.66
		long-line: 			88.110.117
		; inner scintilla styles
		default: 			[253.246.227 0.43.54]
		line-number: 		[238.232.213 7.54.66]
		brace-light: 		[]
		brace-bad:			[]
		control-char: 		[]
		indent-guide:		[195.195.195]
		call-tip: 			[]
		; custom styles
		keyword: 			[]
		string: 			[147.161.161 7.54.66 bold]
		number: 			[147.161.161 bold]
		comment: 			[203.75.22 bold italic]
		operator:			[108.113.196]
		word:				[238.232.213]
		set-word: 			[133.153.0]
		lit-word:			[42.161.152 italic]
		get-word: 			[38.139.210]
		whitespace: 		[88.110.117]
	]

	;load-theme-names: func [
	;	"Return map! of theme-name/theme-file pairs"
	;	/local
	;		themes-path [file!]
	;		themes 		[block!]
	;		names 		[block!]
	;		data 		[object!]
	;		name 		[string!]
	;] [
	;	print "load-theme-names"
	;	themes-path: join ide-home %themes/
	;	themes: read themes-path
	;	names: make block! 2 * length? themes
	;	foreach theme themes [
	;		data: load join themes-path theme
	;		name: select data 'name
	;		unless name [name: form theme]
	;		append names reduce [name theme]
	;	]
	;	make map! names
	;]

	set-theme: func [
		handle 			[integer!]
		theme 			[file! string! object!]
		/local
			theme-path 	[file!]
			sci-styles 	[block!]
			red-styles 	[block!]
			style 		[word!]
			def-font 	[object!]
			f 			[object!]
			keywords	[string!]
	] [
		unless object? theme [
			; TODO: some error handling
			themes-path: join ide/home-dir %cfg/themes/
			theme: join themes-path theme
			either exists? theme [
				theme: object load theme 
			][
				;-- error output
				exit
			]
		]
		sci-styles: [
			default line-number brace-light brace-bad control-char indent-guide call-tip
		]
		red-styles: [
			default comment comment-blk preface operator character quoted-string
			braced-string number pair tuple binary money issue tag file email url
			date time identifier keyword1 keyword2 keyword3 keyword4 keyword5
			keyword6 keyword7 keyword8
		]
		def-font: make-font theme/default
		set-font handle 0 def-font

		; set caret style
		scintilla/set-caret-fore handle theme/caret
		scintilla/set-caret-line-back handle theme/caret-line
		scintilla/set-caret-line-visible handle true ; TODO: move to settings
		scintilla/set-caret-width handle 2

		; set long lines styles
		scintilla/set-edge-color handle theme/long-line
		
		; set scintilla styles
		forall sci-styles [
			style: sci-styles/1
			f: make-font/with select theme style def-font
			set-font handle 31 + index? sci-styles f
		]

		; set red styles
		forall red-styles [
			style: red-styles/1
			if select theme style [
				f: make-font/with select theme style def-font
				set-font handle -1 + index? red-styles f
			]
		]

		keywords: "about ajoin alert also alter any-object? any-path? append apply array as-pair ascii? ask assert attempt bb body-of brightness? build-attach-bodybuild-markup build-tag cause-error cd center-face change-dir charset choose clean-path clear-face clear-fields closure closure? collect collect-words component? confine confirm context object cvs-date cvs-version dbug decode-cgi decode-url default deflag-face delete delete-dir deline delta-time desktop dir? dirize dispatch do-boot do-events do-face do-face-alt do-thru does dt dump-face dump-obj dump-pane echo edge-size? editor emailer enline exists-thru? exists? extract find-key-face find-window first+ flag-face flag-face? flash focus foo foo10 foo9 for forall forever forskip found? func funct function get-face get-net-info get-path? get-style has help hide-popup hilight-all hilight-text import-email in-dir in-window? info? inform input insert-event-funcinside? install invalid-utf? join last? latin1? launch-thru layout license link-app? link-relative-pathlink? list-dir load-image load-stock load-stock-block load-thru ls make-dir make-face map-each mod modified? modulo more move net-error notify offset? open-events outside? overlap? parse-email-addrsparse-header parse-header-dateparse-xml path-thru probe protect-system pwd quote read-cgi read-net read-thru reflect reform rejoin remold remove-event-funcrename repend replace request request-color request-date request-dir request-download request-file request-list request-pass request-text resend reset-face resize-face resolve rm round save-user scalar? screen-offset? scroll-drag scroll-face scroll-para send set-face set-font set-net set-para set-style set-user-name show-popup sign? size? source span? spec-of speed? split-path stylize suffix? swap take throw-error throw-on-error title-of to-binary to-bitset to-block to-char to-closure to-datatype to-date to-decimal to-email to-error to-file to-function to-get-path to-get-word to-hash to-idate to-image to-integer to-issue to-itime to-library to-list to-lit-path to-lit-word to-logic to-map to-money to-none to-pair to-paren to-path to-port to-refinement to-relative-fileto-set-path to-set-word to-string to-tag to-time to-tuple to-typeset to-url to-word true? types-of typeset? undirize unfocus uninstall unlight-text unview upgrade Usage utf? values-of vbug view viewed? viewtop what what-dir win-offset? within? words-of write-user access-os alias all any arccosine arcsine arctangent as-binary as-string bind bind? bound? break browse call caret-to-offset case catch checksum close comment compose compress continue connected? construct cosine create-link crypt-strength? debase declare decloak decompress dehex detab dh-compute-key dh-generate-key dh-make-key difference disarm do do-browser draw dsa-generate-key dsa-make-key dsa-make-signature dsa-verify-signature either else enbase encloak entab exclude exit exp foreach form free get get-env get-modes halt hide hsv-to-rgb if in input? intersect launch list-env load local-request-file log-10 log-2 log-e loop lowercase maximum-of minimum-of mold native new-line new-line? not now offset-to-caret open parse prin print protect q query quit quit-return read read-io recycle reduce remove-each repeat return reverse rgb-to-hsv rsa-encrypt rsa-generate-key rsa-make-key run routine save script? secure set set-env set-modes shift show sine size-text square-root stats switch tangent textinfo throw to-hex to-local-file to-rebol-file trace try type? unbind union unique unless unprotect unset until update uppercase use value? wait with while write write-io"
		scintilla/set-keywords handle 0 keywords
	]

	themes: make map! 20

	;get-themes: has [theme-path theme files] [
	;	clear themes
	;	print "GET-THEMES: get-themes"
	;	print read ide-home
	;	themes-path: join ide-home %themes/
	;	either exists? themes-path [
	;		files: probe read themes-path
	;		foreach file files [
	;			theme: load join themes-path file
	;			extend themes reduce [theme/name file]
	;		]
	;		sort words-of themes
	;	] [
	;		print "GET-THEMES: no themes found"
	;		copy []
	;	]
	;]


	; --- initialization ---------------------------------------------------------

	init-editor: func [ ; FIXME: can't be routine <- throws an error
		"Do basic initialization (set UTF8 codepage, etc)"
		handle		[integer!]
	] [
		scintilla/set-property handle "type" "editor"
		; set word chars (FIXME: ASCII only, should support Unicode. Can scintilla do it?)
		scintilla/set-word-chars handle "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?+-!@#$%^^*/'."
		scintilla/set-punctuation-chars handle "^"[]{}()/"
	;
	;	scintilla/ac-set-separator handle 32 ; space
		scintilla/ac-set-type-separator handle 31 ; FIXME: find best separator

		; set calltip style
		scintilla/calltip-use-style handle 25

		; set UTF8 codepage
		scintilla/set-code-page handle 65001
		; scroller tracks width 
		scintilla/set-scroll-width-tracking handle true
		; set lexer
		scintilla/set-lexer handle 0 ; SCLEX_CONTAINER
	]

	init-info: func [
		"Do basic initialization (set UTF8 codepage, etc)"
		hWnd		[integer!]
	] [
		scintilla/set-property hWnd "type" "console"
 		; set UTF8 codepage
		scintilla/set-code-page hWnd 65001
		; read only
	;	scintilla/set-read-only hWnd true
	
		scintilla/set-scroll-width-tracking hWnd true
		; theming
		prefs/set-theme hWnd editor-options/theme
	]

	init: func [][
		cfg-path: join ide/home-dir %cfg/default.options
		options-blk: load cfg-path
		options: make object! options-blk
		gui/main/offset: options/win-pos
		gui/main/size: options/win-size
		show gui/main
		gui/main/visible?: yes
		show gui/main
	]
]