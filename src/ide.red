Red [
	Title:			"Red IDE"
	Author: 		"Boleslav Březovský" "Qingtian Xie"
	File: 	 		%ide.red
	Tabs:	 		4
	Icon:	 		%../res/red-ant.ico
	Version: 		0.0.0.1
	Company: 		"FullStack Technologies"
	File:			%ide.red
	Needs: 			'View
	Config:		[
		gui-console?: yes
		red-help?: yes
	]
	Notes: {
		ide: context [
		    editor:
		    prefs:
		    gui:
		    files:
		    projects: none
		    ...
		]

		#include %editor.red    ;-- editor component, can include R/S files
		;--     ide/editor: context [
		;--            ;-- editor high-level interaction code, getting text, sending text (loaded from disk, Internet, ...)
		;--            ;-- the code here is totally independent of the editor component used
		;--            ;-- the IDE parts should only access the editor through this object.
		;--     ]

		#include %prefs.red
		;--        ide/prefs: context [
		;--            ;-- all IDE settings management, can have sub-objects for different parts of the IDE
		;--            ;-- Settings here also cover editing, but they are independent of the editor component used.
		;--        ]

		#include %gui.red
		;--     ide/gui: context [
		;--         ;-- IDE gui management. The editor component should expose a higher-level API callable from the
		;--         ;-- code here.
		;--     ]

		#include %files.red
		;--     ide/files: context [
		;--         ;-- source files management functions
		;--     ]

		#include %projects.red
		;--     projects: context [
		;--         ;-- projects data management functions
		;--     ]

		view ide/gui/main
	}
]

system/view/auto-sync?: no
system/state/trace?: no

#include %../../red/environment/console/help.red
#include %../../red/environment/console/engine.red
#include %../../red/environment/console/auto-complete.red

ask: routine [
	question [string!]
	return:  [string!]
][
	as red-string! _series/copy
		as red-series! terminal/ask question
		as red-series! stack/arguments
		null
		yes
		null
]

input: does [ask ""]

#system [
	#include %../../red/environment/console/terminal.reds
]

; --- include required files -------------------------------------------------
#include %scintilla/api.red
#include %call/call.red
#include %tools.red
#include %gui.red
#include %commands.red
#include %prefs.red
#include %files.red
#include %editor.red

	;do %./src/gui.red
	;do %./src/commands.red
	;do %./src/prefs.red
	;do %./src/files.red
	;do %./src/editor.red
	
ide: context [

	home-dir: what-dir

	; --- some support func for debugging
	debug?: true ;false
	debug-print: func [data][if debug? [print data]]

	start: does [
		gui/init
		prefs/init
		files/init
		system/console/launch
		;do-events
	]
]

ide/start