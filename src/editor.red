Red [

]

search-object-proto: object [
	case-sensitive?: 	false
	whole-word?: 		false
	in-selection?: 		false
	previous?: 			false
	all?: 				false
	phrase:				""
	replace:			""
	replace?: 			false
	replace-all?: 		false
] 

search-object: make search-object-proto []

editor: context [

	init: func [
		handle	[integer!]
	][
		scintilla/set-scroll-width handle 10
		scintilla/set-property handle "type" "editor"
		; set word chars (FIXME: ASCII only, should support Unicode. Can scintilla do it?)
		scintilla/set-word-chars handle "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?+-!@#$%^^*/'."
		scintilla/set-punctuation-chars handle "^"[]{}()/"
		;@@ scintilla/ac-set-separator handle 32	; space
		scintilla/ac-set-type-separator handle 31	; FIXME: find best separator	
		scintilla/calltip-use-style handle 25		; set calltip style
		scintilla/set-code-page handle 65001		; set UTF8 codepage
		scintilla/set-scroll-width-tracking handle true
		scintilla/set-lexer handle 121				; set lexer: SCLEX_RED
		prefs/set-options handle
	]

	start-undo-collection: func [
		"Clear undo buffer for doc and start undo action"
		handle 			[integer!]
	] [
		scintilla/set-undo-collection handle true
		scintilla/empty-undo-buffer handle
		scintilla/begin-undo-action handle
	]

	set-save-state: func [
		handle 			[integer!]
		saved? 			[logic!]
		/local
			doc 		[object!]
	] [
		doc: select files/docs handle
		doc/saved?: saved?
		gui/update-doc-info current-doc/filename
	]

	show-result: func [
		handle 		[integer!]
		position 	[integer!]
		text 		[string!]
		/local
			line 	[integer!]
			code 	[block!]
	] [
		line: scintilla/line-from-position handle position

		switch editor-options/run-output [
		 	editor [
				scintilla/annotation-set-text handle line copy text 
				scintilla/annotation-set-visible handle 'standard
			]
			console [
				print [">>" mold scintilla/get-text info-handle scintilla/get-length info-handle]
			;	scintilla/set-read-only info-handle false
			;	scintilla/append-text probe info-handle probe join text newline
			;	scintilla/set-text probe info-handle probe join text newline
			;	scintilla/set-read-only info-handle true
			]
		]
	]

	comment-line: func [
		handle 	[integer!]
		line 	[integer!]
	] [
		position: scintilla/position-from-line handle line
		scintilla/insert-text handle position "; "
	]

	uncomment-line:	func [
		handle 	[integer!]
		line 	[integer!]
		/local
			char 	[integer!]
	] [
		position: scintilla/position-from-line handle line
		while [
			char: scintilla/get-char-at handle position
			not equal? 59 char ; #";"
		] [
			if zero? char [exit]
			position: position + 1
		]
		scintilla/delete-range handle position 2
	]

	get-document: func [
		handle 	[integer!]
	] [
		scintilla/get-text handle scintilla/get-length handle
	]

	; --- indicators ---------------------------------------------------------

	set-indicator: func [ ; TODO: not used, check
		"Add indicator to text"
		handle 					[integer!]
		position 				[integer!]
		length 					[integer!]
	] [
		; TODO: make it flexible (color, style...)
		scintilla/indicator-set-style handle 1 6
		scintilla/indicator-set-fore-color handle 1 200.210.220
		scintilla/set-indicator-current handle 1
		scintilla/indicator-fill-range handle position length
	]

	clear-indicators: func [ ; TODO: not used, check
		"Clear all indicators"
		handle 					[integer!]
		start 					[integer!]
		end 					[integer!]
	] [
		; TODO: make it flexible (indicator ID is hardcoded as 1 currently)
		scintilla/set-indicator-current handle 1
		scintilla/indicator-clear-range handle start end
	]

	; --- search -------------------------------------------------------------

	set-search: func [
		"Set search flags"
		handle 					[integer!]
		search-object 			[object!]
		/local
			flags 				[integer!]
	] [
		flags: 0
		if search-object/case-sensitive? [flags: 4]
		if search-object/whole-word? [flags: flags or 2]
		scintilla/set-search-flags handle flags
	]

	find-text: func [
		handle 					[integer!]
		search-object 			[object!]
		/only 					"Do not mark"	
		/local
			position 			[integer!]
			orig-pos			[integer!]
			target-start 		[integer!]
			mark-start 			[integer!]
			target-end 			[integer!]
			string-size			[integer!]
			matches 			[integer!]
	] [
		; NOTE: Search uses indicator 1 to underline matched strings
		; check if we have something to search
		if any [
			none? search-object/phrase
			empty? search-object/phrase
		] [return none]
		; set some variables
		matches: 0
		mark-start: 0
		print "*** find-text"
		orig-pos: scintilla/get-current-position handle 
		string-size: string-size? search-object/phrase
		either search-object/in-selection? [
			target-start: scintilla/get-selection-start handle
			target-end: scintilla/get-selection-end handle
		] [
			target-start: either search-object/all? [1] [orig-pos]
			target-end: 1 + scintilla/get-length handle
		]
		set-search handle search-object
		; remove old indicators
		clear-indicators handle 1 target-end - 1
		until [
			; target whole document or from the end of last search	
			scintilla/set-target-range handle target-start target-end
			; do actual search
			position: scintilla/search-text handle search-object/phrase
			unless zero? position [
				matches: matches + 1
				; set selection for the first matched string
				if 0 = mark-start [mark-start: position]
				either search-object/replace? [
					; replace result
					scintilla/set-sel current-doc/handle position position + string-size
					scintilla/replace-selection current-doc/handle search-object/replace
					unless search-object/replace-all? [position: 0]
				] [
					; show result (add indicator)
					set-indicator handle position string-size
				]
			]
			; move target start after matched string
			target-start: 1 + position + string-size
			; check if there's nothing more to find
			zero? position
		]

	;	print ["Matches: " matches]

		; go back to original position when needed
		if search-object/replace? [
			scintilla/go-to-position handle orig-pos
		]
		; select result (if there's any)
		if all [not only not zero? mark-start] [
			scintilla/set-selection-start handle mark-start
			scintilla/set-selection-end handle mark-start + string-size
		]
	]

	; --- autocomplete support -----------------------------------------------

	autocomplete-word: func [
		handle 	[integer!]
		word 	[string!]
	;	list 	[string!]
		/local
			list
			words
	] [
		words: words-of system/words
		list: make block! length? words
		foreach wrd words [if find mold wrd word [append list wrd]]
		list: mold/only sort list
		if all [
			not zero? length? list
			2 < length? word 
			word <> list
		] [
			scintilla/ac-show handle length? word list
		]
	]

	; --- calltips support ---------------------------------------------------

	parse-calltip-spec: func [
		word
		/local
			out 	[string!]
			spec 	[block!]
			value
			type
			desc
			help-string-rule 	[block!]
			param-rule 			[block!]
			refinement-rule 	[block!]
			tabs 				[string!]
	] [
		out: make string! 300
		spec: spec-of get word
		value: none
		tabs: tab
		help-string-rule: [
			set value string! (
				append out rejoin [value newline]
				append out rejoin [
					tab tab 
					word " is of type: " mold type? get word 
					newline newline
				]
			)
		]
		param-rule: [
			set value [word! | lit-word! | get-word!]
			opt [set type block!]
			opt [(desc: none) set desc string!]
			(
				append out rejoin [tabs value tab mold type]
				if desc [append out rejoin [tab desc]]
				append out newline 
			)
		]
		refinement-rule: [
			set value refinement!
			opt [(desc: none) set desc string!]
			(
				append out rejoin [tab mold value tab desc newline]
				tabs: "^-^-"
			)
			any param-rule
		;	(append out newline)
		]
		parse spec [
			opt help-string-rule
			any param-rule
			(append out newline) 
			any refinement-rule
		]
		while [newline = last out] [
			remove back tail out ; get rid of last newline /TRIM can't do it/
		]
		out
	]

	describe-object: func [
		"Return text description of an object"
		symbol 			[word!]
		/local
			tip 		[string!]
			length 		[integer!]
			words 		[block!]
			values 		[block!]
	] [
		; TODO: put various length limits to settings
		;	and allow to change them with refinements
		value: get symbol
		words: words-of value
		values: values-of value
		tip: rejoin [
			symbol " is " type? value " with " length? values " elements." newline
		]
		length: min 10 length? values
		repeat i length [
			value: trim/lines mold values/:i
			append tip rejoin [
				tab words/:i ":" 
				tab copy/part value 15 
				either 15 < length? value ["..."][""] 
				newline
			]
		]
		if length < length? values [
			append tip "..."
		]
		tip
	]

	get-calltip: func [
		src 			[string!]
		/local
			code 		[block!]
			symbol 		[any-type!]
			value 		[any-type!]
			calltip 	[string!]
			tip length words values
	] [
		; TODO: simplify the conditions, add more types
		code: try [load/all src]
		unless empty? code [
			symbol: first code
			value: either word? symbol [
				if unset? get/any symbol [return ""]
				get symbol
			] [
				symbol
			]
			calltip: case [
				any [
					function? :value
					action? :value
					routine? :value
					native? :value
					op? :value
				] [
					parse-calltip-spec symbol
				]
				string? value [
					rejoin [
						symbol " is string! with " length? value " characters." newline
						copy/part value 40 either (length? value) > 40 ["..."] [""]
					]
				]
				block? value [
					rejoin [
						symbol " is block! with " length? value " elements." newline
						form copy/part value 10 either (length? value) > 10 ["..."] [""]
					]
				]
				any [
					object? value
					map? value
				] [
					describe-object symbol
				]
				not word? value [
					rejoin [mold type? value " with value " mold value]
				]
				true [
					mold value
				]
			]
		]
		calltip
	]

	find-word?: func [
		words 		[string!]
		word 		[string!]
	] [
		; FIXME: seems to return TRUE always
		ret: parse append " " words [
			some [
				[space word [space to end | end]]
			|	skip	
			]
		]
		ret
	]

	; FIXME: have a cache object for this?
	; add space before and after to simplify searching for whole words
	words-cache: head append " " head append mold/only sort words-of system/words space

	; FIXME: no scintilla specific code should be here

	show-calltip: func [
		handle 		[integer!]
		position 	[integer!]
		word 		[string!]
		/local
			calltip [string!]
			words 	[string!]
	] [
		words: words-cache
		either find words rejoin [space word space] [
			calltip: get-calltip word
			unless empty? calltip [
				scintilla/calltip-show handle position calltip 
				; TODO: highlighiting (of what? first line?)
			;	scintilla/calltip-set-fore-hlt handle red
			;	scintilla/calltip-set-hlt handle 1 3  ; hardcoded test
			]
		] [
			scintilla/calltip-cancel handle
		]
	]


	do-script: func  [
		"Run string in interpreter and return result or formatted error text"
		script 		[string!]
		/local
			result 
	] [
		result: ""
		if empty? script [return ""]
		result: do [
			; check for syntax errors
			result: either error? set/any 'result try [load/all script] [
				form-error :result
			] [
				; check for script errors
				result: either error? set/any 'result try result [
					form-error :result
				] [
					result: either unset? :result [""] [rejoin ["== " :result]]
				]
			] 
			result
		]
		result
	]


; ============================================================================

	get-pos-as-pair: function [
		"Return position as pair!"
		handle
	] [
		print "*** get-pos-as-pair"
		pos: scintilla/get-current-position handle
		line: scintilla/line-from-position handle pos
		line-start: scintilla/position-from-line handle line
		as-pair pos - line-start line
	]

	pair-as-pos: function [
		handle
		pair
	] [
		pair/x + line-start: scintilla/position-from-line handle pair/y
	]

	set-selection: function [
		handle
		start
		end
	] [
		scintilla/set-selection-start handle start
		scintilla/set-selection-end handle end
	]

; ----------------------------------------------------------------------------

	pos: line: none

	rule-move: [
		'up	(
			print "*** rule-move"
			print [
				">>>" scintilla/get-current-position handle
				"up@" get-pos-as-pair handle
				"pos" pair-as-pos handle get-pos-as-pair handle
			]
		)
	|	'down 		(print "down")
	|	'left 		(print "left")
	|	'right 		(print "right")
	]

	rule-select: [
		(
			print "*** rule-select"
			pos: scintilla/get-current-position handle
			line: scintilla/line-from-position handle pos
			print "kekebre"
		)
		'select [
			'word (
				start: scintilla/word-start-position handle pos true
				end: scintilla/word-end-position handle pos true 
			)
		|	'line (
				start: scintilla/position-from-line handle line
				end: -1 + scintilla/position-from-line handle line + 1
			)
		|	'to 'line 'start (
				start: scintilla/position-from-line handle line
				end: pos
			)
		] (
			print ["select form" start "to" end]
			set-selection handle start end
		)
	]

	process-cmd: func [
		"Process command for current doc"
		data 	[string!] "Command(s) to execute"
		/with
			handle 	[integer!] "Select different doc handle"
		/local
			block
	] [
		unless with [handle: current-doc/handle]
		block: clear []
		load/into data block

		rule-move: bind rule-move 'handle
		rule-select: bind rule-select 'handle
		
		parse block [
			rule-move
		|	rule-select
		]
		scintilla/grab-focus handle
	]

]