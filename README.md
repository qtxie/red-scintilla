# Project Description

[Red IDE](http://github.com/red/red-ide/) is a lightweight cross-platform Red IDE with syntax highlighting,
code completion and debugger.

![Red IDE screenshot]()

## Features

* Written in Red, so easily customizable.
* Small, portable. (only Window for now)
* Auto-completion for functions, keywords.
* Interactive console to directly test code snippets.
* Integrated debugger

## License

See [LICENSE](LICENSE).
